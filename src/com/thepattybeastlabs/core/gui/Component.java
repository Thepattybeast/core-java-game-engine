package com.thepattybeastlabs.core.gui;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;

public abstract class Component {

	protected float x, y;
	protected float width;
	protected float height;
	protected boolean highlighted;
	public enum Alignment {DEFAULT, CENTER};
	private Alignment align;
	protected String id = "";
	protected boolean enabled = true;
	protected Map<String, Color> colors = new TreeMap<String, Color>(String.CASE_INSENSITIVE_ORDER);
	
	public Component(float x, float y, float width, float height, Alignment align){
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.align = align;
		enabled = true;
	}
	
	public Component(String id, float x, float y, float width, float height){
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.id = id;
		align = Alignment.DEFAULT;
		enabled = true;
	}
	
	public void render(Graphics g, Input in){
		render(g, in, x, y, width, height);
	}
	
	protected abstract void renderI(Graphics g, Input in, float x, float y, float width, float height);
	protected abstract void renderOverlayI(Graphics g, Input in, float x, float y, float width, float height);
	
	public void render(Graphics g, Input in, float x, float y, float width, float height){
		renderI(g, in, x, y, width, height);
	}
	
	public void renderOverlay(Graphics g, Input in, float x, float y, float width, float height){
		renderOverlayI(g, in, x, y, width, height);
		
	}
	
	public void tick(Input in, AudioEngine ae, float delta){
		tick(in, ae, delta, x, y, width, height);
		
	}
	
	public void tick(Input in, AudioEngine ae, float delta, float x, float y, float width, float height){
		if(enabled){
			highlighted = enabled&&in.getX() > x && in.getY() > y && in.getX() < x+width && in.getY() < y+height;
			tickI(in, ae, delta, x, y, width, height);
		}else{
			highlighted=false;
		}
	}
	
	protected abstract void tickI(Input in, AudioEngine ae,float delta, float x, float y, float width, float height);
	
	public float getX() {
		return x;
	}

	public Component setX(float x) {
		this.x = x;
		return this;
	}

	public float getY() {
		return y;
	}

	public Component setY(float y) {
		this.y = y;
		return this;
	}

	public float getWidth() {
		return width;
	}

	public Component setWidth(float width) {
		this.width = width;
		return this;
	}

	public float getHeight() {
		return height;
	}

	public Component setHeight(float height) {
		this.height = height;
		return this;
	}
	
	public Component setDim(float width, float height){
		setWidth(width);
		setHeight(height);
		return this;
	}
	
	public Component setPos(float x, float y){
		setX(x);
		setY(y);
		return this;
	}
	
	public Map<String, Color> getColors(){
		return colors;
	}
	
	public Component setColor(String colorName, Color color){
		colors.put(colorName, color);
		return this;
	}
	
	/***Apply color to the graphics object
	 * @param g Graphics object to apply the color to (g.setColor())
	 * @param name of the color to apply
	 * @returns true if the color is not null (colors.get(name)!=null)*/
	protected boolean setColor(Graphics g, String name){
		Color c = colors.get(name);
		if(c==null)return false;
		g.setColor(c);
		return true;
	}
	
	public Component setColors(Map<String, Color> colors){
		this.colors.putAll(colors);
		return this;
	}

	/**Component must be added to a valid com.thepattybeastlabs.core.gui.Container to work
	 * @See com.thepattybeastlabs.core.gui.Container*/
	public Component setAlign(Alignment align){
		this.align = align;
		return this;
	}
	
	public Alignment getAlign(){
		return align;
	}
	
	/**Should return true the tick when the component is activated by the user (ie when a button is clicked). 
	 * Component.tick() should be called first(it will be called whenever the container it is attached to is ticked and 
	 * should only be called once)*/
	public abstract boolean isActivated(Input in);
	
	public boolean isHighlighted() {
		return highlighted;
	}

	public String getId() {
		return id;
	}

	public Component setId(String id) {
		this.id = id;
		return this;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public Component setEnabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	public Component setID(String id){
		this.id = id;
		return this;
	}
	
	public String getID(){
		return id;
	}
	
}
