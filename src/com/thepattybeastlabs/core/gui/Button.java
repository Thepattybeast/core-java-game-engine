package com.thepattybeastlabs.core.gui;

import java.awt.Color;
import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.Image;

public class Button extends Component {

	protected String text = "Button";
	protected float selAnim = 0;
	public static final Vector2[] RECT = new Vector2[]{new Vector2(0f,0f),new Vector2(1f,0f),
	new Vector2(1f,1f),new Vector2(0f,1f)};
	public static final int[] RECTI = new int[]{0,1,3,1,3,2};
	
	public Button(String text, String id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		this.text = text;
	}

	public void renderI(Graphics g, Input in, float x, float y, float width, float height) {
		g.setColor(Color.BLUE);
		//if(highlighted)g.setColor(Color.CYAN);
		//g.fillRect(x, y, width, height);
		g.setColor(Color.WHITE);
		g.fill(RECT, RECTI, x, y, width-40+selAnim, height,
				new Color[]{new Color(100+(int)selAnim,100+(int)selAnim,100+(int)selAnim,255)
				,new Color(100+(int)selAnim,100+(int)selAnim,100+(int)selAnim,0),new Color(100+(int)selAnim,100+(int)selAnim,100+(int)selAnim,0),
				new Color(100+(int)selAnim,100+(int)selAnim,100+(int)selAnim,255)});
		if(highlighted){
			g.setColor(Color.YELLOW);
		}else{
			g.setColor(Color.WHITE);
		}
		g.setFont("Tahoma", height-4);
		g.drawString(text, x+5, y);
	}

	protected void tickI(Input in, AudioEngine ae, float delta, float x, float y, float width, float height) {
		if(highlighted){
			selAnim+=1*delta;
		}else{
			selAnim-=1*delta;
		}
		if(selAnim < 0)selAnim = 0;
		if(selAnim > 40)selAnim = 40;
	}

	public boolean isActivated(Input in) {
		return enabled && highlighted && (in.isPressed("gui_activate"));
	}

	@Override
	protected void renderOverlayI(Graphics g, Input in, float x, float y,
			float width, float height) {
		// TODO Auto-generated method stub
		
	}

	public void setText(String string) {
		this.text = string;
	}

}
