package com.thepattybeastlabs.core.gui;

import java.awt.Color;

import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;

public class ScrollBar extends Component{

	public static enum Orientation{UPPER, LOWER, LEFT, RIGHT}
	
	public static int depth = 15;
	protected Orientation orientation;
	protected float loc, size;
	
	public ScrollBar(String id, float x, float y, Orientation orientation, int size) {
		super(id, x, y, (orientation == Orientation.UPPER || orientation == Orientation.LOWER)? size : depth
				, (orientation == Orientation.UPPER || orientation == Orientation.LOWER)? depth : size);
		this.orientation = orientation;
		this.size = size;
		if(size<0)size=0;
		this.loc = 0;
		setColor("fore", Color.WHITE);
		setColor("back", null);
		setColor("border", Color.WHITE);
	}

	

	@Override
	protected void renderI(Graphics g, Input in, float x, float y, float width,
			float height) {
		g.setColor(Color.WHITE);
		float slideSize = loc/(((size+getDrawSize(width, height))/getDrawSize(width, height)));
		float slideDSize = getDrawSize(width, height)/(((size+getDrawSize(width, height))/getDrawSize(width, height)));
		if(size==0)return;
		if(setColor(g,"back"))g.fillRect(x, y, width, height);
		if(setColor(g,"border"))g.drawRect(x, y, width, height);
		if(orientation == Orientation.UPPER || orientation == Orientation.LOWER){
			if(setColor(g,"fore"))g.fillRect(x + (slideSize), y, (slideDSize < 10)? 10 : slideDSize, height);
		}else{
			if(setColor(g,"fore"))g.fillRect(x, y + (slideSize), width, (slideDSize < 10)? 10 : slideDSize);
		}
	}
	
	public float getDrawSize(float width, float height){
		return (orientation == Orientation.UPPER || orientation == Orientation.LOWER)? width : height;
	}

	protected void tickI(Input in, AudioEngine ae, float delta, float x, float y,
			float width, float height) {
		if(in.isDown("gui_activate") && highlighted){
			float slideSize = getDrawSize(width, height)/(size);
			if(orientation == Orientation.UPPER || orientation == Orientation.LOWER){
				loc = (int)((in.getX()-x)*slideSize);
				
				//System.out.println("Loc");
			}else{
				loc = (int)((in.getY()-y)/slideSize);
			}
			
		}
		if(loc>size)loc=size;
		if(loc < 0)loc = 0;
	}

	@Override
	public boolean isActivated(Input in) {
		// TODO Auto-generated method stub
		return false;
	}

	protected void renderOverlayI(Graphics g, Input in, float x, float y,
			float width, float height) {
		
	}



	public float getLoc() {
		return loc;
	}



	public void setLoc(float loc) {
		this.loc = loc;
	}



	public float getSize() {
		return size;
	}

	public boolean isShowing(){
		return (size!=0);
	}

	public void setSize(float size) {
		this.size = size;
		if(size<0)this.size=0;
	};
	
	
}
