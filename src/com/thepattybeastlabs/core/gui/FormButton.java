package com.thepattybeastlabs.core.gui;

import java.awt.Color;

import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.input.Input;

public class FormButton extends Button{

	public FormButton(String text, String id, float x, float y, float width, float height) {
		super(text, id, x, y, width, height);
		setColor("back", Color.BLUE);
		setColor("fore", Color.WHITE);
		setColor("border", Color.WHITE);
		setColor("highBack", Color.CYAN);
		setColor("highFore", Color.BLACK);
		setColor("highBorder", Color.WHITE);
		setColor("disabledBack", Color.DARK_GRAY);
		setColor("disabledFore", Color.WHITE);
		setColor("disabledBorder", Color.WHITE);
	}
	
	public void render(Graphics g, Input in, float x, float y, float width, float height) {
		setColor(g,"back");
		if(highlighted){
			if(setColor(g, "highBack"))g.fillRect(x, y, width, height);
			if(setColor(g, "highBorder"))g.drawRect(x, y, width, height);
			setColor(g, "highFore");
		}else if(!enabled){
			if(setColor(g, "disabledBack"))g.fillRect(x, y, width, height);
			if(setColor(g, "disabledBorder"))g.drawRect(x, y, width, height);
			setColor(g, "disabledFore");
		}else{
			if(setColor(g, "back"))g.fillRect(x, y, width, height);
			if(setColor(g, "border"))g.drawRect(x, y, width, height);
			setColor(g, "fore");
		}
		g.setTextAlign(1, 1);
		g.setFont("Tahoma", height-4);
		g.drawString(text, x+(width/2), y+height/2);
		g.setTextAlign(0, 0);
	}

}
