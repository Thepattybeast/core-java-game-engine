package com.thepattybeastlabs.core.gui;

import java.util.ArrayList;
import java.util.Arrays;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class WindowManager {
	
	public static final WindowManager I  = new WindowManager();
	
	private List<Window> windows = new ArrayList<>();
	private Window bringToFocus;
	private Queue<Window> addQueue = new LinkedBlockingQueue<>();
	private int filterId=-1;
	
	public WindowManager(){
	}
	
	public void add(Window window){
		addQueue.add(window);
		window.setWM(this);
	}
	
	public boolean hasFocus(Window window){
		if(windows.size()<1)return false;
		return windows.get(windows.size()-1)==window;
	}
	
	public void bringToFocus(Window window){
		if(contains(window)){
			bringToFocus = window;
		}
	}
	
	public Window getFocused(){
		if(windows.size()==0)return null;
		return windows.get(windows.size()-1);
	}
	
	public void remove(Window window){
		windows.remove(window);
	}
	
	public Window get(int index){
		return windows.get(index);
	}
	
	public void remove(int index){
		windows.remove(index);
	}
	
	public int getWindowCount(){
		return windows.size();
	}
	
	public boolean contains(Window window){
		return windows.contains(window)||addQueue.contains(window);
	}
	
	public List<Window> windows(){
		return windows;
	}
	
	public void render(Graphics g, Input in){
		for(int i = 0; i < windows.size(); i++){
			windows.get(i).render(g, in);
		}
		
	}
	
	public void tick(Input in, AudioEngine ae, float delta){
		if(filterId==-1||in.getFilter(filterId)==null){
			
			filterId=in.createFilter((input,id,value,active,self)->{
				if(windows.size()==0)return value;
				if(input.getKeybind(id).getController()!=Input.OS_MOUSE)return value;
				Window w = getFocused();
				if(active==self){
					return value;
				}else if(in.getX()<w.getX()-Window.borderWidth||in.getY()<w.getY()-Window.borderWidth-20
						||in.getX()>w.getX()+w.getWidth()+Window.borderWidth||in.getY()>w.getY()+w.getHeight()+Window.borderWidth){
					return value;
				}
				return 0;
			});
		}
		in.validateFilter(filterId);
		for(int i = 0; i < windows.size(); i++){
			if(i==windows.size()-1)in.enableFilter(filterId);
			windows.get(i).tick(in, ae, delta);
			if(i==windows.size()-1)in.disableFilter(filterId);
		}
		if(bringToFocus!=null){
			windows.remove(bringToFocus);
			windows.add(bringToFocus);
			bringToFocus=null;
		}
		while(!addQueue.isEmpty()){
			windows.add(addQueue.poll());
		}
	}

	public boolean contains(Class<? extends Window> windowClass) {
		for(Window w : windows){
			if(w.getClass().isAssignableFrom(windowClass))return true;
		}
		for(Window w : addQueue){
			if(w.getClass().isAssignableFrom(windowClass))return true;
		}
		return false;
	}
	
}
