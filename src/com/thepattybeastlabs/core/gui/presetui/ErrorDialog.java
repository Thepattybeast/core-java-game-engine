package com.thepattybeastlabs.core.gui.presetui;

import com.thepattybeastlabs.core.gui.Component;
import com.thepattybeastlabs.core.gui.Container;
import com.thepattybeastlabs.core.gui.FormButton;
import com.thepattybeastlabs.core.gui.TextBox;
import com.thepattybeastlabs.core.gui.Window;

public class ErrorDialog extends Container {

	private TextBox text;
	private Window w;
	
	public static Window window(String title, Throwable error, float width, float height){
		Window w = new Window(width, height, title);
		ErrorDialog e = new ErrorDialog(error, "Exit_Btn", 1, 1, -1, -1, w);//I feel bad really
		w.addComponent(e);
		return w;
	}
	
	public ErrorDialog(Throwable e, String id, float x, float y, float width, float height, Window w) {
		super(id, x, y, width, height);
		this.w = w;
		StringBuilder b = new StringBuilder(e.toString()+"\n");
		StackTraceElement[] stes = e.getStackTrace();
		for(StackTraceElement ste : stes){
			b.append("   at "+ste+"\n");
		}
		text = new TextBox(b.toString(), "errorBox", 10, 10, -20, -55);
		text.setLineWrap(false);
		addComponent(new FormButton("Exit Game", "exit", -120, -35, 110, 25));
		addComponent(text);
	}

	@Override
	public void onActivate(Component comp, String id) {
		if(id.equals("exit")){
			w.setLastActivatedId("Exit_Btn");
		}
	}

	
	
}
