package com.thepattybeastlabs.core.gui.presetui;

import java.awt.Color;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.gui.Component;
import com.thepattybeastlabs.core.gui.FormButton;
import com.thepattybeastlabs.core.gui.Window;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.Image;

public class ImageListWindow extends Window {

	private int index;
	
	public ImageListWindow(float x, float y, float width, float height) {
		super(x, y, width, height, "Image Viewer");
		addComponent(new FormButton("Next", "next", -170, 20, 150, 25));
	}

	@Override
	protected void renderI(Graphics g, Input in, float x, float y, float width, float height) {
		super.renderI(g, in, x, y, width, height);
		g.setColor(Color.WHITE);
		Image i = Image.IMAGES.get(index);
		if(i.getProps()!=null){
			g.drawString((index+1)+"/"+Image.IMAGES.size() + " " + i.getProps().getWidth() + "x"+i.getRealHeight() + " " + i.getId(), x+20, y+20);
			g.drawImage(i, x+20,y+50,i.getWidth(), i.getHeight());
		}else{
			g.drawString((index+1)+"/"+Image.IMAGES.size() + " No Information Avalible.", x+20, y+20);
		}
		
		
	}

	@Override
	public void onActivate(Component comp, String id) {
		super.onActivate(comp, id);
		if(id.equals("next")){
			index++;
			if(index>=Image.IMAGES.size())index=0;
		}
	}
	
	

}
