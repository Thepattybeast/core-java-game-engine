package com.thepattybeastlabs.core.gui.presetui;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.gui.Container;
import com.thepattybeastlabs.core.gui.TabbedComponent;
import com.thepattybeastlabs.core.gui.Window;

public class ConfigDialog extends TabbedComponent {

	public ConfigDialog(float x, float y, float width, float height) {
		super("config",x, y, width, height);
		addTab("Display", new VideoConfigView("vidcfg", 1, 1,-2, -2));
		addTab("Keybinds",new KeybindConfigView("keycfg", 1, 1,-2, -2));
	}
	
	public static void show(String title){
		Window w = new Window(600, 400, title);
		w.addComponent(new ConfigDialog(1, 1, -2, -2));
		Core.wm.add(w);
	}

}
