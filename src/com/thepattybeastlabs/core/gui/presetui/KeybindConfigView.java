package com.thepattybeastlabs.core.gui.presetui;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.gui.Button;
import com.thepattybeastlabs.core.gui.Component;
import com.thepattybeastlabs.core.gui.Container;
import com.thepattybeastlabs.core.gui.FormButton;
import com.thepattybeastlabs.core.gui.List;
import com.thepattybeastlabs.core.input.Controller;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.input.Input.KeyBind;

public class KeybindConfigView extends Container {

	private List<Input.KeyBind> keybinds;
	private boolean listening, inputsReleased, inputActivated;
	
	public KeybindConfigView(String id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		keybinds = new List<>("keybind", 1, 1, -2, -37, new List.AbstractItemManager<Input.KeyBind>() {
			public String getText(KeyBind item, int index) {
				return item.getDisplayName() + " - " + item.getComponenet().getName();
			}
		});
		keybinds.setData(Core.input.getKeybinds().toArray(new Input.KeyBind[0]));
		addComponent(keybinds);
		addComponent(new FormButton("Set Keybind", "keybind", 1, -27, -2, 25));
	}

	@Override
	protected void renderI(Graphics g, Input in, float x, float y, float width, float height) {
		if(listening){
			g.setTextAlign(1, 1);
			g.setFont("Tahoma", 40);
			g.drawString("Activate Any Input.", x+(width/2), y+(height/2));
			g.setFont("Tahoma", 20);
			int off=0;
			for(Controller cont : in.getControllers()){
				off++;
				g.drawString(cont.getName(), x+(width/2), y+(height/2)+(off*25));
			}
			g.setTextAlign(0, 0);
		}else{
			super.renderI(g, in, x, y, width, height);
		}
	}

	@Override
	public void tickI(Input in, AudioEngine ae, float delta, float x, float y, float width, float height) {
		if(listening){
			inputActivated=false;
			for(Controller cont : in.getControllers())checkInput(cont, in);
			checkInput(Input.OS_KEYBOARD, in);
			checkInput(Input.OS_MOUSE, in);
			if(!inputActivated)inputsReleased=true;
		}else{
			super.tickI(in, ae, delta, x, y, width, height);
		}
	}
	
	private void checkInput(Controller cont, Input in){
		cont.poll();
		for(com.thepattybeastlabs.core.input.Component c : cont.getComponents()){
			if(c.getPollData()>0.1){
				inputActivated=true;
				if(inputsReleased){
					keybinds.getSelectedItem().setController(cont);
					keybinds.getSelectedItem().setComponenet(c);
					in.save();
					listening=false;
					return;
				}
			}
		}
	}

	public void onActivate(Component comp, String id) {
		listening=true;
		inputsReleased=false;
	}
	
	

}
