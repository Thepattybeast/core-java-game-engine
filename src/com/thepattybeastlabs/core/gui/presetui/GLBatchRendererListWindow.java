package com.thepattybeastlabs.core.gui.presetui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.graphics.GLBatchRender;
import com.thepattybeastlabs.core.graphics.RenderSection;
import com.thepattybeastlabs.core.gui.Component;
import com.thepattybeastlabs.core.gui.FormButton;
import com.thepattybeastlabs.core.gui.Window;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.Image;

public class GLBatchRendererListWindow extends Window {

	private int index;
	
	public GLBatchRendererListWindow(float x, float y, float width, float height) {
		super(x, y, width, height, "GLBatchRenderer Viewer");
		addComponent(new FormButton("Next", "next", -170, 20, 150, 25));
	}

	@Override
	protected void renderI(Graphics g, Input in, float x, float y, float width, float height) {
		super.renderI(g, in, x, y, width, height);
		g.setColor(Color.WHITE);
		GLBatchRender i = GLBatchRender.RENDERERS.get(index);
		g.drawString((index+1)+"/"+GLBatchRender.RENDERERS.size(), x+20, y+20);
		g.drawString("Vertices: "+i.getVeticeCount(), x+20, y+50);
		g.drawString("Clear On Paint: "+i.isClearOnPaint(), x+20, y+80);
		List<RenderSection> sections = i.getSections();
		for(int j = 0; j < sections.size(); j++){
			g.drawString(sections.get(j).getTex().getAlias()+", " + sections.get(j).getRenderMode(), x+20, y+100+(j*22));
		}
	}

	@Override
	public void onActivate(Component comp, String id) {
		super.onActivate(comp, id);
		if(id.equals("next")){
			index++;
			if(index>=GLBatchRender.RENDERERS.size())index=0;
		}
	}
	
	

}
