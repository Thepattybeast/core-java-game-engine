package com.thepattybeastlabs.core.gui.presetui;

import java.awt.DisplayMode;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;

import com.thepattybeastlabs.core.config.Config;
import com.thepattybeastlabs.core.gui.Button;
import com.thepattybeastlabs.core.gui.Component;
import com.thepattybeastlabs.core.gui.Container;
import com.thepattybeastlabs.core.gui.FormButton;
import com.thepattybeastlabs.core.gui.List;

public class VideoConfigView extends Container {

	private List<String> videoMode, videoResolution;
	
	public VideoConfigView(String id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		videoMode=new List<>("videoMode", 1, 1, -2, 26*3, new List.DefaultItemManager<String>());
		videoMode.setData(new String[]{"Fullscreen", "Borderless", "Windowed"});
		videoMode.setSelelectedIndex(Config.i.getDisplayMode(1));
		videoResolution = new List<>("videoResolution", 1, 26*3+10, -2, -(26*3+46), new List.DefaultItemManager<>());
		ArrayList<String> resolutions = new ArrayList<>();
		int forIndex=0;
		for(DisplayMode d : GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayModes()){
			String s = d.getWidth()+"x"+d.getHeight();
			if(!resolutions.contains(s)){
				if(d.getWidth()==Config.i.getDisplayWidth(1280)&&d.getHeight()==Config.i.getDisplayHeight(720))
					videoResolution.setSelelectedIndex(forIndex);
				resolutions.add(0,s);
				forIndex++;
			}
		}
		videoResolution.setData(resolutions.toArray(new String[0]));
		videoResolution.setSelelectedIndex(resolutions.indexOf(Config.i.getDisplayWidth(1280)+"x"+Config.i.getDisplayHeight(720)));
		addComponent(videoResolution);
		addComponent(videoMode);
		addComponent(new FormButton("Apply", "apply", 1, -(26), -2, 25));
	}

	@Override
	public void onActivate(Component comp, String id) {
		if(id.equals("apply")){
			String[] resSplit = videoResolution.getSelectedItem().split("x");
			Config.i.set("Display", "width", resSplit[0]);
			Config.i.set("Display", "height", resSplit[1]);
			Config.i.set("Display", "mode", Integer.toString(videoMode.getSelelectedIndex()));
			Config.i.applyDisplayModeAndResolution();
		}
	}
	
	

}
