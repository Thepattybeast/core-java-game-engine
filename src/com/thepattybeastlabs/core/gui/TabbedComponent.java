package com.thepattybeastlabs.core.gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.Image;

public class TabbedComponent extends Component {

	public List<TabContainer> tabs = new ArrayList<TabContainer>();
	private int selected = 0, highTab = 0;
	
	public TabbedComponent(String id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
	}

	protected void renderI(Graphics g, Input in, float x, float y, float width, float height) {
		float off = x;
		for(int i = 0; i < tabs.size(); i++){
			off+=renderTabTitle(g, tabs.get(i), off, y, 26, selected==i, highTab==i)+4;
		}
		g.setColor(Color.WHITE);
		g.drawRect(x, y+26, width, height-26);
		if(selected > -1 && selected < tabs.size()){
			TabContainer tab = tabs.get(selected);
			tab.component.render(g, in, x, y+26, width, height-26);
		}
	}
	
	private int renderTabTitle(Graphics g, TabContainer container, float x, float y, 
			float height, boolean selected, boolean highlighted){
		String text = container.getTitle();
		g.setFont((CoreFont)null, (int)height-2);
		int width = g.stringWidth(text);
		g.setColor(selected ? Color.WHITE:Color.BLUE);
		g.fillRect(x, y, width+4, height);
		g.setColor(selected ? Color.BLACK:Color.WHITE);
		g.drawRect(x, y, width+4, height);
		
		g.setColor(selected ? Color.BLACK:(highlighted? Color.YELLOW : Color.WHITE));
		g.drawString(text, x + 2, y);
		return container.width = width+4;
	}

	protected void renderOverlayI(Graphics g, Input in, float x, float y,
			float width, float height) {
		// TODO Auto-generated method stub

	}

	protected void tickI(Input in, AudioEngine ae, float delta, float x,
			float y, float width, float height) {
		float off = x;
		highTab = -1;
		if(highlighted && in.getY() - y < 26)
		for(int i = 0; i < tabs.size(); i++){
			TabContainer tab = tabs.get(i);
			if(in.getX() > off && in.getX() < off+tab.width){
				highTab = i;
			}
			off+=tab.width;
		}
		if(in.isPressed("gui_activate") && highTab > -1 && highTab < tabs.size()){
			selected = highTab;
		}
		if(selected > -1 && selected < tabs.size()){
			TabContainer tab = tabs.get(selected);
			tab.component.tick(in, ae, delta, x, y+26, width, height-26);
		}
	}

	public List<TabContainer> getTabContainers(){
		return tabs;
	}
	
	public void addTab(TabContainer tab){
		if(tab.component==null)throw new NullPointerException("Tab component can't be null");
		if(tab.title==null)throw new NullPointerException("Tab name can't be null");
		tabs.add(tab);
	}
	
	public void addTab(String title, Component comp){
		addTab(new TabContainer(comp, title, null));
	}
	
	public boolean isActivated(Input in) {return false;}
	
	public class TabContainer{
		private Component component;
		private String title;
		private Image icon;
		private int width;
		
		public TabContainer(Component component, String title, Image icon) {
			this.component = component;
			this.title = title;
			this.icon = icon;
		}
		public Component getComponent() {
			return component;
		}
		public void setComponent(Component component) {
			this.component = component;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public Image getIcon() {
			return icon;
		}
		public void setIcon(Image icon) {
			this.icon = icon;
		}
		
	}

}
