package com.thepattybeastlabs.core.gui;

import java.awt.Color;

import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.input.TextInput;

public class TextField extends Component{

	private TextInput tin;
	private boolean showBar;
	private float barTimer;
	private boolean selected;
	
	public TextField(String text, String id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		tin = new TextInput(text);
		setColor("back", Color.BLUE);
		setColor("fore", Color.WHITE);
		setColor("border", Color.WHITE);
		setColor("highBack", Color.CYAN);
		setColor("highFore", Color.BLACK);
		setColor("highBorder", Color.WHITE);
		setColor("selBack", Color.CYAN);
		setColor("selFore", Color.BLACK);
		setColor("selBorder", Color.WHITE);
		setColor("disabledBack", Color.DARK_GRAY);
		setColor("disabledFore", Color.WHITE);
		setColor("disabledBorder", Color.WHITE);
	}

	@Override
	protected void renderI(Graphics g, Input in, float x, float y, float width,
			float height) {
		String state = !enabled?"disabled":highlighted?"high":selected?"sel":"";
		//g.setColor(Color.WHITE);
		if(setColor(g, state+"Back"))g.fillRect(x, y, width, height);
		if(setColor(g, state+"Border"))g.drawRect(x, y, width, height);
		g.setClip(x, y, width, height);
		g.setFont("Tahoma", height-4);
		if(setColor(g, state+"Fore"))g.drawString(tin.getText(), 0, 0);
		int linePos = g.stringWidth(tin.getText(), 0, tin.getCaretPos());
		if(linePos==0)linePos=4;
		if(showBar && selected&&setColor(g, state+"Fore"))g.drawLine(linePos, 2, linePos, height-1);
		g.resetClip();
	}

	protected void renderOverlayI(Graphics g, Input in, float x, float y,
			float width, float height) {
		
	}

	@Override
	protected void tickI(Input in, AudioEngine ae, float delta, float x, float y,
			float width, float height) {
		tin.tick();
		if(barTimer > 70){
			showBar = !showBar;
			barTimer = 0;
		}
		barTimer+=1*delta;
		if(in.isPressed("gui_activate")){
			selected = highlighted;
		}
		tin.setEnabled(selected);
	}
	
	public String getText(){
		return tin.getText();
	}

	public TextField setText(String text){
		tin.setText(text);
		return this;
	}
	
	public boolean isActivated(Input in) {
		return false;
	}

}
