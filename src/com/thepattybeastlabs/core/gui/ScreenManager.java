package com.thepattybeastlabs.core.gui;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;

public class ScreenManager {

	public static final ScreenManager I = new ScreenManager();
	public enum State {ENTERING, EXITING,DISPLAYING};
	
	private State state = State.DISPLAYING;
	private Screen active, setTo;
	
	public void setScreen(Screen screen){
		setTo=screen;
		if(screen!=null){
			screen.onEnter();
		}
		if(active!=null){
			state=State.EXITING;
		}else{
			active=screen;
			setTo=null;
			state=State.ENTERING;
		}
	}
	
	public Screen getScreen(){
		return active;
	}
	
	public State getState(){
		return state;
	}
	
	public void tick(Input in, AudioEngine ae, float delta, float x, float y, float width, float height){
		if(state==State.EXITING){
			if(active.playExitAnim(in, delta)){
				state=State.ENTERING;
				active.onExit();
				active=setTo;
				setTo=null;
			}
		}else if(state==State.ENTERING){
			if(active==null||active.playEnterAnim(in, delta)){
				state=State.DISPLAYING;
				
			}
		}
		if(active!=null)active.tick(in, ae, delta, x, y, width, height);
	}
	
	public void render(Graphics g, Input in, float x, float y, float width, float height){
		if(active!=null)active.render(g, in, x, y, width, height);
	}
	
}
