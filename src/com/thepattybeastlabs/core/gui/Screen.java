package com.thepattybeastlabs.core.gui;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;

public abstract class Screen extends Container{

	protected Screen parent;
	
	public Screen(Screen parent){
		super(0,0,Core.width,Core.height);
		this.parent = parent;
	}
	
	public abstract void onEnter();
	public abstract void onExit();
	public abstract void onActivate(Component comp, String id);
	protected abstract void tickS(Input in, AudioEngine ae, float delta, float x, float y, float width, float height);
	protected abstract void renderS(Graphics g, Input in, float x, float y, float width,float height);
	
	protected void renderI(Graphics g, Input in, float x, float y, float width,
			float height) {
		super.renderI(g, in, x, y, width, height);
		renderS(g, in, x, y, width, height);
	}

	public void tickI(Input in, AudioEngine ae, float delta, float x, float y,
			float width, float height) {
		super.tickI(in, ae, delta, x, y, width, height);
		tickS(in, ae, delta, x, y, width, height);
	}

	public boolean playEnterAnim(Input in, float delta){
		return true;
	}
	
	public boolean playExitAnim(Input in, float delta){
		return true;
	}
	
}
