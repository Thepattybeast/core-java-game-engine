package com.thepattybeastlabs.core.gui;

import java.awt.Color;
import java.util.Arrays;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;

public class List<T> extends Component {

	protected java.util.List<T> list;
	protected ItemManager<T> im;
	protected int highIndex, selIndex = -1;
	protected boolean activated;
	protected float sOff;
	protected ScrollBar verticalScroll;
	
	public List(String id, float x, float y, float width, float height, ItemManager<T> im) {
		super(id, x, y, width, height);
		list = new java.util.ArrayList<T>();
		this.im = im;
		verticalScroll = new ScrollBar(id, x+width, y, ScrollBar.Orientation.RIGHT, (int)height);
		verticalScroll.setLoc(0);
		setColor("back", new Color(0,0,0,100));
		setColor("border", Color.WHITE);
		setColor("fore", Color.WHITE);
		setColor("highFore", Color.YELLOW);
		setColor("highBack", new Color(200, 200, 200, 100));
		setColor("selFore", Color.BLACK);
		setColor("selBack", Color.WHITE);
	}
	
	public List<T> setData(T[] data){
		list.clear();
		list.addAll(Arrays.asList(data));
		return this;
	}

	protected void renderI(Graphics g, Input in, float x, float y, float width,
			float height) {
		if(setColor(g, "back"))g.fillRect(x, y, width, height);
		if(setColor(g, "border"))g.drawRect(x, y, width, height);
		verticalScroll.render(g, in, x+width-20, y, 20, height);
		g.setClip(x, y, width, height);
		float sOff = verticalScroll.getLoc();
		float off = -sOff;
		for(int i = 0; i < list.size(); i++){
			im.renderItem(list.get(i), i, off, width-(verticalScroll.isShowing()?20:0), this, in, g, i==highIndex, selIndex==i);
			off+=im.getItemHeight(list.get(i), i);
		}
		g.resetClip();
	}

	@Override
	protected void renderOverlayI(Graphics g, Input in, float x, float y,
			float width, float height) {
		
	}

	public java.util.List<T> getList() {
		return list;
	}
	
	public ScrollBar getVerticalScroll(){
		return verticalScroll;
	}

	@Override
	protected void tickI(Input in, AudioEngine ae, float delta, float x, float y, float width,
			float height) {
		verticalScroll.tick(in,ae, delta, x+width-20, y, 20, height);
		sOff = verticalScroll.getLoc();
		if(in.getAxis("gui_scroll") !=0){
			verticalScroll.setLoc(sOff-=in.getAxis("gui_scroll")*delta*5);
		}
		float off = -sOff;
		float realOff = 0;
		highIndex = -1;
		for(int i = 0; i < list.size(); i++){
			float ioff =im.getItemHeight(list.get(i), i);
			if(highlighted && in.getY() - y > off && in.getY() - y < off+ioff&&in.getX()-x < width-(verticalScroll.isShowing()?20:0))highIndex = i;
			im.tickItem(list.get(i), i, off, width-20, this, in, ae, delta, highIndex==i, selIndex==i);
			off+=ioff;
			realOff += ioff;
		}
		activated = false;
		if(highlighted && in.isPressed("gui_activate")&&in.getX()-x < width-(verticalScroll.isShowing()?20:0)){
			if(highIndex == selIndex && selIndex!=-1)activated = true;
			selIndex = highIndex;
		}
		verticalScroll.setSize(realOff-height);
	}

	public boolean isActivated(Input in) {
		return activated;
	}
	
	public T getSelectedItem(){
		if(selIndex==-1){
			return null;
		}else{
			return list.get(selIndex);
		}
	}
	
	
	
	public int getHighIndex() {
		return highIndex;
	}

	public int getSelelectedIndex() {
		return selIndex;
	}

	public List<T> setSelelectedIndex(int selIndex) {
		this.selIndex = selIndex;
		return this;
	}



	/**Handles item display and stuff
	 * See AbstractItemManager and DefaultItemManager*/
	public static interface ItemManager<L>{
		public String getText(L item, int index);
		public void renderItem(L item, int index, float y, float width, List<L> items, Input in, Graphics g, boolean high, boolean sel);
		public void tickItem(L item, int index, float y, float width, List<L> items, Input in, AudioEngine ae, float delta, boolean high, boolean sel);
		public float getItemHeight(L item, int index);
	}
	
	public static abstract class AbstractItemManager<L> implements ItemManager<L>{
		public void renderItem(L item, int index, float y, float width, List<L> items, Input in, Graphics g, boolean high, boolean sel){
			g.setFont("Tahoma", 24);
			if(sel){
				if(items.setColor(g, "selBack"))g.fillRect(0, y, width, 26);
				items.setColor(g, "selFore");
			}
			if(high){
				if(items.setColor(g, "highBack"))g.fillRect(0, y, width, 26);
				items.setColor(g, "highFore");
			}else if(!sel){
				items.setColor(g, "fore");
			}
			g.drawString(getText(item, index), 2, y);
		}
		public void tickItem(L item, int index, float y, float width, List<L> items, Input in,
				AudioEngine ae, float delta, boolean highlighted,
				boolean selected) {
		}
		public float getItemHeight(L item, int index){
			return 26;
		}
	}
	
	public static class DefaultItemManager<L> extends AbstractItemManager<L>{
		public String getText(L item, int index) {
			if(item==null)return "null";
			return item.toString();
		}
		
	}

}
