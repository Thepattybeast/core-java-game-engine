package com.thepattybeastlabs.core.gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;

public class Container extends Component {

	public List<Component> components;
	private Color backgroundColor = new Color(0,0,0,0);
	protected Component lastActivated;
	protected String lastActivatedId;
	
	public Container(float x, float y, float width, float height) {
		super(x, y, width, height, Alignment.DEFAULT);
		components = new ArrayList<>();
	}
	
	public Container(String id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		components = new ArrayList<>();
	}
	
	public float posFromPercent(float value, float max, float perMax){
		return (max/perMax)*value;
	}

	protected void renderI(Graphics g, Input in, float x, float y, float width, float height) {
		g.setColor(backgroundColor);
		g.fillRect(x, y, width, height);
		g.setColor(Color.WHITE);
		for(Component comp : components){
			float cx = comp.x;if(cx<0)cx = width+cx;if(comp.getAlign()==Alignment.CENTER)cx = (width/2)+cx;
			float cy = comp.y;if(cy<0)cy = height+cy;if(comp.getAlign()==Alignment.CENTER)cx = (width/2)+cx;
			float cw = comp.width;if(cw<0)cw = width+cw;
			float ch = comp.height;if(ch<0)ch = height+ch;
			comp.render(g, in, cx+x, cy+y, cw, ch);
		}
	}

	public void tickI(Input in, AudioEngine ae, float delta, float x, float y, float width, float height) {
		for(Component comp : components){
			float cx = comp.x;if(cx<0)cx = width+cx;if(comp.getAlign()==Alignment.CENTER)cx = (width/2)+cx;
			float cy = comp.y;if(cy<0)cy = height+cy;if(comp.getAlign()==Alignment.CENTER)cx = (width/2)+cx;
			float cw = comp.width;if(cw<0)cw = width+cw;
			float ch = comp.height;if(ch<0)ch = height+ch;
			comp.tick(in, ae, delta, cx + x, cy + y, cw, ch);
			if(comp.isActivated(in)){
				lastActivated = comp;
				lastActivatedId = comp.getID();
				onActivate(comp, comp.getID());
			}
		}
	}
	
	public Container setBackground(Color color){
		backgroundColor = color;
		return this;
	}
	
	public Container addComponent(Component component){
		components.add(component);
		return this;
	}
	
	public List<Component> getComponents(){
		return components;
	}

	public boolean isActivated(Input in) {
		for(Component comp : components){
			if(comp.isActivated(in)){
				return true;
			}
		}
		return false;
	}

	@Override
	protected void renderOverlayI(Graphics g, Input in, float x, float y,
			float width, float height) {
		for(Component comp : components){
			float cx = comp.x;if(cx<0)cx = posFromPercent(-cx, width, 100);
			float cy = comp.y;if(cy<0)cy = posFromPercent(-cy, height, 100);
			float cw = comp.width;if(cw<0)cw = posFromPercent(-cw, width, 100);
			float ch = comp.height;if(ch<0)ch = posFromPercent(-ch, height, 100);
			//CoreFont.list.get("").drawString(g, 200, 200, cx+"@FU.COM");
			comp.renderOverlay(g, in, cx+x, cy+y, cw, ch);
		}
	}
	
	public void onActivate(Component comp, String id){}
	public Component getLastActivated(){
		return lastActivated;
	}
	
	public void setLastActivatedId(String id){
		lastActivatedId = id;
	}
	
	public String getLastActivatedId(){
		if(lastActivatedId!=null)return lastActivatedId;
		if(getLastActivated()!=null)return getLastActivated().getID();
		return "";
	}

}
