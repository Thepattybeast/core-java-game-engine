package com.thepattybeastlabs.core.gui;

import java.awt.Color;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.input.Input;

public class Window extends Container {

	public static final int borderWidth = 4;
	
	private String title;
	private boolean xSelected, mSelected;
	private int lastX, lastY;
	private boolean dragWind;
	private boolean resizeLower, resizeRight, resizeLeft, resizeUpper;
	private boolean maximized;
	private float mx,my,mw,mh;
	private boolean exitOnActivate;
	private WindowManager wm;
	
	public Window(float x, float y, float width, float height, String title) {
		super(x, y, width, height);
		this.title=title;
		setBackground(Color.BLUE.brighter());
	}
	
	public Window(float width, float height, String title) {
		super(Core.width/2-width/2, Core.height/2-height/2, width, height);
		this.title=title;
		setBackground(Color.BLUE.brighter());
	}

	protected void renderI(Graphics g, Input in, float x, float y, float width,
			float height) {
		
		super.renderI(g, in, x, y, width, height);
		if(wm==null||wm.hasFocus(this))g.setColor(30, 130, 150);
		else g.setColor(40, 90, 100);
		g.fillRect(x, y-20, width, 20);
		g.setColor(Color.WHITE);
		g.setFont("Tahoma", 18);
		g.drawString(title, x+4, y-18);
		g.setTextAlign(2, 0);
		g.setFont("Tahoma", 20);
		if(mSelected)g.setColor(Color.BLACK);
		g.drawString(maximized?"-":"+", x+width-20, y-21);
		if(xSelected)g.setColor(Color.BLACK);
		else g.setColor(Color.WHITE);
		g.drawString("X", x+width-4, y-21);
		g.setTextAlign(0, 0);
		g.setColor(20, 100, 250);
		g.fillRect(x-borderWidth, y-20-borderWidth, borderWidth, height+20 + (borderWidth*2));//left
		g.fillRect(x+width, y-20-borderWidth, borderWidth, height+20 + (borderWidth*2));//right
		g.fillRect(x-borderWidth, y+height, width+(borderWidth*2), borderWidth);//bottom
		g.fillRect(x-borderWidth, y-20-borderWidth, width+(borderWidth*2), borderWidth);//top
	}

	public void tickI(Input in, AudioEngine ae, float delta, float x, float y,
			float width, float height) {
		if(this.x < 0)this.x=0;
		if(this.y < 20)this.y=20;
		if(this.width > Core.width)this.width = Core.width;
		if(this.height > Core.height-20)this.height = Core.height-20;
		if(wm==null||wm.hasFocus(this))
		super.tickI(in, ae, delta, x, y, width, height);
		xSelected = (in.getX() > x+width-18 &&in.getY() > y-18&&in.getX() < x+width-4&&in.getY() < y);
		mSelected = (in.getX() > x+width-34 &&in.getY() > y-18&&in.getX() < x+width-20&&in.getY() < y);
		if(xSelected&&in.isPressed("gui_activate")){
			if(wm!=null)wm.remove(this);
			lastActivatedId = "Exit_Btn";
		}
		if(mSelected&&in.isPressed("gui_activate")){
			if(!maximized){
				mx=x;my=y;mw=width;mh=height;
				maximized = true;
			}else{
				maximized = false;
				this.x = mx; this.y = my;
				this.width = mw;this.height = mh;
			}
		}
		int xm = in.getX()-lastX;
		int ym = in.getY()-lastY;
		if(in.isPressed("gui_activate")){
			if(in.getX() > x &&in.getY() > y-18&&in.getX() < x+width&&in.getY() < y)dragWind = true;
			if(in.getX() > x-borderWidth &&in.getY() > y+height&&in.getX() < 
					x+width+borderWidth&&in.getY() < y +height+ (borderWidth*2))resizeLower = true;//lower
			if(in.getX() > x-borderWidth &&in.getY() > y-20-borderWidth&&in.getX() < 
					x+width+borderWidth&&in.getY() < y-20)resizeUpper = true;//upper
			if(in.getX() > x+width &&in.getY() > y-20-borderWidth&&in.getX() < 
					x+width+borderWidth&&in.getY() < y-20-borderWidth+height+20 + (borderWidth*3))resizeRight = true;//right
			if(in.getX() > x-borderWidth &&in.getY() > y-20-(borderWidth*2)&&in.getX() < 
					x&&in.getY() < y-20-(borderWidth*2)+height+20 + (borderWidth*3))resizeLeft = true;//left
		}else if(!in.isDown("gui_activate")){
			dragWind = false;
			resizeLower = false;resizeRight = false;
			resizeLeft = false;resizeUpper = false;
		}
		if(dragWind){
			this.x+=xm;
			this.y+=ym;
		}
		if(resizeLower){
			this.height +=  ym;
		}
		if(resizeUpper){
			this.height -=  ym;
			this.y+=ym;
		}
		if(resizeRight){
			this.width += xm;
		}
		if(resizeLeft){
			this.width -= xm;
			this.x+=xm;
		}
		lastX = in.getX();
		lastY = in.getY();
		if(maximized){
			this.x = 0;this.y = 20;this.width = Core.width;this.height = Core.height-20;
		}
		if(!maximized){
			if(this.x < 0)this.x = 0;
			if(this.y-20 < 0)this.y = 20;
			if(this.x+this.width > Core.width)this.x = Core.width-width;
			if(this.y+this.height > Core.height)this.y = Core.height-height;
			if(this.width < 100)this.width = 100;
			if(this.height < 30)this.height = 30;
		}
		if(exitOnActivate){
			for(int i = 0; i < components.size(); i++){
				if(components.get(i).isActivated(in)){
					System.out.println("Signal::Triggr::Remove");
					if(wm!=null)wm.remove(this);
				}
			}
		}
		if(wm!=null&&!isFocused()&&in.isPressed("gui_activate")){
			if(in.getX() > x-borderWidth && in.getY() > y-borderWidth-20&&in.getX() < x+width+borderWidth&&in.getY() < y+height+borderWidth){
				wm.bringToFocus(this);
			}
		}
		
	}
	
	public boolean isFocused(){
		return wm==null||wm.hasFocus(this);
	}
	
	public Window setCloseOnActivate(boolean closeOnActivate){
		this.exitOnActivate = closeOnActivate;
		return this;
	}
	
	public boolean getCloseOnActivate(){
		return exitOnActivate;
	}
	
	public Window setWM(WindowManager wm){
		this.wm=wm;
		return this;
	}

	public WindowManager getWm() {
		return wm;
	}
	
}
