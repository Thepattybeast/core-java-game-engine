package com.thepattybeastlabs.core.gui;

import java.awt.Color;

import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.gui.ScrollBar.Orientation;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.input.TextInput;

public class TextBox extends Component{

	private TextInput tin;
	private boolean lineWrap, wordWarp, indentWrappedLines, editable = true;
	private ScrollBar scroll = new ScrollBar(id, -1, -1, Orientation.RIGHT, 0);
	private boolean updated=true;
	
	public TextBox(String text, String id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		tin = new TextInput(text);
	}

	@Override
	protected void renderI(Graphics g, Input in, float x, float y, float width,
			float height) {
		g.setColor(Color.WHITE);
		g.drawRect(x, y, width, height);
		g.setClip(x+2, y+2, width-4, height-4);
		g.setFont("Tahoma", 14);
		String text = tin.getText();
		int begin = 0;
		int yOff = -(int)scroll.getLoc();
		for(int i = 0; i < text.length(); i++){
			if((lineWrap&&g.stringWidth(text, begin, i-begin) > width-14) || text.charAt(i)=='\n'){
				if(text.charAt(i)!='\n')i--;
				if(yOff < height)g.drawString(text, 0, yOff, begin, i-begin);
				yOff+=16;
				if(yOff>height)break;
				begin = i;
			}
		}
		if(yOff<height)g.drawString(text, 0, yOff,begin, text.length() - begin);
		g.resetClip();
		if(updated){
			begin = 0;
			yOff = 0;
			for(int i = 0; i < text.length(); i++){
				if((lineWrap&&g.stringWidth(text, begin, i-begin) > width-14) || text.charAt(i)=='\n'){
					if(text.charAt(i)!='\n')i--;
					yOff+=16;
					begin = i;
				}
			}
			yOff+=16;
			scroll.setSize(yOff-height);
		}
		scroll.render(g, in, x+width-20, y, 20, height);
	}

	@Override
	protected void renderOverlayI(Graphics g, Input in, float x, float y,
			float width, float height) {
		
	}

	@Override
	protected void tickI(Input in, AudioEngine ae, float delta, float x, float y,
			float width, float height) {
		tin.setEnabled(highlighted && editable);
		tin.tick();
		scroll.tick(in,ae,delta, x+width-20, y, 20, height);
	}

	public void flagUpdated(){
		tin.updateCache();
		updated=true;
	}
	
	@Override
	public boolean isActivated(Input in) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public String getText(){
		return tin.getText();
	}

	public TextBox setText(String text){
		tin.setText(text);
		return this;
	}
	
	public TextInput getTextInput(){
		return tin;
	}

	public boolean isEditable() {
		return editable;
	}

	public TextBox setEditable(boolean editable) {
		this.editable = editable;
		return this;
	}

	public boolean isLineWrap() {
		return lineWrap;
	}

	public void setLineWrap(boolean lineWrap) {
		this.lineWrap = lineWrap;
	}

	public boolean isWordWarp() {
		return wordWarp;
	}

	public void setWordWarp(boolean wordWarp) {
		this.wordWarp = wordWarp;
	}

	public boolean isIndentWrappedLines() {
		return indentWrappedLines;
	}

	public void setIndentWrappedLines(boolean indentWrappedLines) {
		this.indentWrappedLines = indentWrappedLines;
	}

}
