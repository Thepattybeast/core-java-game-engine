package com.thepattybeastlabs.core.geom;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

import com.thepattybeastlabs.core.Core;

public class Matrix4 {

	private float[][] data;
	
	public Matrix4(float[][] data){
		if(!(data.length==4&&data[0].length==4)){
			throw new RuntimeException("Invalid Matrix Size: " + data.length);
		}
		setData(data);
	}
	
	public void clear(){
		for(int x = 0; x < data.length; x++){
			for(int y = 0; y < data[0].length; y++){
				if(x==y){
					set(x,y,1f);
				}else{
					set(x,y,0f);
				}
			}
		}
	}
	
	public void set(int x, int y, float data){
		this.data[x][y] = data;
	}
	
	public Matrix4 mul(Matrix4 m){
		float[][] res = new float[4][4];
		for(int x = 0; x < data.length; x++){
			for(int y = 0; y < data[0].length; y++){
				res[x][y] = (data[x][0] * m.data[0][y]+
							 data[x][1] * m.data[1][y]+
							 data[x][2] * m.data[2][y]+
							 data[x][3] * m.data[3][y]);
			}
		}
		return new Matrix4(res);
	}
	
	public Matrix4 mul(float[][] m){
		float[][] res = new float[4][4];
		for(int x = 0; x < data.length; x++){
			for(int y = 0; y < data[0].length; y++){
				res[x][y] = (data[x][0] * m[0][y]+
							 data[x][1] * m[1][y]+
							 data[x][2] * m[2][y]+
							 data[x][3] * m[3][y]);
			}
		}
		return new Matrix4(res);
	}

	public float[][] getData() {
		return data;
	}

	public void setData(float[][] data) {
		this.data = data;
	}
	
	public FloatBuffer asBuffer(){
		FloatBuffer res = BufferUtils.createFloatBuffer(4*4);
		for(int x = 0; x < 4; x++){
			for(int y = 0; y < 4; y++){
				res.put(data[x][y]);
			}
		}
		res.flip();
		return res;
	}
	
	public static Matrix4 translate(float x, float y, float z){
		return new Matrix4(new float[][]{
				new float[]{1f, 0f, 0f ,0},
				new float[]{0f, 1f, 0f ,0},
				new float[]{0f, 0f, 1f ,0},
				new float[]{x, y, z ,1f}
		});
	}
	
	public static Matrix4 rotation(float x, float y, float z){
		x = (float)Math.toRadians(x);y = (float)Math.toRadians(y);z = (float)Math.toRadians(z);
		float[][] rz = new float[][]{
				new float[]{(float)Math.cos(z), -(float)Math.sin(z), 0f ,0f},
				new float[]{(float)Math.sin(z), (float)Math.cos(z), 0f ,0f},
				new float[]{0f, 0f, 1f ,0f},
				new float[]{0f, 0f, 0f ,1f}};
		float[][] rx = new float[][]{
				new float[]{1f, 0f, 0f ,0f},
				new float[]{0f, (float)Math.cos(x), -(float)Math.sin(x) ,0f},
				new float[]{0f, (float)Math.sin(x), (float)Math.cos(x) ,0f},
				new float[]{0f, 0f, 0f ,1f}};
		float[][] ry = new float[][]{
				new float[]{(float)Math.cos(y), 0f, -(float)Math.sin(y) ,0f},
				new float[]{0f, 1f, 0f ,0f},
				new float[]{(float)Math.sin(y), 0f, (float)Math.cos(y) ,0f},
				new float[]{0f, 0f, 0f ,1f}};
		return new Matrix4(rz).mul(new Matrix4(ry).mul(rx));
	}
	
	public static Matrix4 ortho(float left, float right, float bottom, float top, float near, float far){
		System.out.println(right + "x"+bottom);
		return new Matrix4(new float[][]{
				new float[]{2f/right, 0f, 0f ,0f},
				new float[]{0f, -2f/bottom, 0f ,0f},
				new float[]{0f, 0f, 1f ,0f},
				new float[]{-1f, 1f, 0f ,1f}
		});
	}
	
	public String toString(){
		String s = "";
		for(int i = 0; i < data.length; i++){
			s+="[";
			for(int j = 0; j < data[0].length; j++){
				s+=data[i][j] + ((j!=data[0].length-1)?", ":"");
				//s+=data[i][j] + ", ";
			}
			s+="]\n";
		}
		return s;
	}
	
	public static Matrix4 blankProjection(){
		return new Matrix4(new float[][]{
				new float[]{1f, 0f, 0f ,0f},
				new float[]{0f, 1f, 0f ,0f},
				new float[]{0f, 0f, 1f ,0f},
				new float[]{0f, 0f, 0f ,1f}
		});
	}
	
	public static Matrix4 projection(float fov, float aspect, float near, float far){
		float tan = (float)(Math.tan(Math.toRadians(fov/2)));
		float depth = (near-far);
		return new Matrix4(new float[][]{
				new float[]{1.0f/(tan*aspect), 0f, 0f ,0f},
				new float[]{0f, 1.0f/tan, 0f ,0f},
				new float[]{0f, 0f, (-near-far)/depth ,1f},
				new float[]{0f, 0f,  2*far*near/depth,0f}
		});
	}
}
