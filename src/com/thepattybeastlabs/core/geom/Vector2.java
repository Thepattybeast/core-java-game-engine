package com.thepattybeastlabs.core.geom;

public class Vector2 {

	public float x, y;

	public Vector2(){}
	
	public Vector2(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public Vector2 sub(Vector2 v){
		return new Vector2(x-v.x, y-v.y);
	}
	
	public Vector2 mul(Vector2 v){
		return new Vector2(x+v.x, y+v.y);
	}
	public Vector2 add(Vector2 v){
		return new Vector2(x*v.x, y*v.y);
	}
	public Vector2 div(Vector2 v){
		return new Vector2(x/v.x, y/v.y);
	}
	
	public String str(){
		return x + ", "+y;
	}

	public String toString() {
		return "Vector2 [x=" + x + ", y=" + y + "]";
	}
	
}
