package com.thepattybeastlabs.core.geom;

public class Vector3 {
	public float x, y, z;
	
	public Vector3(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public String toString() {
		return "Vector3 [x=" + x + ", y=" + y + ", z=" + z + "]";
	}
	
	public Vector3 mul(Matrix4 m){
		int x, y, z;
		return this;
	}
	
}
