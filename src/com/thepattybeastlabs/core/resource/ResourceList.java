package com.thepattybeastlabs.core.resource;

import java.io.File;
import java.util.ArrayList;


import com.thepattybeastlabs.core.Core;

public class ResourceList<T extends LoadableResource> {
	
	private ArrayList<T> list = new ArrayList<>();
	private String searchPath;
	private Class<T> resourceType;
	public int maxLoadSize = 1024*1024*10;
	private ArrayList<T> loadQue = new ArrayList<>();
	private ArrayList<File> failed = new ArrayList<>();
	
	public ResourceList(Class<T> resourceType){
		this.resourceType = resourceType;
		Core.lists.add(this);
	}
	
	public ResourceList(String searchPath, Class<T> resourceType){
		this(resourceType);
		this.searchPath = searchPath.replace("/", File.separator).replace("//", File.separator);
	}
	
	public void loadResource(T resource, String alias){
		if(alias!=null)resource.setAlias(alias);
		try {
			if(Core.hasOpenGLContext()){
				resource.load();
				list.add(resource);
			}else{
				loadQue.add(resource);
			}
		} catch (Exception e) {
			e.printStackTrace();
			failed.add(resource.getPath());
		}
	}
	
	public void tickLoad(){
		if(loadQue.size() > 0){
			T res = loadQue.get(loadQue.size()-1);
			loadQue.remove(loadQue.size()-1);
			try {
				res.load();
				list.add(res);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean load(String path){
		return load(path, null);
	}
	
	public boolean load(String path, String alias){
		if(alias==null)alias=path;
		if(searchPath !=null && !searchPath.isEmpty()){
			path = searchPath + File.separator+path;
		}
		if(new File(path).isDirectory()){
			File[] files = new File(path).listFiles();
			for(int i = 0; i < files.length; i++){
				load(files[i].getPath().replace(searchPath + File.separator, ""));
			}
			return true;
		}
		if(Core.doResourceLoadLogs)System.out.println("Loading Resource: " + path +", alias: " + alias);
		long st = System.currentTimeMillis();
		try {
			loadResource((resourceType.getConstructor(File.class).newInstance(new File(path))), alias);
		} catch (Exception e) {
			failed.add(new File(path));
			e.printStackTrace();
			return false;
		}
		if(Core.doResourceLoadLogs)System.out.println("Loaded resource in " + (System.currentTimeMillis() - st) + "ms.");
		return false;
	}
	
	public T getResource(int index){
		return list.get(index);
	}
	
	public boolean isLoadQueEmpty(){
		return loadQue.size() == 0;
	}
	
	public T get(String path){
		String orig = path;
		path = path.replace("/", File.separator);
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).getAlias().equalsIgnoreCase(path))return list.get(i);
		}
		for(int i = 0; i < loadQue.size(); i++){
			if(loadQue.get(i).getAlias().equalsIgnoreCase(path))return null;
		}
		for(int i = 0; i < failed.size(); i++){
			if(failed.get(i).getPath().equalsIgnoreCase(new File(path).getPath()))return null;
		}
		if(Core.doDynamicLoading){
			if(Core.doDynamicLoadWarnings)System.err.println("Warning: Dynamically loading '"+path+"' do not use in production.");
			if(load(orig)){
				return list.get(list.size()-1);
			}else{
				return null;
			}
		}
		return null;
	}
	
	public void destory(){
		Core.lists.remove(this);
		for(int i = 0 ; i < list.size(); i++){
			try {
				list.get(i).destroy();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String getLoadPath() {
		return searchPath;
	}

	public void setLoadPath(String loadPath) {
		searchPath = loadPath;
	}
}
