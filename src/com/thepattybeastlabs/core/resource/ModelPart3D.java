package com.thepattybeastlabs.core.resource;

import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.geom.Vector3;

public class ModelPart3D {

	public Vector3[] vectors;
	public int[] indices;
	public Vector2[] uv;
	public String id;
	
	public ModelPart3D(Vector3[] vectors, int[] indices, Vector2[] uv, String texture, String id) {
		this.vectors = vectors;
		this.indices = indices;
		this.uv=uv;
		this.id = id;
	}
	
	public Vector3[] getVectors() {
		return vectors;
	}
	public void setVectors(Vector3[] vectors) {
		this.vectors = vectors;
	}
	public int[] getIndices() {
		return indices;
	}
	public void setIndices(int[] indices) {
		this.indices = indices;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Vector2[] getUv() {
		return uv;
	}

	public void setUv(Vector2[] uv) {
		this.uv = uv;
	}
	
}
