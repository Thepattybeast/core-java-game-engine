package com.thepattybeastlabs.core.resource.animation;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;

import com.sun.imageio.plugins.gif.GIFImageMetadata;

public class GIFAnimationLoader {

	private List<Frame> frames;
	
	public GIFAnimationLoader(File path) throws Exception{
		long st = System.currentTimeMillis();
		ImageReader reader = ImageIO.getImageReadersBySuffix("gif").next();
		ImageInputStream in = ImageIO.createImageInputStream(path);
		reader.setInput(in);
		frames = new ArrayList<>();
		BufferedImage fBuf = new BufferedImage(reader.getWidth(0), reader.getHeight(0), BufferedImage.TYPE_4BYTE_ABGR);
		Graphics fBufG=fBuf.getGraphics();
		
		try{
			while(true){
				GIFImageMetadata meta = (GIFImageMetadata)reader.getImageMetadata(frames.size());
				BufferedImage fi = reader.read(frames.size());
				if(meta.disposalMethod==2)clearImage(fBuf);
				fBufG.drawImage(fi, meta.imageLeftPosition, meta.imageTopPosition, null);
				Frame f = new Frame(copyImage(fBuf), meta.delayTime*10);
				frames.add(f);
			}
		}catch(IndexOutOfBoundsException e){}//read frames until this exception is thrown indicating there are no more frames
		in.close();
		System.out.println((System.currentTimeMillis()-st)+" ms.");
	}
	
	private BufferedImage copyImage(BufferedImage src){
		BufferedImage image = new BufferedImage(src.getWidth(), src.getHeight(), src.getType());
		for(int x= 0; x < src.getWidth(); x++){
			for(int y= 0; y < src.getHeight(); y++){
				image.setRGB(x, y, src.getRGB(x, y));
			}
		}
		return image;
	}
	
	private void clearImage(BufferedImage src){
		for(int x= 0; x < src.getWidth(); x++){
			for(int y= 0; y < src.getHeight(); y++){
				src.setRGB(x, y, 0x00000000);
			}
		}
	}
	
	public Frame[] getFrames(){
		return frames.toArray(new Frame[0]);
	}
	
}
