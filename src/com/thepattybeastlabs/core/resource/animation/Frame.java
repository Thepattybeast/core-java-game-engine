package com.thepattybeastlabs.core.resource.animation;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.thepattybeastlabs.core.resource.Image;

public class Frame {

	private BufferedImage image;
	private int x, y, width, height;
	private Icon iconCache =null;
	private int delay  =20;
	private Image glImage;
	
	public Frame(BufferedImage image, int x, int y, int width, int height, int ticks) {
		this.image = image;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.delay = ticks;
	}
	
	public Frame(BufferedImage img, int delay) throws Exception{
		this.image = img;
		this.delay = delay;
		glImage = new Image(img);
	}
	
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		iconCache=null;
		this.image = image;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		iconCache=null;
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		iconCache=null;
		this.y = y;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		iconCache=null;
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		iconCache=null;
		this.height = height;
	}
	public int getDelay() {
		return delay;
	}
	public void setDelay(int delay) {
		this.delay = delay;
	}
	
	public void save(DataOutputStream out)throws IOException{
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		ImageIO.write(makeImage(), "png", bOut);
		out.writeUTF("png");
		out.writeInt(bOut.size());
		out.write(bOut.toByteArray());
		out.writeInt(getDelay());
		bOut.close();
	}
	
	/**Generate a TYPE_4BYTE_ABGR BufferedImage representing this frame with the specified width and height*/
	public BufferedImage makeImage(int width, int height){
		BufferedImage res = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = res.getGraphics();
		g.drawImage(getImage(), 0, 0, width, height,this.x,this.y,this.x+this.width,this.y+this.height, null);
		return res;
	}
	
	public BufferedImage makeImage(){
		return makeImage(width, height);
	}
	
	public Icon getIcon(){
		if(iconCache!=null){
			return iconCache;
		}else{
			return iconCache=new ImageIcon(makeImage(64, 64));
		}
	}
	public Image getGlImage() {
		return glImage;
	}
	public void setGlImage(Image glImage) {
		this.glImage = glImage;
	}
	
}
