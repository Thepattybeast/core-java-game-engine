package com.thepattybeastlabs.core.resource.animation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.resource.LoadableResource;

public class Animation extends LoadableResource {

	private Frame[] frames;
	private int frame;
	private float frameDelay;
	
	public Animation(File path) throws Exception {
		super(path);
		if(!path.exists())throw new FileNotFoundException(path.getAbsolutePath() + " does not exist.");
		if(path.getName().toLowerCase().endsWith(".gif")){
			frames = new GIFAnimationLoader(path).getFrames();
		}else{
			DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(path)));
			byte[] header = new byte[4];
			in.readFully(header);
			if(!new String(header, "UTF-8").equals("CJAI")){
				in.close();
				throw new IOException("Invalid header: is this an animation image (.cai)?");
			}
			int version = in.readInt();
			if(version > 3){
				in.close();
				throw new IOException("This animation uses an newer version of the file format that is not supported ("+version+" > 3)");
			}
			int frameCount = in.readInt();
			frames = new Frame[frameCount];
			for(int i = 0; i < frameCount; i++){
				String imgFormat = in.readUTF();
				int imgLen = in.readInt();
				System.out.println("Frame: "+imgFormat + ", " + imgLen);
				byte[] imgB = new byte[imgLen];
				in.read(imgB);
				ByteArrayInputStream imgBs = new ByteArrayInputStream(imgB);
				BufferedImage img = ImageIO.read(imgBs);
				imgBs.close();
				int delay = in.readInt();
				frames[i] = new Frame(img, delay);
				//JOptionPane.showMessageDialog(null, new JLabel(delay+"", new ImageIcon(img), 0));
			}
			
			in.close();
		}
	}
	
	public void render(Graphics g, float x, float y, float width, float height){
		g.drawImage(frames[frame].getGlImage(), x, y, width, height);
	}
	
	public void tick(float delta){
		if(frameDelay > frames[frame].getDelay()){
			frame++;
			if(frame>=frames.length)frame=0;
			frameDelay=0;
		}else{
			frameDelay+=(1000/Core.deltaTickrate)*delta;
		}
	}

	public void load() throws Exception {
		for(int i = 0; i < frames.length; i++){
			frames[i].getGlImage().load();
		}
	}
	
	public int getCurrentFrame(){
		return frame;
	}
	
	public int getFrameCount(){
		return frames.length;
	}

	public void destroy() throws Exception {
		for(int i = 0; i < frames.length; i++){
			frames[i].getGlImage().destroy();
		}
	}

}
