package com.thepattybeastlabs.core.resource;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ImageSheet extends LoadableResource {

	private Image base;
	private Map<String, Image> images;
	
	public ImageSheet(File path) throws Exception {
		super(path);
		images = new HashMap<String, Image>();
	}
	
	public ImageSheet(Image image) throws Exception {
		super();
		images = new HashMap<String, Image>();
		base = image;
	}

	public void load() throws Exception {
		base = new Image(path);
		base.load();	
		
	}

	public Image bind(String id, int x, int y, int width, int height){
		try {
			Image i = new Image(base.getProps(), x, y, width, height);
			images.put(id, i);
			return i;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Image get(String  id){
		return images.get(id);
	}
	
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

}
