package com.thepattybeastlabs.core.resource;

import java.io.File;

public abstract class LoadableResource {
	
	protected File path;
	protected String alias;
	
	public LoadableResource(File path) throws Exception{
		this.path = path;
		alias =path.getPath();
	}
	
	public LoadableResource() throws Exception{
	}
	
	public abstract void load() throws Exception;
	public abstract void destroy() throws Exception;
	
	public File getPath(){
		return path;
	}
	
	public void setAlias(String alias){
		this.alias = alias;
	}
	
	public String getAlias(){
		return alias;
	}
	
}
