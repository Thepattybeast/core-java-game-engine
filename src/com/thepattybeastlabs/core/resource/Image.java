package com.thepattybeastlabs.core.resource;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.resource.ResourceLoadingUtils.TexProps;
import com.thepattybeastlabs.core.utils.ReadOnlyList;

public class Image extends LoadableResource {
	
	public static final ResourceList<Image> list = new ResourceList<>("res/textures", Image.class);
	private static List<Image> iList = new ArrayList<>();
	public static final ReadOnlyList<Image> IMAGES = new ReadOnlyList<>(iList);
	public static long size = 0;
	
	public static void recreateGL(){
		for(Image i : IMAGES){
			try {
				i.load();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void destroyGL(){
		for(Image i : IMAGES){
			try {
				i.unload();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private TexProps props;
	private BufferedImage image;
	private int width, height;
	private int x, y;
	private boolean child=false;
	
	public Image(File path) throws Exception {
		super(path);
		if(!path.exists())throw new FileNotFoundException(path.getAbsolutePath() + " does not exist");
		image = ImageIO.read(path);
		iList.add(this);
	}
	
	public Image(TexProps p, int x,int y,int  width, int  height) throws Exception{
		super();
		props = p;
		this.x = x;
		this.y =y;
		this.width = width;
		this.height = height;
		iList.add(this);
		child = true;
	}
	
	public Image(BufferedImage image) throws Exception {
		this.image = image;
		iList.add(this);
	}
	
	public void load() throws Exception{
		if(child)return;
		if(image!=null){
			
			width = image.getWidth();
			height = image.getHeight();
			props = ResourceLoadingUtils.loadTexture(image);
		}else{		
			throw new NullPointerException("Image source is null.");
		}
	}

	private void unload(){
		Core.gl.glDeleteTextures(props.getId());
	}
	
	public void destroy() throws Exception {
		unload();
		iList.remove(this);
	}

	public File getPath() {
		return path;
	}
	
	public void draw(Graphics g, int x, int y, int width, int height){
		g.drawImage(this, x, y, width, height);
	}
	
	public void draw(Graphics g, int x, int y, int width, int height, int imgX, int imgY, int imgWidth, int imgHeight){
		g.drawImage(this, x, y, width, height, imgX, imgY, imgWidth, imgHeight);
	}
	
	public void draw(Graphics g, int x, int y){
		this.draw(g, x, y, width, height);
	}
	
	public float getWidth(){
		return width;
	}
	
	public float getHeight(){
		return height;
	}
	
	/**Returns the width of the texture loaded into video memory*/
	public float getRealWidth(){
		return props.getWidth();
	}
	/**Returns the height of the texture loaded into video memory*/
	public float getRealHeight(){
		return props.getHeight();
	}

	public void bind() {
		Core.gl.glBindTexture(Core.gl.GL_TEXTURE_2D, props.getId());
	}
	
	public int getId(){
		return props.getId();
	}

	public TexProps getProps() {
		return props;
	}

	public float getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public float getLeftUV(float x){
		return (x+getX()+0.5f)/getRealWidth();
	}
	
	public float getTopUV(float y){
		return (y+getY()+0.5f)/getRealHeight();
	}
	
	
	public float getRightUV(float x, float width){
		return (x+getX()+width-0.5f)/getRealWidth();
	}
	
	public float getBottomUV(float y, float heigth){
		return (y+getY()+heigth-0.5f)/getRealHeight();
	}


}
