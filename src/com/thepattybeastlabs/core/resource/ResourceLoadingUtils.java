package com.thepattybeastlabs.core.resource;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;

import org.lwjgl.BufferUtils;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.geom.Vector3;
public class ResourceLoadingUtils {
	
	public static int texMinFilter = GL_LINEAR, texMagFilter = GL_LINEAR;
	
	public static int loadShader(String data, int type){
		int shader = Core.gl.glCreateShader(type);
		if(shader==0)throw new RuntimeException("glCreateShader() Failed");
		Core.gl.glShaderSource(shader, data);
		Core.gl.glCompileShader(shader);
		if(Core.gl.glGetShaderi(shader, GL_INFO_LOG_LENGTH)>1){
			System.out.println((type==GL_FRAGMENT_SHADER? "Fragment":"Vertex") + " Shader Log:");
			System.out.println(Core.gl.glGetShaderInfoLog(shader, Core.gl.glGetShaderi(shader, GL_INFO_LOG_LENGTH)));
			if(Core.gl.glGetShaderi(shader, GL_COMPILE_STATUS)==GL_FALSE)System.exit(0);
		}
		return shader;
	}
	
	public static int loadProgram(int... shaders){
		int prog = Core.gl.glCreateProgram();
		for(int i = 0; i < shaders.length; i++){
			glAttachShader(prog, shaders[i]);
		}
		Core.gl.glLinkProgram(prog);
		Core.gl.glValidateProgram(prog);
		if(Core.gl.glGetProgrami(prog, GL_LINK_STATUS)==GL_FALSE){
			System.out.println("Error Linking Program");
			System.out.println(Core.gl.glGetProgramInfoLog(prog, 1024));
			System.exit(0);
		}
		if(Core.gl.glGetProgrami(prog, GL_VALIDATE_STATUS)==GL_FALSE){
			System.out.println("Error Validating Program");
			System.out.println(Core.gl.glGetProgramInfoLog(prog, 1024));
			System.exit(0);
		}
		return prog;
	}
	
	public static TexProps loadTexture(BufferedImage image) throws IOException{
		if(image==null){
			throw new NullPointerException("image==null");
		}
		int resW = image.getWidth()*1, resH =image.getHeight()*1;
		if(Core.forcePowTwoTex){
			resW = toPowerOfTwo(image.getWidth(), image.getHeight());
			resH = resW;
		}
		Image.size+=(resW*resH*4);
		ByteBuffer b = null;
		try{
			b = BufferUtils.createByteBuffer(((resW * resH)*4));
		}catch(OutOfMemoryError e){
			JOptionPane.showMessageDialog(null, "Error allocating texture buffer: Out of memory.\n"
					+"Size: " + readableBytes((resW * resH)*4) + "\n"
					+"Width: " + image.getWidth()+"\nHeight: " + image.getHeight()+
					"\nAlocatted: " + readableBytes(Runtime.getRuntime().totalMemory()) + " (Max: " + readableBytes(Runtime.getRuntime().maxMemory()) + ")"
					+"\nTry making the texture smaller."
					, "Out Of Memory", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return new TexProps(0, 1, 1);
		}
		for(int y = 0; y < resH; y++){
			for(int x = 0;x < resW; x++){
				if(y < image.getHeight() && x < image.getWidth()){
					Color c = new Color(image.getRGB(x, y), true);
					b.put((byte)c.getRed());
					b.put((byte)c.getGreen());
					b.put((byte)c.getBlue());
					b.put((byte)c.getAlpha());
				}else{
					b.put((byte)0);b.put((byte)0);
					b.put((byte)0);b.put((byte)0);
				}
			}
		}
		b.flip();
		int id = Core.gl.glGenTextures();
		Core.gl.glBindTexture(Core.gl.GL_TEXTURE_2D, id);
		//glTexParameteri(GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL_TRUE);
		Core.gl.glTexImage2D(Core.gl.GL_TEXTURE_2D, 0, Core.gl.GL_RGBA, resW, resH, 0, Core.gl.GL_RGBA, Core.gl.GL_UNSIGNED_BYTE, b);
		Core.gl.glTexParameteri(Core.gl.GL_TEXTURE_2D, Core.gl.GL_TEXTURE_MAG_FILTER, texMagFilter);
		Core.gl.glTexParameteri(Core.gl.GL_TEXTURE_2D, Core.gl.GL_TEXTURE_MIN_FILTER, texMinFilter);
		float max = Core.gl.glGetFloat(0x84FF);
		
		//System.out.println("Filter: " + max);
		Core.gl.glTexParameterf(Core.gl.GL_TEXTURE_2D, 0x84FE, max);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
	
		b.clear();
		Core.gl.glBindTexture(0, Core.gl.GL_TEXTURE_2D);
		return new TexProps(id, resW, resH);
	}
	
	public static ModelPart3D loadObj(File objFile){
		try {
			Scanner s = new Scanner(objFile);
			ArrayList<Vector3> vertices = new ArrayList<>();
			ArrayList<Integer> indices = new ArrayList<>();
			ArrayList<Vector2> uvCords = new ArrayList<>();
			while(s.hasNext()){
				String line = s.nextLine();
				//
				if(!line.startsWith("#")){
					String[] tokens = line.replaceAll(" +", " ").split(" ");
					if(tokens[0].equals("v")){
						vertices.add(new Vector3(Float.parseFloat(tokens[1]), 
								Float.parseFloat(tokens[2]), Float.parseFloat(tokens[3])));
					}else if(tokens[0].equals("f")){
						if(tokens.length!=4){
							//throw new RuntimeException("Invalid Model: please use triangles");
						}
						//System.out.println(line);
						indices.add(Integer.parseInt(tokens[1].split("/")[0]));
						indices.add(Integer.parseInt(tokens[2].split("/")[0]));
						indices.add(Integer.parseInt(tokens[3].split("/")[0]));
					}else if(tokens[0].equals("vt")){
						uvCords.add(new Vector2(Float.parseFloat(tokens[1]),Float.parseFloat(tokens[2])));
					}
				}	
			}
			s.close();
			int[] res = new int[indices.size()];
			for(int i = 0; i < indices.size(); i++){
				res[i] = indices.get(i);
			}
			System.out.println(res.length + " vectors");
			return new ModelPart3D(vertices.toArray(new Vector3[0]), res, uvCords.toArray(new Vector2[0]),"", objFile.getName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static int toPowerOfTwo(int width, int height){
		int size = width;if(height>size)size = height;
		double exp = (Math.log(size) / Math.log(2));
		if(exp > (int)exp)exp = (int)exp + 1;
		return (int)Math.pow(2, (int)exp);
	}
	
	public static class TexProps{
		private int id, width, height;
		
		public TexProps(int id, int width, int height) {
			this.id = id;
			this.width = width;
			this.height = height;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getWidth() {
			return width;
		}

		public void setWidth(int width) {
			this.width = width;
		}

		public int getHeight() {
			return height;
		}

		public void setHeight(int height) {
			this.height = height;
		}
		
	}
	
	public static String readableBytes(long byteCount){
		if(byteCount==0)return "0 B";
		int exp = (int) (Math.log(byteCount) / Math.log(1024));
		if(exp-1 < 0)return "";
	    String pre = ("KMGTPE").charAt(exp-1) + ("");
	    return String.format("%.1f %sB", byteCount / Math.pow(1024, exp), pre);
	}
	
}
