package com.thepattybeastlabs.core.audio;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Vector;

import javax.sound.sampled.AudioFileFormat.Type;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.lwjgl.BufferUtils;

import static org.lwjgl.openal.AL10.*;

import com.thepattybeastlabs.core.resource.LoadableResource;
import com.thepattybeastlabs.core.resource.ResourceList;


public class Sound extends LoadableResource{

	public static final ResourceList<Sound> list = new ResourceList<Sound>("res/audio", Sound.class);
	private int bufId;
	private int sourceId;
 	
	public Sound(File path) throws Exception {
		super(path);
		
	}

	public void load() throws Exception {
		bufId = alGenBuffers();
		AudioInputStream in = AudioSystem.getAudioInputStream(path);
		for(Type type : AudioSystem.getAudioFileTypes()){
			System.out.println(type.getExtension());
		}
		int bitSize = in.getFormat().getSampleSizeInBits();
		int channels = in.getFormat().getChannels();
		int format = AL_FORMAT_MONO8;
		if(channels==1 && bitSize==8){
			format=AL_FORMAT_MONO8;
		}else if(channels==1 && bitSize==16){
			format=AL_FORMAT_MONO16;
		}else if(channels==2 && bitSize==8){
			format=AL_FORMAT_STEREO8;
		}else if(channels==2 && bitSize==16){
			format=AL_FORMAT_STEREO16;
		}else{
			System.out.println("Format Unsupported");
			throw new UnsupportedAudioFileException("Unsupported bit size or channel count.");
		}
		ByteBuffer buf = BufferUtils.createByteBuffer(in.available());
		byte[] array = new byte[in.available()];
		in.read(array);
		buf.put(array);
		buf.flip();
		in.close();
		//System.out.println(in.getFormat().getSampleRate() + ", " + bitSize + ", " + channels);
		alBufferData(bufId, format, buf, (int)in.getFormat().getSampleRate());
		buf.clear();
		
		sourceId = alGenSources();
		alSourcei(sourceId, AL_BUFFER, bufId);
	}
	
	public Sound(int bufferID)throws Exception{
		bufId = bufferID;
		sourceId = alGenSources();
		alSourcei(sourceId, AL_BUFFER, bufId);
	}

	public void destroy() throws Exception {
		alDeleteSources(sourceId);
		alDeleteBuffers(bufId);
	}
	
	/**Will return a new source using the same buffer, deleting this sound will also*/
	public Sound clone(){
		try {
			return new Sound(bufId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public int getID(){
		return sourceId;
	}

}
