package com.thepattybeastlabs.core.audio;

import static org.lwjgl.openal.AL10.*;

import org.lwjgl.openal.ALContext;

public class AL10AudioEngine implements AudioEngine {

	private ALContext al;
	
	public void play(Sound sound) {
		if(sound==null)return;
		alSourcei(sound.getID(), AL_LOOPING, 0);
		alSourcePlay(sound.getID());
	}

	public void pause(Sound sound) {
		if(sound==null)return;
		alSourcePause(sound.getID());
	}

	public void stop(Sound sound) {
		if(sound==null)return;
		alSourceStop(sound.getID());
	}

	public void loop(Sound sound) {
		if(sound==null)return;
		alSourcei(sound.getID(), AL_LOOPING, 1);
		alSourcePlay(sound.getID());
	}

	public void setPos(Sound sound, float x, float y, float z) {
		if(sound==null)return;
		alSource3f(sound.getID(), AL_POSITION, x, y, z);
	}

	public void setPos(float x, float y, float z) {
		alListener3f(AL_POSITION, x, y, z);
	}
	public void setVel(Sound sound, float x, float y, float z) {
		if(sound==null)return;
		alSource3f(sound.getID(), AL_VELOCITY, x, y, z);
	}
	public void setVel(float x, float y, float z) {
		alListener3f(AL_VELOCITY, x, y, z);
	}

	public void setVolume(Sound sound, float volume) {
		if(sound==null)return;
		alSourcef(sound.getID(), AL_GAIN, volume);
	}

	public void setVolume(float volume) {
		alListenerf(AL_GAIN, volume);
	}

	public void create() throws Exception {
		al = ALContext.create();
	}

	@Override
	public void destroy() throws Exception {
		al.getDevice().destroy();
		al.destroy();
	}

	public String getName() {
		return "OpenAL Version " + alGetString(AL_VERSION);
	}

}
