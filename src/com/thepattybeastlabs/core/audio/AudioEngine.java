package com.thepattybeastlabs.core.audio;

public interface AudioEngine {

	public void play(Sound sound);
	
	public void pause(Sound sound);
	
	public void stop(Sound sound);
	
	public void loop(Sound sound);
	
	public void setPos(Sound s, float x, float y, float z);
	
	public void setPos(float x, float y, float z);
	
	public void setVel(Sound s, float x, float y, float z);
	
	public void setVel(float x, float y, float z);
	
	public void setVolume(Sound s, float volume);
	
	public void setVolume(float volume);

	public void create() throws Exception;
	public void destroy() throws Exception;
	public String getName();
}
