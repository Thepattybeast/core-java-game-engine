package com.thepattybeastlabs.core;

import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.input.Input;

/**Extend the AbstractCoreGame class
 * @see AbstractCoreGame*/
public interface CoreGame {

	public void init(Input in) throws Exception;
	public void postInit() throws Exception;
	public void openGLInit() throws Exception;
	public void tick(Input in, AudioEngine ae, float delta, Screen activeScreen) throws Exception;
	public void render(Graphics g, Input in) throws Exception;
	public void render(Graphics3D g, Input in) throws Exception;
	public void close() throws Exception;
	
	public String getName();
	
}
