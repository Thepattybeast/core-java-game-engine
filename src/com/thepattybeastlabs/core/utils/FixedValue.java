package com.thepattybeastlabs.core.utils;

public class FixedValue implements Value {

	private float value;
	
	public FixedValue(float value) {
		this.value = value;
	}

	public void set(float value){
		this.value=value;
	}
	
	public float get() {
		return value;
	}

}
