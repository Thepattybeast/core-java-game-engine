package com.thepattybeastlabs.core.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.Locale;

import com.thepattybeastlabs.core.console.Console;

public class PrintStreamWrapper extends PrintStream {

	private PrintStream parent;
	private String lntag;
	
	public PrintStreamWrapper(PrintStream parent, String tag){
		super(new ByteArrayOutputStream());
		this.parent = parent;
		lntag="\n";
	}

	@Override
	public void flush() {
		parent.flush();
	}

	@Override
	public void close() {
		parent.close();
	}

	@Override
	public boolean checkError() {
		// TODO Auto-generated method stub
		return parent.checkError();
	}

	@Override
	protected void setError() {
		
	}

	@Override
	protected void clearError() {
		
	}

	@Override
	public void write(int b) {
		// TODO Auto-generated method stub
		parent.write(b);
	}

	@Override
	public void write(byte[] buf, int off, int len) {
		// TODO Auto-generated method stub
		parent.write(buf, off, len);
	}

	@Override
	public void print(boolean b) {
		Console.i().write(Boolean.toString(b));
		parent.print(b);
	}

	public void print(char c) {
		Console.i().write(Character.toString(c));
		parent.print(c);
	}

	@Override
	public void print(int i) {
		// TODO Auto-generated method stub
		Console.i().write(Integer.toString(i));
		parent.print(i);
	}

	@Override
	public void print(long l) {
		Console.i().write(Long.toString(l));
		parent.print(l);
	}

	@Override
	public void print(float f) {
		Console.i().write(Float.toString(f));
		parent.print(f);
	}

	@Override
	public void print(double d) {
		Console.i().write(Double.toString(d));
		parent.print(d);
	}

	@Override
	public void print(char[] s) {
		Console.i().write(new String(s));
		parent.print(s);
	}

	@Override
	public void print(String s) {
		Console.i().write(s);
		parent.print(s);
	}

	@Override
	public void print(Object obj) {
		Console.i().write(obj==null?"null":obj.toString());
		parent.print(obj);
	}

	@Override
	public void println() {
		Console.i().write(lntag);
		parent.println();
	}

	@Override
	public void println(boolean x) {
		Console.i().write(Boolean.toString(x)+lntag);
		parent.println(x);
	}

	@Override
	public void println(char x) {
		Console.i().write(Character.toString(x)+lntag);
		parent.println(x);
	}

	@Override
	public void println(int x) {
		Console.i().write(Integer.toString(x)+lntag);
		parent.println(x);
	}

	@Override
	public void println(long x) {
		Console.i().write(Long.toString(x)+lntag);
		parent.println(x);
	}

	@Override
	public void println(float x) {
		Console.i().write(Float.toString(x)+lntag);
		parent.println(x);
	}

	@Override
	public void println(double x) {
		Console.i().write(Double.toString(x)+lntag);
		parent.println(x);
	}

	@Override
	public void println(char[] x) {
		Console.i().write(new String(x)+lntag);
		parent.println(x);
	}

	@Override
	public void println(String x) {
		Console.i().write(x+lntag);
		parent.println(x);
	}

	@Override
	public PrintStream printf(String format, Object... args) {
		Console.i().write(String.format(format, args));
		return parent.printf(format, args);
	}

	@Override
	public PrintStream printf(Locale l, String format, Object... args) {
		Console.i().write(String.format(l, format, args));
		return parent.printf(l, format, args);
	}

	@Override
	public PrintStream format(String format, Object... args) {
		Console.i().write(String.format(format, args));
		return parent.format(format, args);
	}

	@Override
	public PrintStream format(Locale l, String format, Object... args) {
		Console.i().write(String.format(l, format, args));
		return parent.format(l, format, args);
	}

	@Override
	public PrintStream append(CharSequence csq) {
		Console.i().write(csq.toString());
		return parent.append(csq);
	}

	@Override
	public PrintStream append(CharSequence csq, int start, int end) {
		Console.i().write(csq.subSequence(start, end).toString());
		return parent.append(csq, start, end);
	}

	@Override
	public PrintStream append(char c) {
		Console.i().write(Character.toString(c));
		return parent.append(c);
	}
	
	

}
