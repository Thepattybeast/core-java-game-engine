package com.thepattybeastlabs.core.utils;

import java.util.Random;

public class RandomValue implements Value {

	private float min, max;
	private Random random;
	
	public RandomValue(float min, float max) {
		this(min,max, new Random());
	}
	
	public RandomValue(float min, float max, Random random) {
		this.min = min;
		this.max = max;
		this.random = random;
	}
	
	public float getMin() {
		return min;
	}

	public void setMin(float min) {
		this.min = min;
	}

	public float getMax() {
		return max;
	}

	public void setMax(float max) {
		this.max = max;
	}
	
	public void set(float min, float max){
		setMin(min);
		setMax(max);
	}

	public Random getRandom() {
		return random;
	}

	public void setRandom(Random random) {
		this.random = random;
	}

	public float get() {
		return random.nextFloat()*(max-min)+min;
	}

}
