package com.thepattybeastlabs.core.utils;

public interface Value {

	public float get();
	
}
