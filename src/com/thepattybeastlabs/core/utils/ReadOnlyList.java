package com.thepattybeastlabs.core.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.stream.Stream;

public class ReadOnlyList<T> implements Iterable<T> {

	private List<T> list;
	
	public ReadOnlyList(List<T> list) {
		this.list=list;
	}
	
	@Override
	public Iterator<T> iterator() {
		return list.iterator();
	}

	public int size(){
		return list.size();
	}
	
	public T get(int index){
		return list.get(index);
	}
	
	public boolean contains(Object o){
		return list.contains(o);
	}
	
	public boolean containsAll(Collection<?> c){
		return list.containsAll(c);
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	public int indexOf(Object o){
		return list.indexOf(o);
	}
	
	public int lastIndexOf(Object o){
		return list.lastIndexOf(o);
	}
	
	public ListIterator<T> listIterator(){
		return list.listIterator();
	}
	
	public ListIterator<T> listIterator(int index){
		return list.listIterator(index);
	}
	
	public Spliterator<T> spliterator(){
		return list.spliterator();
	}
	
	public Object[] toArray(){
		return list.toArray();
	}
	
	public T[] toArray(T[] a){
		return list.toArray(a);
	}
	
	public Stream<T> stream(){
		return list.stream();
	}
	
	public Stream<T> parallelStream(){
		return list.parallelStream();
	}

}
