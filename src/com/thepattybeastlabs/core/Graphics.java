package com.thepattybeastlabs.core;

import java.awt.Color;

import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.graphics.GLBatchRender;
import com.thepattybeastlabs.core.resource.Image;

public interface Graphics {
	
	public static final int ALIGN_LEFT = 0;
	public static final int ALIGN_TOP = 0;
	public static final int ALIGN_CENTER = 1;
	public static final int ALIGN_RIGHT = 2;
	public static final int ALIGN_BOTTOM = 2;
	public static final int TEXT_LOAD_HEIGHT = -1;
	
	public void fillRect(float x, float y, float width, float height);
	
	public void fill(Vector2[] shape, int[] indices, float x, float y, float width, float height, Color[] colors);
	
	public void draw(Vector2[] shape, int[] indices, float x, float y, float width, float height, Color[] colors);
	
	public void drawRect(float x, float y, float width, float height);
	
	public void drawLine(float x, float y, float x2, float y2);
	
	public void drawCircle(float x, float y, float width, float height);
	public void fillCircle(float x, float y, float width, float height);
	
	public void drawImage(Image image, float x, float y, float width, float height);
	public void drawImage(Image image, float x, float y, float width, float height, float rot);
	
	public void drawImage(Image image, float x, float y, float width, float height, float imgX, float imgY, float imgWidth, float imgHeight);
	public void drawImage(Image image, float x, float y, float width, float height, float imgX, float imgY, float imgWidth, float imgHeight, float rotation);
	
	
	public void setTextAlign(int horizontal, int vertical);
	
	public void setFont(CoreFont font, float size);
	
	public void setFont(String font, float size);
	
	public void drawString(String text, float x, float y);
	public void drawString(String text, float x, float y, int off, int len);
	public void drawString(String text, float x, float y, Color textBG);
	public void drawString(String text, float x, float y, int off, int len, Color textBG);
	
	public void setColor(Color color);
	
	public void setColor(Color color, int alpha);
	
	public void setColor(int r, int g, int b);
	public void setColor(int r, int g, int b, int a);
	
	public Color getColor();
	
	public String getName();
	
	public void create() throws Exception;
	
	public void destory() throws Exception;
	
	public void finish();
	
	/**Set the orthographic rendering scale. This is done automatically to the window size if (Core.doDynamicResChanges==true).
	 * If that value is false then this can be done manually*/
	public void setRenderingScale(int width, int height);

	public int stringWidth(String text);
	public int stringWidth(String text, int off, int len);
	
	public void setClip(float x, float y, float width, float height);
	
	public void resetClip();
	
	public void rotate(float rot, float width, float height);
	public void rotateTo(float rot, float width, float height);
	public float getRotation();
	
	public void setClearColor(Color color);
	/**@Returns whether or not this graphics object has an underlying batch render */
	public boolean hasBatchRender();
	/**@Returns The underlying batch render or null if one does not exist(see hasBatchRender())*/
	public GLBatchRender getBatchRender();
	
}
