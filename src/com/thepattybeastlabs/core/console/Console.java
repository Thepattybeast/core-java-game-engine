package com.thepattybeastlabs.core.console;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.Util;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.console.commands.DefaultCommandList;
import com.thepattybeastlabs.core.console.commands.DisplayCommandList;
import com.thepattybeastlabs.core.gui.Component;
import com.thepattybeastlabs.core.gui.Container;
import com.thepattybeastlabs.core.gui.FormButton;
import com.thepattybeastlabs.core.gui.TextBox;
import com.thepattybeastlabs.core.gui.TextField;
import com.thepattybeastlabs.core.gui.Window;
import com.thepattybeastlabs.core.input.Input;

public class Console extends Container {
	private static Console i;
	public static Console i(){
		return i;
	}
	
	public static void i(Console newInstance){
		if(newInstance!=null)i=newInstance;
	}
	private TextBox console;
	
	private Window w;
	private TextField cmdField;
	private Map<String,Command> commands = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
	
	public Console(String id, float x, float y, float width, float height) {
		super(id, x, y, width, height);
		addComponent(console = new TextBox("", "console", 1, 1, -1, -27));
		addComponent(cmdField=new TextField("", "cmd", 1, -22, -106, 22));
		addComponent(new FormButton("Execute", "exec", -101, -22, 100, 22));
		console.setEditable(false);
		new DisplayCommandList().registerCommands(this);
		new DefaultCommandList().registerCommands(this);
	}
	
	public Console(String title, int width, int height) {
		this("console",9, 9, -19, -19);
		w=new Window(width, height, title);
		w.addComponent(this);
	}
	
	public void writeLine(String str){
		console.getTextInput().getStringBuilder().append(str);
		console.getTextInput().getStringBuilder().append('\n');
		console.flagUpdated();
	}
	
	public void write(String str){
		console.getTextInput().getStringBuilder().append(str);
		console.getTextInput().updateCache();
	}
	
	public Window getWindow(){
		return w;
	}
	
	public void onActivate(Component comp, String id) {
		if(id.equals("exec")){
			String[] args = Util.parseCommandLine(cmdField.getText());
			Command c = commands.get(args[0]);
			if(c!=null){
				try{
					c.execute(Arrays.copyOfRange(args, 1, args.length));
				}catch(Throwable t){
					writeLine("[ERR] A java exception was encountered running this command, this may be an issue with command or an invalid input.");
					writeLine("[ERR] "+t);
				}
			}else{
				writeLine("Command Not Found: " + args[0]);
			}
			cmdField.setText("");
		}
	}
	
	public void registerCommand(String name, Command command){
		commands.put(name, command);
	}

	@Override
	public boolean isActivated(Input in) {
		// TODO Auto-generated method stub
		return false;
	}

}
