package com.thepattybeastlabs.core.console.commands;

import com.thepattybeastlabs.core.console.Console;

public interface CommandList {

	public void registerCommands(Console console);
	
}
