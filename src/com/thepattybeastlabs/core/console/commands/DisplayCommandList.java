package com.thepattybeastlabs.core.console.commands;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.config.Config;
import com.thepattybeastlabs.core.console.Console;

public class DisplayCommandList implements CommandList {

	public void registerCommands(Console console) {
		console.registerCommand("displaymode", args->{
			if(args.length==3){
				int width = Integer.parseInt(args[0]);
				int height = Integer.parseInt(args[1]);
				int mode = Integer.parseInt(args[2]);
				Config.i.set("Display", "width", width+"");
				Config.i.set("Display", "height", height+"");
				Config.i.set("Display", "mode", mode+"");
				Config.i.applyDisplayModeAndResolution();
			}else{
				System.err.println("Usage: displaymode [width] [height] [mode]");
			}
		});
		console.registerCommand("targetfps", args->{
			Core.targetFPS = Integer.parseInt(args[0]);
		});
	}

}
