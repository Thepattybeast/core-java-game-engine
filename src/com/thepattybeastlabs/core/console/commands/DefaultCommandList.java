package com.thepattybeastlabs.core.console.commands;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.console.Console;
import com.thepattybeastlabs.core.gui.presetui.ConfigDialog;
import com.thepattybeastlabs.core.gui.presetui.GLBatchRendererListWindow;
import com.thepattybeastlabs.core.gui.presetui.ImageListWindow;

public class DefaultCommandList implements CommandList {

	@Override
	public void registerCommands(Console console) {
		console.registerCommand("exit", args->{
			Core.exit();
		});
		console.registerCommand("cfg", args->{
			ConfigDialog.show("Core Engine Configuration");
		});
		console.registerCommand("images", args->{
			ImageListWindow w = new ImageListWindow(Core.width/2-250, Core.height/2-175, 500, 350);
			Core.wm.add(w);
		});
		console.registerCommand("renderers", args->{
			GLBatchRendererListWindow w = new GLBatchRendererListWindow(Core.width/2-250, Core.height/2-175, 500, 350);
			Core.wm.add(w);
		});
	}

}
