package com.thepattybeastlabs.core.console;

public interface Command {

	public void execute(String[] args);
	
}
