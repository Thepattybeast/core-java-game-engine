package com.thepattybeastlabs.core.system;

import java.util.ArrayList;

import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.input.Controller;

public interface System {

	public static final int WIN_MODE_FULLSCREEN = 0;
	public static final int WIN_MODE_BORDERLESS_WINDOW = 1;
	public static final int WIN_MODE_WINDOW = 2;
	
	public void createDisplay(String title, int width, int height, int mode) throws Exception;
	public void destoryDisplay() throws Exception;
	public Key[] getKeys();
	public void tick() throws Exception;
	public void resize();
	public boolean isExitRequested();
	public Vector2 getMouseInfo();
	public Vector2 getDisplayResolution();
	public void updateDisplay();
	public boolean[] getMouseButtons();
	public void setMousePosition(Vector2 pos);
	public ArrayList<Controller> getControllers();
	public int getMWheelPos();
	
	public void setDisplayMode(int wm, int width, int height);
	
	public class Key{
		private int code;
		private String name;
		private boolean pressed;
		public Key(String name, int code) {
			this.code = code;
			this.name = name;
		}
		public int getCode() {
			return code;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public boolean isPressed() {
			return pressed;
		}
		public void setPressed(boolean pressed) {
			this.pressed = pressed;
		}
		
	}
	
}
