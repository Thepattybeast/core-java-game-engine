package com.thepattybeastlabs.core.system;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWvidmode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.input.Controller;
import com.thepattybeastlabs.core.input.glfwJoystickController;

public class LWJGLSystem implements System{
	
	private ArrayList<Key> keys = new ArrayList<>();
	private static Map<String, String> keyText = new HashMap<>();
	private long window;
	private boolean[] mouseButtons;
	private DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
	private DoubleBuffer y = BufferUtils.createDoubleBuffer(1);
	IntBuffer width = BufferUtils.createIntBuffer(1);
	IntBuffer height = BufferUtils.createIntBuffer(1);
	private GLFWErrorCallback error;//keep a refence to the callback so it doesn't get garbage collected
	private GLFWScrollCallback scrollCallback;//There is not way to get scroll wheel without a callback
	private ArrayList<Controller> controllers = new ArrayList<Controller>();
	private double scrollPos;
	private String title;
	
	static{
		keyText.put("MINUS", "-");
		keyText.put("EQUALS", "=");
		keyText.put("LBRACKET", "[");
		keyText.put("RBRACKET", "]");
		keyText.put("RETURN", "ENTER");
		keyText.put("SEMICOLON", ";");
		keyText.put("APOSTROPHE", "'");
		keyText.put("GRAVE", "`");
		keyText.put("BACKSLASH", "\\");
		keyText.put("COMMA", ",");
		keyText.put("PERIOD", ".");
		keyText.put("SLASH", "/");
		keyText.put("MULTIPLY", "*");
		keyText.put("LMENU", "LALT");
		keyText.put("CAPITAL", "CAPS");
		keyText.put("RMENU", "RALT");
	}
	
	public static String keyToReadable(String key){
		String rep = keyText.get(key);
		if(rep!=null)key=rep;
		return key;
	}
	
	public void createDisplay(String title, int width, int height,
			int mode) throws Exception {
		this.title=title;
		java.lang.System.out.println("GLFW Version: " + glfwGetVersionString() + " wm " + mode);
		glfwSetErrorCallback(error = errorCallbackThrow());
		if(glfwInit()==-1){java.lang.System.err.println("glfw init fail");return;}
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_RESIZABLE, GL11.GL_TRUE);
		glfwWindowHint(GLFW_VISIBLE, GL11.GL_FALSE);
		glfwWindowHint(GLFW_DECORATED, mode==1?GL11.GL_FALSE : GL11.GL_TRUE );
		window = glfwCreateWindow(width, height, title, mode==0?glfwGetPrimaryMonitor():NULL, NULL);
		ByteBuffer vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		if(mode!=0)glfwSetWindowPos(window, GLFWvidmode.width(vidMode)/2-width/2, GLFWvidmode.height(vidMode)/2-height/2);
		
		if(window == -1){java.lang.System.err.println("window create fail");return;}
		glfwMakeContextCurrent(window);
		glfwSwapInterval(0);
		GLContext.createFromCurrent();
		glfwSwapBuffers(window);
		glfwSetScrollCallback(window, scrollCallback = new GLFWScrollCallback() {
			public void invoke(long window, double xoffset, double yoffset) {
				scrollPos+=yoffset;
			}
		});
		int mbCount = 0;
		while(true){
			try{
				glfwGetMouseButton(window, mbCount);
			}catch(Exception e){break;};
			mbCount++;
		}
		int jsCount = 0;
		controllers.clear();
		
		while(true){
			try{
				controllers.add(new glfwJoystickController(glfwGetJoystickName(jsCount)
						, glfwGetJoystickButtons(jsCount), glfwGetJoystickAxes(jsCount), jsCount, controllers));
			}catch(Exception e){break;};
			jsCount++;
		}
		mouseButtons = new boolean[mbCount];
		glfwShowWindow(window);
	}
	
	public void setDisplayMode(int mode, int width, int height) {
		Core.destroyGL();
		long lastWindow = window;
		glfwDestroyWindow(lastWindow);
		glfwWindowHint(GLFW_RESIZABLE, GL11.GL_TRUE);
		glfwWindowHint(GLFW_VISIBLE, GL11.GL_FALSE);
		glfwWindowHint(GLFW_DECORATED, mode==1?GL11.GL_FALSE : GL11.GL_TRUE );
		window = glfwCreateWindow(width, height, title, mode==0?glfwGetPrimaryMonitor():NULL, NULL);
		ByteBuffer vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		if(mode!=0)glfwSetWindowPos(window, GLFWvidmode.width(vidMode)/2-width/2, GLFWvidmode.height(vidMode)/2-height/2);
		glfwSetScrollCallback(window, scrollCallback = new GLFWScrollCallback() {
			public void invoke(long window, double xoffset, double yoffset) {
				scrollPos+=yoffset;
			}
		});
		if(window == -1){java.lang.System.err.println("window create fail");return;}
		glfwMakeContextCurrent(window);
		glfwSwapInterval(0);
		GLContext.createFromCurrent();
		glfwSwapBuffers(window);
		
		glfwShowWindow(window);
		Core.recreateGL();
	}
	
	public void updateDisplay(){
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	public void destoryDisplay() {
		glfwDestroyWindow(window);
		glfwTerminate();
	}

	public void tick() {
		try{
			glfwGetJoystickName(controllers.size());
			String name = glfwGetJoystickName(controllers.size());
			
			controllers.add(new glfwJoystickController(glfwGetJoystickName(controllers.size())
						, glfwGetJoystickButtons(controllers.size()), glfwGetJoystickAxes(controllers.size())
						, controllers.size(), controllers));
			java.lang.System.out.println("Controller '"+name+"' Connected");
		}catch(Exception e){}
		for(int i = 0 ; i< keys.size(); i++){
			keys.get(i).setPressed(glfwGetKey(window, keys.get(i).getCode())==GLFW_PRESS);
		}
		for(int i = 0; i < mouseButtons.length;i++){
			mouseButtons[i] = GLFW.glfwGetMouseButton(window, i)==GLFW_PRESS;
		}
	}

	public void resize() {
		
	}

	public boolean isExitRequested() {
		return glfwWindowShouldClose(window)==GL_TRUE;
	}
	public Vector2 getMouseInfo() {
		x.position(0);
		y.position(0);
		glfwGetCursorPos(window, x, y);
		Vector2 ret = new Vector2((float)x.get(), (float)y.get());
		return ret;
	}
	
	public void setMousePosition(Vector2 pos){
		GLFW.glfwSetCursorPos(window, pos.x, pos.y);
	}
	
	public Vector2 getDisplayResolution(){
		width.position(0);
		height.position(0);
		glfwGetWindowSize(window, width, height);
		return new Vector2(width.get(), height.get());
	}

	public Key[] getKeys() {
		return keys.toArray(new Key[]{});
	}

	public boolean[] getMouseButtons() {
		return mouseButtons;
	}
	
	public LWJGLSystem(){
		keys.add(new Key("space", 32));
		keys.add(new Key("'", 39 /* ' */));
		keys.add(new Key(",", 44 /* , */));
		keys.add(new Key("-", 45 /* - */));
		keys.add(new Key(".", 46 /* . */));
		keys.add(new Key("/", 47 /* / */));
		keys.add(new Key("0", 48));
		keys.add(new Key("1", 49));
		keys.add(new Key("2", 50));
		keys.add(new Key("3", 51));
		keys.add(new Key("4", 52));
		keys.add(new Key("5", 53));
		keys.add(new Key("6", 54));
		keys.add(new Key("7", 55));
		keys.add(new Key("8", 56));
		keys.add(new Key("9", 57));
		keys.add(new Key(";", 59));
		keys.add(new Key("=", 61 /* = */));
		keys.add(new Key("A", 65));
		keys.add(new Key("B", 66));
		keys.add(new Key("C", 67));
		keys.add(new Key("D", 68));
		keys.add(new Key("E", 69));
		keys.add(new Key("F", 70));
		keys.add(new Key("G", 71));
		keys.add(new Key("H", 72));
		keys.add(new Key("I", 73));
		keys.add(new Key("J", 74));
		keys.add(new Key("K", 75));
		keys.add(new Key("L", 76));
		keys.add(new Key("M", 77));
		keys.add(new Key("N", 78));
		keys.add(new Key("O", 79));
		keys.add(new Key("P", 80));
		keys.add(new Key("Q", 81));
		keys.add(new Key("R", 82));
		keys.add(new Key("S", 83));
		keys.add(new Key("T", 84));
		keys.add(new Key("U", 85));
		keys.add(new Key("V", 86));
		keys.add(new Key("W", 87));
		keys.add(new Key("X", 88));
		keys.add(new Key("Y", 89));
		keys.add(new Key("Z", 90));
		keys.add(new Key("[", 91 /* [ */));
		keys.add(new Key("\\", 92 /* \ */));
		keys.add(new Key("]", 93 /* ] */));
		keys.add(new Key("`", 96 /* ` */));
		keys.add(new Key("WORLD_1", 161 /* non-US #1 */));
		keys.add(new Key("WORLD_2", 162 /* non-US #2 */));
		keys.add(new Key("escape", 256));
		keys.add(new Key("enter", 257));
		keys.add(new Key("tab", 258));
		keys.add(new Key("back", 259));
		keys.add(new Key("ins", 260));
		keys.add(new Key("del", 261));
		keys.add(new Key("right", 262));
		keys.add(new Key("left", 263));
		keys.add(new Key("down", 264));
		keys.add(new Key("up", 265));
		keys.add(new Key("PAGE_UP", 266));
		keys.add(new Key("PAGE_DOWN", 267));
		keys.add(new Key("HOME", 268));
		keys.add(new Key("END", 269));
		keys.add(new Key("CAPS_LOCK", 280));
		keys.add(new Key("SCROLL_LOCK", 281));
		keys.add(new Key("NUM_LOCK", 282));
		keys.add(new Key("PRINT_SCREEN", 283));
		keys.add(new Key("PAUSE", 284));
		keys.add(new Key("F1", 290));
		keys.add(new Key("F2", 291));
		keys.add(new Key("F3", 292));
		keys.add(new Key("F4", 293));
		keys.add(new Key("F5", 294));
		keys.add(new Key("F6", 295));
		keys.add(new Key("F7", 296));
		keys.add(new Key("F8", 297));
		keys.add(new Key("F9", 298));
		keys.add(new Key("F10", 299));
		keys.add(new Key("F11", 300));
		keys.add(new Key("F12", 301));
		keys.add(new Key("F13", 302));
		keys.add(new Key("F14", 303));
		keys.add(new Key("F15", 304));
		keys.add(new Key("F16", 305));
		keys.add(new Key("F17", 306));
		keys.add(new Key("F18", 307));
		keys.add(new Key("F19", 308));
		keys.add(new Key("F20", 309));
		keys.add(new Key("F21", 310));
		keys.add(new Key("F22", 311));
		keys.add(new Key("F23", 312));
		keys.add(new Key("F24", 313));
		keys.add(new Key("F25", 314));
		keys.add(new Key("KP_0", 320));
		keys.add(new Key("KP_1", 321));
		keys.add(new Key("KP_2", 322));
		keys.add(new Key("KP_3", 323));
		keys.add(new Key("KP_4", 324));
		keys.add(new Key("KP_5", 325));
		keys.add(new Key("KP_6", 326));
		keys.add(new Key("KP_7", 327));
		keys.add(new Key("KP_8", 328));
		keys.add(new Key("KP_9", 329));
		keys.add(new Key("KP_DECIMAL", 330));
		keys.add(new Key("KP_DIVIDE", 331));
		keys.add(new Key("KP_MULTIPLY", 332));
		keys.add(new Key("KP_SUBTRACT", 333));
		keys.add(new Key("KP_ADD", 334));
		keys.add(new Key("KP_ENTER", 335));
		keys.add(new Key("KP_EQUAL", 336));
		keys.add(new Key("LSHIFT", 340));
		keys.add(new Key("LCONTROL", 341));
		keys.add(new Key("LALT", 342));
		keys.add(new Key("LSUPER", 343));
		keys.add(new Key("RSHIFT", 344));
		keys.add(new Key("RCONTROL", 345));
		keys.add(new Key("RALT", 346));
		keys.add(new Key("RSUPER", 347));
		keys.add(new Key("MENU", 348));
	}

	@Override
	public ArrayList<Controller> getControllers() {
		return controllers;
	}

	public int getMWheelPos(){
		return (int)scrollPos;
	}
}
