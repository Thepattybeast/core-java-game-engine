package com.thepattybeastlabs.core.config;

import java.io.File;

import com.thepattybeastlabs.core.Core;

public class Config extends INIFile {

	public static final Config instance = new Config(new File("cfg/config.ini"));
	public static final Config inst =instance, i = instance;
	
	protected Config(File file) {
		super(file);
	}
	
	public int getDisplayWidth(int def){
		return get("Display", "width", def);
	}
	public int getDisplayHeight(int def){
		return get("Display", "height", def);
	}
	public int getDisplayMode(int def){
		return get("Display", "mode", def);
	}
	
	public void applyDisplayModeAndResolution(){
		Core.sys.setDisplayMode(getDisplayMode(2), getDisplayWidth(1280), getDisplayHeight(720));
	}

}
