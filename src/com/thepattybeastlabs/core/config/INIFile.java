package com.thepattybeastlabs.core.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class INIFile {

	private File file;
	private List<String> commentBuffer = new ArrayList<String>();
	private List<Group> groups = new ArrayList<Group>();
	
	public INIFile(File file) {
		this.file = file;
		readFromFile();
	}
	
	public void readFromFile(){
		try {
			BufferedReader r = new BufferedReader(new FileReader(file));
			String line = "";
			Group active = null;
			while((line=r.readLine())!=null){
				if(line.startsWith("#")){
					commentBuffer.add(line);
				}else if(line.isEmpty()){
					commentBuffer.add("");
				}else if(line.startsWith("[") && line.endsWith("]")){
					ArrayList<String> copyCommentBuffer = new ArrayList<String>(commentBuffer.size());
					copyCommentBuffer.addAll(commentBuffer);
					commentBuffer.clear();
					groups.add(active = new Group(copyCommentBuffer, this,line.substring(1, line.length()-1)));
				}else if(line.contains("=")){
					ArrayList<String> copyCommentBuffer = new ArrayList<String>(commentBuffer.size());
					copyCommentBuffer.addAll(commentBuffer);
					commentBuffer.clear();
					active.getTags().add(new Tag(copyCommentBuffer, this, line));
				}else{
					commentBuffer.add("#"+line);
				}
			}
			r.close();
		} catch (FileNotFoundException e) {} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void save() throws IOException{
		FileOutputStream out = new FileOutputStream("cfg/config.ini");
		out.write(Config.i.getText().getBytes());
		out.close();
	}
	
	public boolean hasTag(String group, String name){
		List<Tag> tags = getGroup(group).getTags();
		for(int i = 0; i < tags.size(); i++){
			if(tags.get(i).name.equals(name)){
				return true;
			}
		}
		return false;
	}
	
	public boolean hasGroup(String name){
		for(int i = 0; i < groups.size(); i++){
			if(groups.get(i).name.equals(name)){
				return true;
			}
		}
		return false;
	}
	
	/**@Returns the tag with the specified id in the specified group, if one does not exist one is created, 
	 * if multiple exist the first one found is returned.*/
	private Tag getTag(String group, String name, String defaultValue){
		List<Tag> tags = getGroup(group).getTags();
		for(int i = 0; i < tags.size(); i++){
			if(tags.get(i).name.equals(name)){
				return tags.get(i);
			}
		}
		Tag add = new Tag(new ArrayList<String>(), this, name+"="+defaultValue);
		tags.add(add);
		return add;
	}
	
	public String get(String group, String name, String defaultValue){
		return getTag(group, name, defaultValue).value;
	}
	
	public int get(String group, String name, int defaultValue){
		try{
			return Integer.parseInt(get(group, name, Integer.toString(defaultValue)));
		}catch(NumberFormatException e){}//return default value if not an integer
		return defaultValue;
	}
	
	public void set(String group, String name, String value){
		getTag(group, name, value).value = value;
	}
	
	public List<Group> getGroups(){
		return groups;
	}
	
	/**@Returns the group with the specified id, if one does not exist one is created, if multiple exist 
	 * the first occurrence is returned*/
	private Group getGroup(String id){
		for(int i = 0; i < groups.size(); i++){
			if(groups.get(i).name.equals(id)){
				return groups.get(i);
			}
		}
		Group add = new Group(new ArrayList<String>(), this, id);
		groups.add(add);
		return add;
	}
	
	public String getText(){
		StringBuilder res = new StringBuilder();
		for(Group g : groups){
			for(String c : g.comments){
				res.append(c + '\n');
			}
			res.append("["+g.name+"]" + '\n');
			for(Tag t : g.getTags()){
				for(String c : t.comments){
					res.append(c + '\n');
				}
				res.append(t.name + "=" + t.value + '\n');
			}
		}
		return res.toString();
	}
	
	public class Group{
		private List<String> comments;
		private INIFile parent;
		private List<Tag> tags;
		private String name;
		public Group(List<String> comments, INIFile parent, String name){
			this.comments = comments;
			this.parent = parent;
			this.name = name;
			tags = new ArrayList<Tag>();
		}
		private List<Tag> getTags(){
			return tags;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public List<String> getComments() {
			return comments;
		}
		public INIFile getParent() {
			return parent;
		}
	}
	
	public class Tag{
		private List<String> comments;
		private INIFile parent;
		private String name, value;
		
		public Tag(List<String> comments, INIFile parent, String line){
			this.comments = comments;
			this.parent = parent;
			String[] splt = line.split("=", 2);
			name = splt[0];
			value = splt[1];
		}
	}

}
