package com.thepattybeastlabs.core.tests;

import java.awt.Color;
import java.io.File;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.Image;
import com.thepattybeastlabs.core.shader.Shader;
import com.thepattybeastlabs.core.shader.Shader.ShaderType;
import com.thepattybeastlabs.core.shader.ShaderProgram;

public class ShaderTest extends TestBase {

	public String getName() {
		return "Shader Test";
	}

	public void init(Input in) throws Exception {
		ShaderProgram.list.loadResource(new ShaderProgram(new Shader(ShaderType.VERTEX,new File("testdata/shaders/shadertest.vs"))
				,new Shader(ShaderType.FRAGMENT,new File("testdata/shaders/shadertest.fs"))), "def");
		Image.list.load("image_test.png");
		Image.list.load("image_test2.png");
		
	}

	public void tick(Input in, AudioEngine ae, float delta, Screen activeScreen) throws Exception {
		
	}

	public void render(Graphics g, Input in) throws Exception {
		g.getBatchRender().bindShader(ShaderProgram.list.get("def"));
		g.drawImage(Image.list.get("image_test.png"), 0, 0, Core.width, Core.height);
		g.drawLine(0, 0, 100, 100);
		g.drawImage(Image.list.get("image_test2.png"), 100, 100, 200, 200);
		g.drawImage(Image.list.get("image_test.png"), 400, 400, 200, 100);
		g.setColor(Color.BLUE);
		g.setFont("Tahoma", -1);
		g.drawString("Test String...", 100, 100);
		g.getBatchRender().bindShader(null);
	}
	
	

}
