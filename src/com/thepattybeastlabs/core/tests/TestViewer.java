package com.thepattybeastlabs.core.tests;

import java.util.ArrayList;
import java.util.List;

import com.thepattybeastlabs.core.AbstractCoreGame;
import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.config.Config;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.Image;

public class TestViewer extends AbstractCoreGame{

	private ArrayList<TestBase> tests = new ArrayList<>();
	private TestBase active;
	
	public String getName() {
		return "Core Game Engine";
	}

	public void init(Input in) throws Exception {
		CoreFont.list.load("Tahoma");
		tests.add(new ImageTest());
		tests.add(new ShaderTest());
		tests.add(new GIFAnimationTest());
		tests.add(new ParticleTest());
		in.createBind("Menu", "menu", Input.OS_KEYBOARD, "escape");
		Image.list.setLoadPath("testdata/textures");
		for(TestBase t : tests){
			t.init(in);
		}
		active = tests.get(Config.i.get("tests", "sel", 0));
	}
	
	public void openTest(TestBase test){
		active = test;
		if(tests.contains(active)){
			Config.i.set("tests", "sel", tests.indexOf(active)+"");
		}
	}
	
	public List<TestBase> tests(){
		return tests;
	}

	@Override
	public void tick(Input in, AudioEngine ae, float delta, Screen activeScreen) throws Exception {
		active.tick(in, ae, delta, activeScreen);
		if(in.isPressed("menu")){
			if(activeScreen!=null){
				Core.setScreen(null);
			}else{
				Core.setScreen(new TestSelectorScreen(null, this));
			}
		}
	}

	@Override
	public void render(Graphics g, Input in) throws Exception {
		active.render(g, in);
	}
	
	public static void main(String[] args){
		Core.loadGame(args, 1280, 720, new TestViewer());
	}

}
