package com.thepattybeastlabs.core.tests;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.gui.Button;
import com.thepattybeastlabs.core.gui.Component;
import com.thepattybeastlabs.core.gui.List;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.gui.presetui.ConfigDialog;
import com.thepattybeastlabs.core.input.Input;

public class TestSelectorScreen extends Screen {

	private List<TestBase> tests;
	private TestViewer viewer;
	
	public TestSelectorScreen(Screen parent, TestViewer viewer) {
		super(parent);
		this.viewer=viewer;
		addComponent(tests=new List<>("tests", 310, 10, -320, -20, new List.AbstractItemManager<TestBase>() {
			public String getText(TestBase item, int index) {
				return item.getName();
			}
		}));
		tests.setData(viewer.tests().toArray(new TestBase[0]));
		addComponent(new Button("Return", "return", 10, -200, 290, 35));
		addComponent(new Button("Load Test", "tests", 10, -155, 290, 35));
		addComponent(new Button("Options", "options", 10, -110, 290, 35));
		addComponent(new Button("Exit", "exit", 10, -65, 290, 35));
	}

	public void onEnter() {
		// TODO Auto-generated method stub

	}

	public void onExit() {
		// TODO Auto-generated method stub

	}

	public void onActivate(Component comp, String id) {
		if(id.equals("return")){
			Core.setScreen(null);
		}
		if(id.equals("tests")&&tests.getSelectedItem()!=null){
			viewer.openTest(tests.getSelectedItem());
			Core.setScreen(null);
		}
		if(id.equals("options")){
			ConfigDialog.show("Core Engine Configuration");
		}
		if(id.equals("exit")){
			Core.exit();
		}
	}

	@Override
	protected void tickS(Input in, AudioEngine ae, float delta, float x, float y, float width, float height) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void renderS(Graphics g, Input in, float x, float y, float width, float height) {
		// TODO Auto-generated method stub

	}

}
