package com.thepattybeastlabs.core.tests;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.graphics.Color;
import com.thepattybeastlabs.core.graphics.particles.DefaultParticleEmitter;
import com.thepattybeastlabs.core.graphics.particles.ParticleSystem;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.Image;
import com.thepattybeastlabs.core.utils.FixedValue;
import com.thepattybeastlabs.core.utils.RandomValue;

public class ParticleTest extends TestBase {

	private ParticleSystem particles = new ParticleSystem();
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Particle Test";
	}

	@Override
	public void init(Input in) throws Exception {
		Image.list.load("simple_particle.png");
		particles.add(e1());
		in.createBind("Spawn Particles", "particles", Input.OS_MOUSE, "m1");
	}
	
	private DefaultParticleEmitter e1(){
		DefaultParticleEmitter d= new DefaultParticleEmitter();
		d.setX(new RandomValue(400, 400));
		d.setY(new RandomValue(300, 300));
		d.setWidth(new FixedValue(2));
		d.setHeight(new FixedValue(2));
		d.setImage("simple_particle.png");
		d.setVelX(new RandomValue(-0.1f,0.1f));
		d.setVelY(new FixedValue(0.3f));
		d.setLife(new FixedValue(20));
		d.setSpawnRate(new FixedValue(2));
		//d.setWindX(new RandomValue(0, -0.01f));
		//d.setWindY(new RandomValue(0, -0.01f));
		d.setDespawnCountdown(120);
		d.setDespawn(false);
		d.setFriction(new FixedValue(0.0000f));
		d.setStartColor(new Color(245,77,39,400));
		d.setEndColor(new Color(255,234,0,0));
		d.setSpawnAmount(new FixedValue(1));
		return d;
	}
	
	private DefaultParticleEmitter e2(){
		DefaultParticleEmitter d= new DefaultParticleEmitter();
		d.setX(new FixedValue(Core.input.getX()));
		d.setY(new FixedValue(Core.input.getY()));
		d.setWidth(new FixedValue(4));
		d.setHeight(new FixedValue(4));
		d.setImage("simple_particle.png");
		d.setVelX(new RandomValue(-0.2f,0.2f));
		d.setVelY(new RandomValue(-0.2f,0.2f));
		d.setLife(new FixedValue(100));
		d.setSpawnRate(new FixedValue(1000));
		//d.setWindX(new RandomValue(0, -0.01f));
		//d.setWindY(new RandomValue(0, -0.01f));
		d.setDespawnCountdown(120);
		d.setDespawn(true);
		d.setSpawnAmount(new FixedValue(8));
		d.setStartColor(new Color(245,77,39,400));
		d.setEndColor(new Color(0,0,100,0));
		return d;
	}

	@Override
	public void tick(Input in, AudioEngine ae, float delta, Screen activeScreen) throws Exception {
		if(in.isDown("particles")){
			particles.add(e2());
		}
		particles.tick(delta, 0, 0);
	}

	@Override
	public void render(Graphics g, Input in) throws Exception {
		particles.render(g, 0, 0);
		g.drawString("Particles: " + particles.getParticleCount(), 5, 600);
	}
	
	

}
