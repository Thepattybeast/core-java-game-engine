package com.thepattybeastlabs.core.tests;

import java.awt.Color;
import java.io.File;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.animation.Animation;

public class GIFAnimationTest extends TestBase{

	boolean animLoaded=false;
	private Animation anim, anim2;
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "GIF Animation Test";
	}

	@Override
	public void init(Input in) throws Exception {
		new Thread(()->{
			try {
				anim = new Animation(new File("testdata/test_1.gif"));
				anim2 = new Animation(new File("testdata/test_2.gif"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		
	}

	@Override
	public void tick(Input in, AudioEngine ae, float delta, Screen activeScreen) throws Exception {
		if(!animLoaded &&anim2!=null){
			animLoaded=true;
			anim.load();
			anim2.load();
		}
		if(animLoaded){
			anim.tick(delta);anim2.tick(delta);
		}
	}

	@Override
	public void render(Graphics g, Input in) throws Exception {
		if(animLoaded){
			anim.render(g, 30, 30, 100, 100);
			anim2.render(g, 160, 30, 400, 400);
			g.setColor(Color.WHITE);
			g.drawRect(160, 440, 400, 10);
			g.fillRect(160, 440, 400f/anim2.getFrameCount()*anim2.getCurrentFrame(), 10);
		}else{
			g.drawString("Loading...", 100, 100);
		}
	}
	
	

}
