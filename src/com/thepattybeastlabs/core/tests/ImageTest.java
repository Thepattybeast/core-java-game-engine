package com.thepattybeastlabs.core.tests;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.resource.Image;

public class ImageTest extends TestBase {

	@Override
	public String getName() {
		return "Image Test";
	}

	public void tick(Input in, AudioEngine ae, float delta, Screen activeScreen) throws Exception {
		
	}

	public void render(Graphics g, Input in) throws Exception {
		g.drawImage(Image.list.get("image_test.png"), 0, 0, Core.width, Core.height);
		g.drawLine(0, 0, 100, 100);
		g.drawImage(Image.list.get("image_test2.png"), 100, 100, 200, 200);
		g.drawImage(Image.list.get("image_test.png"), 400, 400, 200, 100);
	}

	public void init(Input in) throws Exception {
		Core.showFPS=false;
		Image.list.load("image_test.png");
		Image.list.load("image_test2.png");
	}
	
	

}
