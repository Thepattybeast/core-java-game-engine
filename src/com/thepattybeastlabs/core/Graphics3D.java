package com.thepattybeastlabs.core;

import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslatef;

import java.awt.Color;

import com.thepattybeastlabs.core.geom.Vector3;
import com.thepattybeastlabs.core.resource.Image;

public interface Graphics3D {

	public void create()throws Exception;
	public void enter();
	
	public void fillCube(float x, float y, float z, float width, float height, float depth);
	public void fillRect(float x, float y, float z, float width, float height, float rotX, float rotY);
	public void rotate(float rot, float width, float height);
	public void rotateTo(float rot, float width, float height, float depth, boolean x, boolean y, boolean z);
	public void drawImage(Image image, float x, float y, float z, float width, float height, float rotX,float rotY);
	public void drawImage(Image image, float x, float y, float z, float width, float height, float rotX,float rotY, float imgX, float imgY, float imgWidth, float imgHeight);
	public void translate(float x, float y, float z);
	public void drawTriangles(Vector3[] triangles, int[] indices, int x, int y, int z);
	/**Draw Vertices to screen. will be called at the end of the render cycle*/
	public void flush();
	
	public void setColor(Color color);
	
	public void setColor(Color color, int alpha);
	
	public void setColor(int r, int g, int b);
	public void setColor(int r, int g, int b, int a);
	public void setTranslation(float x, float y, float z);
	
}
