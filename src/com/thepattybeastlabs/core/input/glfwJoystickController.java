package com.thepattybeastlabs.core.input;

import static org.lwjgl.glfw.GLFW.*;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;

public class glfwJoystickController implements Controller{

	private String name;
	private ByteBuffer buttons;
	private FloatBuffer axes;
	private FloatComponent[] components;
	private int id;
	private ArrayList<Controller> controllers;
	
	public glfwJoystickController(String name, ByteBuffer byteBuffer,
			FloatBuffer axes, int id, ArrayList<Controller> controllers) {
		this.name = name;
		this.buttons = byteBuffer;
		this.axes = axes;
		this.controllers = controllers;
		components = new FloatComponent[buttons.capacity() + axes.capacity()];
		for(int i = 0; i < components.length; i++){
			components[i] = new FloatComponent(i > axes.capacity()- 1? ("btn_"+(i-axes.capacity())) : ("axis_"+i));
		}
		this.id = id;
	}

	public Type getType() {
		return Type.JOYSTICK;
	}

	public Component[] getComponents() {
		return components;
	}

	public boolean poll() {
		try{
		buttons = glfwGetJoystickButtons(id);
		axes = glfwGetJoystickAxes(id);
		buttons.position(0);
		axes.position(0);
		}catch(Exception e){if(id < controllers.size())controllers.remove(id);return false;}
		
		for(int i = 0; i < components.length; i++){
			components[i].value=0;
			if(i > axes.capacity()- 1){
				components[i].value = buttons.get()==GLFW_PRESS? 1.0f: 0.0f;
			}else{
				components[i].value = axes.get();
				
			}
			if(components[i].value < -1||components[i].value > 1)components[i].value=0;
		}
		return true;
	}

	public String getName() {
		return name;
	}

	public class FloatComponent implements Component{
		
		private String name;
		private float value = 0.0f;
		public FloatComponent(String name) {
			this.name = name;
		}
		public boolean isRelative() {return false;}
		public boolean isAnalog() {
			return true;
		}

		public float getDeadZone() {
			return 0.2f;
		}
		public float getPollData() {
			if(value < -1||value > 1)return 0;
			return value;
		}
		public String getName() {
			return name;
		}
	}
}
