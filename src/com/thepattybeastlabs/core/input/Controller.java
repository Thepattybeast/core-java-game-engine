package com.thepattybeastlabs.core.input;

public interface Controller {

	public enum Type {MOUSE, KEYBOARD, JOYSTICK};
	
	public Type getType();
	public Component[] getComponents();
	public boolean poll();
	public String getName();
	
}
