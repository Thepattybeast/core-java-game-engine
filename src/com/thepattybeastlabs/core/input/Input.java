package com.thepattybeastlabs.core.input;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.input.InputFilter.InputFilterContainer;

public class Input {

	public static final Controller OS_MOUSE = new OSMouse();
	public static final Controller OS_KEYBOARD = new OSKeyboard();
	public static boolean saveKeys = true;
	
	private Rectangle drawBounds;
	private Dimension render;
	private ArrayList<Controller> controllers;
	private ArrayList<InputFilterContainer> filters = new ArrayList<>();
	private int filterIdAssign=0, activeFilterId=-1;

	private ArrayList<Controller> activeControllers = new ArrayList<>();
	private ArrayList<KeyBind> keybinds = new ArrayList<>();
	private com.thepattybeastlabs.core.system.System sys;
	private File keybindsSave;
	
	public Input(Rectangle drawBounds, Dimension render, File keybinds){
		this.drawBounds = drawBounds;
		this.render = render;
		this.keybindsSave = keybinds;
		
	}
	
	/**Backend method, this will be set by the Core runtime.
	 * Setting this will break things.
	 * There is no reason to ever touch this.*/
	public void setDrawBounds(Rectangle rect){
		drawBounds = rect;
	}
	
	public void setSystem(com.thepattybeastlabs.core.system.System sys){
		this.sys = sys;
		controllers = sys.getControllers();
	}
	
	public int createFilter(InputFilter filter){
		InputFilterContainer c = new InputFilterContainer(filterIdAssign++, filter);
		filters.add(c);
		return c.getId();
	}
	
	public void enableFilter(int id){
		activeFilterId=id;
	}
	
	public void disableFilter(){
		activeFilterId=-1;
	}
	
	public void disableFilter(int id){
		if(activeFilterId==id)activeFilterId=-1;
	}
	
	public void validateFilter(int id){
		InputFilterContainer c = getFilter(id);
		if(c!=null)c.setInvalidated(false);
	}
	
	public InputFilterContainer getFilter(int id){
		for(InputFilterContainer c : filters){
			if(c.getId()==id){
				return c;
			}
		}
		return null;
	}
	
	/**Backend method, this will be set by the Core runtime.
	 * Setting this will break things.
	 * There is no reason to ever touch this.*/
	public void setInternalRenderingResolution(Dimension d){
		render = d;
	}
	
	/**Return the x postion of the mouse*/
	public int getX(){
		return (int)((double)(sys.getMouseInfo().getX()-drawBounds.getX())/((double)drawBounds.getWidth()/(double)render.getWidth()));
	}
	
	/**Return the y postion of the mouse*/
	public int getY(){
		return (int)((double)((sys.getMouseInfo().getY())-drawBounds.getY())/((double)drawBounds.getHeight()/(double)render.getHeight()));
	}
	
	/**Updates the input engine.
	 * This is done by the core runtime.*/
	public void tick(){
		for(int i = 0; i < controllers.size(); i++){
			controllers.get(i).poll();
		}
		OS_MOUSE.poll();
		OS_KEYBOARD.poll();
		for(int i = 0 ; i< keybinds.size(); i++){
			keybinds.get(i).tick();
		}
		for(int i = 0; i < filters.size();i++){
			InputFilterContainer c = filters.get(i);
			if(c.isInvalidated()){
				filters.remove(i);
				i--;
			}else{
				c.setInvalidated(true);
			}
		}
	}
	
	/**Creates a keybind if it does not already exist.
	 * Existing keybinds will not be changed
	 * @returns true if bind was created, otherwise false*/
	public boolean createBind(String name, String id, Controller cont, String keyName){
		if(getKeybind(id)!=null||cont==null)return false;
		Component[] comps = cont.getComponents();
		for(int i = 0; i < comps.length; i++){
			if(comps[i].getName().equalsIgnoreCase(keyName)){
				addBind(new KeyBind(name, id, cont, comps[i], this, false));
				return true;
			}
		}
		printComponenets(cont);
		new Exception(keyName + " Not Found").printStackTrace();
		return false;
	}
	
	public void addBind(KeyBind bind){
		keybinds.add(bind);
		if(!activeControllers.contains(bind.getController())){
			activeControllers.add(bind.getController());
		}
	}
	/**Creates a keybind if it does not already exist.
	 * Existing keybinds will not be changed
	 * @returns true if bind was created, otherwise false*/
	public boolean createBind(String name, String id, Controller cont, int keyId){
		if(getKeybind(id)!=null)return false;
		Component[] comps = cont.getComponents();
		addBind(new KeyBind(name, id, cont, comps[keyId], this, false));
		return true;
	}
	
	public void printComponenets(Controller cont){
		if(cont==null){
			System.out.println("No components (controller==null)");
			return;
		}
		Component[] comps = cont.getComponents();
		for(int i = 0; i < comps.length; i++){
			System.out.println(comps[i].getName() + " - " + comps[i].getPollData());
		}
	}
	
	public ArrayList<Controller> getControllers(){
		return controllers;
	}
	
	public Controller getController(String controllerName){
		if(controllerName.equals("Mouse"))return OS_MOUSE;
		if(controllerName.equals("Keyboard"))return OS_KEYBOARD;
		if(controllers==null)return null;
		for(int i = 0; i < controllers.size(); i++){
			if(controllers.get(i).getName()==controllerName){
				return controllers.get(i);
			}
		}
		return null;
	}
	
	public Controller getControllerOfType(Controller.Type type){
		for(int i = 0; i < controllers.size(); i++){
			if(controllers.get(i).getType()==type){
				return controllers.get(i);
			}
		}
		return null;
	}
	
	public KeyBind getKeybind(String id){
		for(int i = 0; i < keybinds.size(); i++){
			if(keybinds.get(i).getName().equals(id)){
				return keybinds.get(i);
			}
		}
		return null;
	}
	
	public float getAxis(String id){
		KeyBind key = getKeybind(id);
		
		if(key==null){
			System.err.println(id+ " not found ("+new Exception().getStackTrace()[1]+").");
			return 0;
		}
		
		return key.getPollValue();
	}
	
	/**Is button down*/
	public boolean isDown(String id){
		KeyBind key = getKeybind(id);
		if(key==null){
			System.err.println(id+ " not found ("+new Exception().getStackTrace()[1]+").");
			return false;
		}
		
		return key.getPollValue() > 0;
	}
	/**Did the button state change from up to down this tick**/
	public boolean isPressed(String id){
		KeyBind key = getKeybind(id);
		if(key==null){
			System.err.println(id+ " not found ("+new Exception().getStackTrace()[1]+").");
			return false;
		}
		return key.getLastPollValue()==0 && key.getPollValue() > 0;
	}
	
	/**Did the button state change from down to up this tick**/
	public boolean isReleased(String id){
		KeyBind key = getKeybind(id);
		if(key==null){
			System.err.println(id+ " not found ("+new Exception().getStackTrace()[1]+").");
			return false;
		}
		return key.getPollValue()==0 && key.getLastPollValue() > 0;
	}
	
	public List<KeyBind> getKeybinds(){
		return keybinds;
	}
	
	public void save(){
		if(!saveKeys)return;
		try {
			Formatter f = new Formatter(keybindsSave);
			for(int i = 0; i < keybinds.size(); i++){
				KeyBind k = keybinds.get(i);
				f.format("%s<,>%s<,>%s<,>%s%n", k.displayName, k.name, k.controller.getName(), k.component.getName());
			}
			f.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void load(){
		if(keybindsSave.exists()){
			try {
				Scanner s = new Scanner(keybindsSave);
				while(s.hasNextLine()){
					String line = s.nextLine();
					if(!line.startsWith("#")){
						String[] splt = line.split("<,>");
						if(splt.length!=4){
							System.err.println("Invalid line '"+line+"' invalid number of parameters ("+splt.length+"!=4)");
							continue;
						}
						createBind(splt[0], splt[1], getController(splt[2]), splt[3]);
					}
				}
				s.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	public float filter(float pollData, String id){
		float res = pollData;
		for(InputFilterContainer c : filters){
			float poll = c.getFilter().filter(this, id, pollData, activeFilterId, c.getId());
			if(poll<res){
				res=poll;
			}
		}
		return res;
	}
	
	public class KeyBind{
		private String displayName, name;
		private Controller controller;
		private Component component;
		private Input in;
		private float poll, lastPoll;
		
		public KeyBind(String displayName, String name, Controller controller, Component component, Input in, boolean negative) {
			this.displayName = displayName;
			this.name = name;
			this.controller = controller;
			this.component = component;
			this.in = in;
			if(in==null)throw new NullPointerException("Perameter 'in' cannot equal null");
		}
		
		public void tick(){
			lastPoll = poll;
			poll = component.getPollData();
			if((poll < component.getDeadZone()&& poll > 0) || (poll > -component.getDeadZone()&&poll < 0))poll = 0;
		}
		
		public float getLastPollValue(){
			return in.filter(lastPoll, getName());
		}
		
		public float getPollValue(){
			return in.filter(poll, getName());
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Controller getController() {
			return controller;
		}

		public void setController(Controller controller) {
			this.controller = controller;
		}

		public Component getComponenet() {
			return component;
		}

		public void setComponenet(Component componenet) {
			this.component = componenet;
		}
		
	}
	
}
