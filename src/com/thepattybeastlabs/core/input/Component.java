package com.thepattybeastlabs.core.input;

import com.thepattybeastlabs.core.system.System.Key;

public interface Component {
	
	public boolean isRelative();
	
	public boolean isAnalog();

	public float getDeadZone();
	
	public float getPollData();

	public String getName();
	
}
