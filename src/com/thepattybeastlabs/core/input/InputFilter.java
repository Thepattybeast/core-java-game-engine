package com.thepattybeastlabs.core.input;

public interface InputFilter {

	public float filter(Input in, String id, float value, int active, int self);
	
	public static class InputFilterContainer{
		private int id;
		private InputFilter filter;
		private boolean invalidated;
		
		public InputFilterContainer(int id, InputFilter filter) {
			this.id = id;
			this.filter = filter;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public InputFilter getFilter() {
			return filter;
		}
		public void setFilter(InputFilter filter) {
			this.filter = filter;
		}
		public boolean isInvalidated() {
			return invalidated;
		}
		public void setInvalidated(boolean invalidated) {
			this.invalidated = invalidated;
		}
		
	}
	
}
