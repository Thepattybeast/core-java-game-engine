package com.thepattybeastlabs.core.input;

import java.util.ArrayList;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.system.*;
import com.thepattybeastlabs.core.system.System;

public class TextInput {
	
	private int filterId = -1;
	
	private ArrayList<Key> keys;
	private System.Key LSHIFT, RSHIFT, BACK, LEFT, RIGHT;
	
	private StringBuilder text;
	private String cache = "";
	private boolean enabled;
	private int caretPos;
	private int lastKey=-1;
	
	
	public TextInput(String text){
		cache = text;
		caretPos = text.length();
		this.text = new StringBuilder(text);
		loadKeys();
	}
	
	public void setEnabled(boolean enabled){
		this.enabled = enabled;
		if(filterId==-1){
			filterId=Core.input.createFilter((input, id, val, active, self)->{
				if(active!=self)return val;
				if(input.getKeybind(id).getController()==Input.OS_KEYBOARD)return 0;
				return val;
			});
		}
		if(enabled)Core.input.enableFilter(filterId);
		//else Core.input.disableFilter(filterId);
	}
	
	public void tick(){
		Core.input.validateFilter(filterId);
		if(!enabled)return;
		for(int i = 0; i < keys.size(); i++){
			if(keys.get(i).getKey().isPressed()){
				if(lastKey==-1){
					if(BACK.isPressed()){
						if(caretPos > 0){
							text.deleteCharAt(caretPos-1);
							caretPos--;
						}
						
					}else if(LEFT.isPressed()){
						if(caretPos > 0)caretPos--;
					}else if(RIGHT.isPressed()){
						if(caretPos < text.length())caretPos++;
					}else{
						if(LSHIFT.isPressed()){
							text.insert(caretPos, keys.get(i).getSecondary());
						}else{
							text.insert(caretPos, keys.get(i).getPrimary());
						}
						caretPos++;
					}
					lastKey=keys.get(i).getKey().getCode();
					cache=text.toString();
					break;
				}
			}else if(keys.get(i).getKey().getCode()==lastKey) {
				lastKey=-1;
			}
		}
	}
	
	public StringBuilder getStringBuilder(){
		 return text;
	}
	
	public void updateCache(){
		cache=text.toString();
	}
	
	public String getText(){
		return cache;
	}
	
	private void loadKeys(){
		keys = new ArrayList<>();
		System.Key[] sysKeys = Core.sys.getKeys();
		for(int i = 0; i < sysKeys.length; i++){
			Key toAdd = null;
			switch(sysKeys[i].getName().toUpperCase()){
			case "A": toAdd=new Key("a", "A", sysKeys[i]);break;
			case "B": toAdd=new Key("b", "B", sysKeys[i]);break;
			case "C": toAdd=new Key("c", "C", sysKeys[i]);break;
			case "D": toAdd=new Key("d", "D", sysKeys[i]);break;
			case "E": toAdd=new Key("e", "E", sysKeys[i]);break;
			case "F": toAdd=new Key("f", "F", sysKeys[i]);break;
			case "G": toAdd=new Key("g", "G", sysKeys[i]);break;
			case "H": toAdd=new Key("h", "H", sysKeys[i]);break;
			case "I": toAdd=new Key("i", "I", sysKeys[i]);break;
			case "J": toAdd=new Key("j", "J", sysKeys[i]);break;
			case "K": toAdd=new Key("k", "K", sysKeys[i]);break;
			case "L": toAdd=new Key("l", "L", sysKeys[i]);break;
			case "M": toAdd=new Key("m", "M", sysKeys[i]);break;
			case "N": toAdd=new Key("n", "N", sysKeys[i]);break;
			case "O": toAdd=new Key("o", "O", sysKeys[i]);break;
			case "P": toAdd=new Key("p", "P", sysKeys[i]);break;
			case "Q": toAdd=new Key("q", "Q", sysKeys[i]);break;
			case "R": toAdd=new Key("r", "R", sysKeys[i]);break;
			case "S": toAdd=new Key("s", "S", sysKeys[i]);break;
			case "T": toAdd=new Key("t", "T", sysKeys[i]);break;
			case "U": toAdd=new Key("u", "U", sysKeys[i]);break;
			case "V": toAdd=new Key("v", "V", sysKeys[i]);break;
			case "W": toAdd=new Key("w", "W", sysKeys[i]);break;
			case "X": toAdd=new Key("x", "X", sysKeys[i]);break;
			case "Y": toAdd=new Key("y", "Y", sysKeys[i]);break;
			case "Z": toAdd=new Key("z", "Z", sysKeys[i]);break;
			case "1": toAdd=new Key("1", "!", sysKeys[i]);break;
			case "2": toAdd=new Key("2", "@", sysKeys[i]);break;
			case "3": toAdd=new Key("3", "#", sysKeys[i]);break;
			case "4": toAdd=new Key("4", "$", sysKeys[i]);break;
			case "5": toAdd=new Key("5", "%", sysKeys[i]);break;
			case "6": toAdd=new Key("6", "^", sysKeys[i]);break;
			case "7": toAdd=new Key("7", "&", sysKeys[i]);break;
			case "8": toAdd=new Key("8", "*", sysKeys[i]);break;
			case "9": toAdd=new Key("9", "(", sysKeys[i]);break;
			case "0": toAdd=new Key("0", ")", sysKeys[i]);break;
			case "-": toAdd=new Key("-", "_", sysKeys[i]);break;
			case "=": toAdd=new Key("=", "+", sysKeys[i]);break;
			case "[": toAdd=new Key("[", "{", sysKeys[i]);break;
			case "]": toAdd=new Key("]", "}", sysKeys[i]);break;
			case "\\": toAdd=new Key("\\", "|", sysKeys[i]);break;
			case ";": toAdd=new Key(";", ":", sysKeys[i]);break;
			case "'": toAdd=new Key("'", "\"", sysKeys[i]);break;
			case ",": toAdd=new Key(",", "<", sysKeys[i]);break;
			case ".": toAdd=new Key(".", ">", sysKeys[i]);break;
			case "/": toAdd=new Key("/", "?", sysKeys[i]);break;
			case "`": toAdd=new Key("`", "~", sysKeys[i]);break;
			case "SPACE": toAdd=new Key(" ", " ", sysKeys[i]);break;
			case "ENTER": toAdd=new Key("\n", "\n", sysKeys[i]);break;
			
			case "LSHIFT": LSHIFT = sysKeys[i];break;
			case "BACK": BACK = sysKeys[i];toAdd=new Key("", "", sysKeys[i]);break;
			case "LEFT": LEFT = sysKeys[i];toAdd=new Key("", "", sysKeys[i]);break;
			case "RIGHT": RIGHT = sysKeys[i];toAdd=new Key("", "", sysKeys[i]);break;
			}
			if(toAdd!=null)keys.add(toAdd);
		}
		
	}
	
	
	public static class Key{
		private String primary;
		private String secondary;
		private System.Key key;
		public Key(String primary, String secondary, System.Key key) {
			this.primary = primary;
			this.secondary = secondary;
			this.key = key;
		}
		public String getPrimary() {
			return primary;
		}
		public void setPrimary(String primary) {
			this.primary = primary;
		}
		public String getSecondary() {
			return secondary;
		}
		public void setSecondary(String secondary) {
			this.secondary = secondary;
		}
		public System.Key getKey() {
			return key;
		}
		public void setKey(System.Key key) {
			this.key = key;
		}
	}

	public void setText(String text) {
		this.text.delete(0, this.text.length());
		this.text.append(text);
		this.text.trimToSize();
		caretPos = this.text.length();
		cache = this.text.toString();
	}
	
	public void setCaretPos(int pos){
		caretPos = pos;
	}
	
	public int getCaretPos(){
		return caretPos;
	}
	
}
