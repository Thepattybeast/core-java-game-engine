package com.thepattybeastlabs.core.input;

import com.thepattybeastlabs.core.system.System.Key;

public class OSKeyboardComponent implements Component {
	
	private Key key;
	
	public boolean isRelative() {
		return false;
	}
	public boolean isAnalog() {
		return false;
	}

	public float getDeadZone() {
		return 0;
	}
	
	public OSKeyboardComponent(Key key) {
		this.key = key;
	}
	
	public float getPollData() {
		return key.isPressed()? 1 : 0;
	}

	public String getName() {
		return key.getName();
	}

}
