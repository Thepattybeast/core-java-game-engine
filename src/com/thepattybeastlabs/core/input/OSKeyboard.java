package com.thepattybeastlabs.core.input;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.system.System.Key;

public class OSKeyboard implements Controller{

	private OSKeyboardComponent[] comp;
	
	public Controller[] getControllers() {
		return new Controller[]{};
	}

	public Type getType() {
		return Type.KEYBOARD;
	}

	public Component[] getComponents() {
		if(comp==null){
			if(Core.sys==null){
				throw new NullPointerException("Core.sys cannot be null");
			}
			Key[] keys = Core.sys.getKeys();
			comp = new OSKeyboardComponent[keys.length];
			for(int i = 0; i < keys.length; i++){
				comp[i] = new OSKeyboardComponent(keys[i]);
			}
		}
		return comp;
	}

	public boolean poll() {
		return true;
	}

	public int getPortNumber() {
		return 0;
	}

	public String getName() {
		return "Keyboard";
	}

	
	
}
