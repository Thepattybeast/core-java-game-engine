package com.thepattybeastlabs.core.input;

import com.thepattybeastlabs.core.Core;

public class OSMouseButtonComponent implements Component {

	private int number;
	
	public OSMouseButtonComponent(int buttonNum){
		number = buttonNum;
	}
	
	public float getDeadZone() {
		return 0;
	}

	public String getName() {
		return "m" + (number+1);
	}

	public float getPollData() {
		return Core.sys.getMouseButtons()[number]? 1:0;
	}

	public boolean isAnalog() {
		return false;
	}

	public boolean isRelative() {
		return false;
	}

}
