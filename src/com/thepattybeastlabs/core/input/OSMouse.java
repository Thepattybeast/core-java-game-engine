package com.thepattybeastlabs.core.input;

import com.thepattybeastlabs.core.Core;

public class OSMouse implements Controller{

	private Component[] comps;
	private float dwheel;
	private int lastMWheel;
	private int dx, dy, lx, ly;
	
	public OSMouse(){
		int num = 3 + Core.sys.getMouseButtons().length;
		comps = new Component[num];
		for(int i = 0; i < comps.length-1; i++){
			comps[i] = new OSMouseButtonComponent(i);
		}
		comps[num-1] = new Component() {
			public boolean isRelative() {
				return false;
			}
			public boolean isAnalog() {return false;}
			public float getPollData() {
				
				return dwheel;
			}
			public String getName() {
				return "mwheel";
			}
			public float getDeadZone() {return 0;}
		};
		comps[num-2] = new Component() {
			public boolean isRelative() {
				return false;
			}
			public boolean isAnalog() {return false;}
			public float getPollData() {
				return dx;
			}
			public String getName() {
				return "dx";
			}
			public float getDeadZone() {return 0;}
		};
		comps[num-3] = new Component() {
			public boolean isRelative() {
				return false;
			}
			public boolean isAnalog() {return false;}
			public float getPollData() {
				return dy;
			}
			public String getName() {
				return "dy";
			}
			public float getDeadZone() {return 0;}
		};
	}
	
	public boolean poll() {
		int mx = Core.input.getX(), my = Core.input.getY();
		dx = lx-mx;
		dy = ly-my;
		lx=mx;
		ly=my;
		dwheel = lastMWheel - Core.sys.getMWheelPos();
		lastMWheel = Core.sys.getMWheelPos();
		return true;
	}
	
	public Type getType() {
		return Type.MOUSE;
	}
	
	public int getPortNumber() {
		return 0;
	}
	
	public String getName() {
		return "Mouse";
	}
	
	public Controller[] getControllers() {
		return new Controller[]{this};
	}
	
	public Component[] getComponents() {
		return comps;
	}
}
