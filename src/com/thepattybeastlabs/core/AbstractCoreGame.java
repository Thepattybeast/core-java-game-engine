package com.thepattybeastlabs.core;

import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.input.Input;

public abstract class AbstractCoreGame implements CoreGame{

	@Override
	public void init(Input in) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postInit() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openGLInit() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tick(Input in, AudioEngine ae, float delta, Screen activeScreen) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g, Input in) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics3D g, Input in) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
