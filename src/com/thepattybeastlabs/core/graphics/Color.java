package com.thepattybeastlabs.core.graphics;

public class Color {

	private float red, green, blue, alpha;

	/**Float values between 0 and 255*/
	public Color(float red, float green, float blue, float alpha) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}

	public float getRed() {
		return red;
	}

	public void setRed(float red) {
		this.red = red;
	}

	public float getGreen() {
		return green;
	}

	public void setGreen(float green) {
		this.green = green;
	}

	public float getBlue() {
		return blue;
	}

	public void setBlue(float blue) {
		this.blue = blue;
	}

	public float getAlpha() {
		return alpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}
	
	public java.awt.Color toAwtColor(){
		return new java.awt.Color((int)byteCap(red),(int)byteCap(green),(int)byteCap(blue),(int)byteCap(alpha));
	}
	
	/**Cap number between 0 and 255*/
	private float byteCap(float num){
		if(num>255)num=255;
		if(num<0)num=0;
		return num;
	}
	
}
