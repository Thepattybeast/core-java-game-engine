package com.thepattybeastlabs.core.graphics;

import com.thepattybeastlabs.core.resource.Image;
import com.thepattybeastlabs.core.shader.ShaderProgram;

public class RenderSection {

		private Image tex;
		private ShaderProgram shader;
		public ShaderProgram getShader() {
			return shader;
		}

		public void setShader(ShaderProgram shader) {
			this.shader = shader;
		}

		private int begin, renderMode;
		private int end;
		private int clipX, clipY, clipWidth, clipHeight;
		private boolean clip,addClipoffset;
		
		public RenderSection(int begin, RenderSection last, Image blanktex, ShaderProgram blankShader) {
			this.begin=begin;
			if(last==null){
				tex=blanktex;
				shader = blankShader;
				return;
			}
			tex=last.getTex();
			renderMode=last.getRenderMode();
			clipX=last.getClipX();
			clipY=last.getClipY();
			clipWidth=last.getClipWidth();
			clipHeight=last.getClipHeight();
			clip = last.isClip();
			addClipoffset = last.isAddClipoffset();
			shader=last.getShader();
		}
		
		public boolean isAddClipoffset() {
			return addClipoffset;
		}

		public void setAddClipoffset(boolean addClipoffset) {
			this.addClipoffset = addClipoffset;
		}

		
		public Image getTex() {
			return tex;
		}
		public void setTex(Image tex) {
			this.tex = tex;
		}
		public int getBegin() {
			return begin;
		}
		public void setBegin(int begin) {
			this.begin = begin;
		}
		public int getEnd() {
			return end;
		}
		public void setEnd(int end) {
			this.end = end;
		}
		public int getRenderMode() {
			return renderMode;
		}
		public void setRenderMode(int renderMode) {
			this.renderMode = renderMode;
		}
		public int getClipX() {
			return clipX;
		}
		public void setClipX(int clipX) {
			this.clipX = clipX;
		}
		public int getClipY() {
			return clipY;
		}
		public void setClipY(int clipY) {
			this.clipY = clipY;
		}
		public int getClipWidth() {
			return clipWidth;
		}
		public void setClipWidth(int clipWidth) {
			this.clipWidth = clipWidth;
		}
		public int getClipHeight() {
			return clipHeight;
		}
		public void setClipHeight(int clipHeight) {
			this.clipHeight = clipHeight;
		}
		public boolean isClip() {
			return clip;
		}
		public void setClip(boolean clip) {
			this.clip = clip;
		}
		
		public void setClipBounds(int x, int y, int width, int height){
			setClipX(x);
			setClipY(y);
			setClipWidth(width);
			setClipHeight(height);
		}
	
}
