package com.thepattybeastlabs.core.graphics.particles;

import java.util.ArrayList;
import java.util.List;

import com.thepattybeastlabs.core.Graphics;

public class ParticleSystem {

	protected List<ParticleEmitter> emitters = new ArrayList<>();
	protected List<Particle> particles = new ArrayList<>();
	
	public void add(ParticleEmitter emitter){
		emitters.add(emitter);
	}
	
	public void add(Particle particle){
		particles.add(particle);
	}
	
	public void render(Graphics g, float offX, float offY){
		for(Particle particle : particles){
			particle.render(g, offX, offY);
		}
	}
	
	public void tick(float delta, float offX, float offY){
		for(int i = 0; i < emitters.size(); i++){
			emitters.get(i).tick(this, delta);
			if(emitters.get(i).isDead()){
				emitters.remove(i);
				i--;
			}
		}
		for(int i = 0; i < particles.size(); i++){
			particles.get(i).tick(delta, offX, offY);;
			if(particles.get(i).isDead()){
				particles.remove(i);
				i--;
			}
		}
	}
	
	public int getParticleCount(){
		return particles.size();
	}
	
}
