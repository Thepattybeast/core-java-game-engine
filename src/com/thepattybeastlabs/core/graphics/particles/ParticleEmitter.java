package com.thepattybeastlabs.core.graphics.particles;

public interface ParticleEmitter {

	public void tick(ParticleSystem sys, float delta);
	public boolean isDead();
	
}
