package com.thepattybeastlabs.core.graphics.particles;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.graphics.Color;
import com.thepattybeastlabs.core.resource.Image;

public class DefaultParticle implements Particle {

	private float x,y,scale,life,velX,velY,windX,windY, friction, width, height;
	private Color startColor,endColor;
	private Color active, change;
	private Image texture;
	
	public DefaultParticle(float x, float y, float life, float width, float height, Image texture, Color startColor, Color endColor) {
		this.x = x;
		this.y = y;
		this.life=life;
		this.width=width;
		this.height=height;
		this.texture=texture;
		this.startColor=startColor;
		this.endColor=endColor;
		active=new Color(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), startColor.getAlpha());
		change=new Color((endColor.getRed()-startColor.getRed())/life, (endColor.getGreen()-startColor.getGreen())/life, 
				(endColor.getBlue()-startColor.getBlue())/life, (endColor.getAlpha()-startColor.getAlpha())/life);
	}

	public void render(Graphics g, float xOff, float yOff) {
		g.setColor(active.toAwtColor());
		g.drawImage(texture, x+xOff, y+yOff, width, height);
	}
	
	public void tick(float delta, float xOff, float yOff) {
		life-=delta;
		x+=velX*delta;
		y+=velY*delta;
		width+=scale*delta;
		height+=scale*delta;
		velX+=windX*delta;
		velY+=windY*delta;
		velX=calcFric(velX);
		velY=calcFric(velY);
		active.setRed(active.getRed()+change.getRed()*delta);
		active.setGreen(active.getGreen()+change.getGreen()*delta);
		active.setBlue(active.getBlue()+change.getBlue()*delta);
		active.setAlpha(active.getAlpha()+change.getAlpha()*delta);
	}
	
	private float calcFric(float vel){
		if(vel>0){
			if(vel<friction){
				vel=0;
			}else{
				vel-=friction;
			}
		}else if(vel<0){
			if(vel>friction){
				vel=0;
			}else{
				vel+=friction;
			}
		}
		
		return vel;
	}

	public boolean isDead() {
		return life<0;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}

	public float getVelX() {
		return velX;
	}

	public void setVelX(float velX) {
		this.velX = velX;
	}

	public float getVelY() {
		return velY;
	}

	public void setVelY(float velY) {
		this.velY = velY;
	}

	public float getWindX() {
		return windX;
	}

	public void setWindX(float windX) {
		this.windX = windX;
	}

	public float getWindY() {
		return windY;
	}

	public void setWindY(float windY) {
		this.windY = windY;
	}

	public float getFriction() {
		return friction;
	}

	public void setFriction(float friction) {
		this.friction = friction;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public Color getStartColor() {
		return startColor;
	}

	public void setStartColor(Color startColor) {
		this.startColor = startColor;
	}

	public Color getEndColor() {
		return endColor;
	}

	public void setEndColor(Color endColor) {
		this.endColor = endColor;
	}

	public Image getTexture() {
		return texture;
	}

	public void setTexture(Image texture) {
		this.texture = texture;
	}
	
	
	
}
