package com.thepattybeastlabs.core.graphics.particles;

import java.util.Random;

import com.thepattybeastlabs.core.graphics.Color;
import com.thepattybeastlabs.core.resource.Image;
import com.thepattybeastlabs.core.utils.FixedValue;
import com.thepattybeastlabs.core.utils.Value;

public class DefaultParticleEmitter implements ParticleEmitter {

	protected Value x = new FixedValue(0);
	protected Value y = new FixedValue(0);
	protected Value velX = new FixedValue(0);
	protected Value velY = new FixedValue(0);
	protected Value width = new FixedValue(16);
	protected Value height = new FixedValue(16);
	protected Value life = new FixedValue(240);
	protected Value spawnRate = new FixedValue(0.2f);
	protected Value scale = new FixedValue(0);
	protected Value windX = new FixedValue(0);
	protected Value windY = new FixedValue(0);
	protected Value friction = new FixedValue(0);
	protected Value spawnAmount=new FixedValue(1);
	protected Color startColor=new Color(255, 255, 255, 255), endColor=new Color(255, 255, 255, 0);
	protected Image image;
	protected String setToImage;
	protected float despawnCountdown=Integer.MAX_VALUE;
	private float spawnTimer;
	/**despawn when the despawnCountdown is equal or less than 0*/
	private boolean despawn;
	
	public void tick(ParticleSystem sys, float delta) {
		if(setToImage!=null&&Image.list.get(setToImage)!=null){
			image = Image.list.get(setToImage);
			setToImage=null;
		}
		if(spawnTimer<=0){
			int count = (int)spawnAmount.get();
			spawnTimer = spawnRate.get();
			for(int i = 0; i < count;i++){
				DefaultParticle p = new DefaultParticle(x.get(), y.get(), life.get(), width.get(), height.get(), image, startColor, endColor);
				p.setVelX(velX.get());
				p.setVelY(velY.get());
				p.setWindX(windX.get());
				p.setWindY(windY.get());
				p.setScale(scale.get());
				p.setFriction(friction.get());
				sys.add(p);
			}
		}else{
			spawnTimer-=delta;
		}
		if(despawn&&despawnCountdown>0){
			despawnCountdown-=delta;
		}
	}

	public boolean isDead() {
		return despawn&&despawnCountdown<0;
	}

	public Value getX() {
		return x;
	}

	public void setX(Value x) {
		this.x = x;
	}

	public Value getY() {
		return y;
	}

	public void setY(Value y) {
		this.y = y;
	}

	public Value getVelX() {
		return velX;
	}

	public void setVelX(Value velX) {
		this.velX = velX;
	}

	public Value getVelY() {
		return velY;
	}

	public void setVelY(Value velY) {
		this.velY = velY;
	}

	public Value getLife() {
		return life;
	}

	public void setLife(Value life) {
		this.life = life;
	}

	public Value getSpawnRate() {
		return spawnRate;
	}

	public void setSpawnRate(Value spawnRate) {
		this.spawnRate = spawnRate;
	}

	public Value getWidth() {
		return width;
	}

	public void setWidth(Value width) {
		this.width = width;
	}

	public Value getHeight() {
		return height;
	}

	public void setHeight(Value height) {
		this.height = height;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	public void setImage(String name){
		setToImage=name;
	}

	public Value getScale() {
		return scale;
	}

	public void setScale(Value scale) {
		this.scale = scale;
	}

	public Value getWindX() {
		return windX;
	}

	public void setWindX(Value windX) {
		this.windX = windX;
	}

	public Value getWindY() {
		return windY;
	}

	public void setWindY(Value windY) {
		this.windY = windY;
	}

	public float getDespawnCountdown() {
		return despawnCountdown;
	}

	public void setDespawnCountdown(float despawnCountdown) {
		this.despawnCountdown = despawnCountdown;
	}

	public boolean isDespawn() {
		return despawn;
	}

	/**should despawn when the despawnCountdown is equal or less than 0*/
	public void setDespawn(boolean despawn) {
		this.despawn = despawn;
	}

	public Value getFriction() {
		return friction;
	}

	public void setFriction(Value friction) {
		this.friction = friction;
	}

	public Color getStartColor() {
		return startColor;
	}

	public void setStartColor(Color startColor) {
		this.startColor = startColor;
	}

	public Color getEndColor() {
		return endColor;
	}

	public void setEndColor(Color endColor) {
		this.endColor = endColor;
	}

	public Value getSpawnAmount() {
		return spawnAmount;
	}

	public void setSpawnAmount(Value spawnAmount) {
		this.spawnAmount = spawnAmount;
	}
	
	
	
}
