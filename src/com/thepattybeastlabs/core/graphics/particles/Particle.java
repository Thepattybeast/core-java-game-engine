package com.thepattybeastlabs.core.graphics.particles;

import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.resource.Image;

public interface Particle {

	public void render(Graphics g, float xOff, float yOff);
	public void tick(float delta, float xOff, float yOff);
	public boolean isDead();
	
}
