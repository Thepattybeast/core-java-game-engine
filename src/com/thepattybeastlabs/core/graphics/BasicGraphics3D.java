package com.thepattybeastlabs.core.graphics;

import java.awt.Color;

import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics3D;
import com.thepattybeastlabs.core.resource.Image;

public abstract class BasicGraphics3D implements Graphics3D {

	protected CoreFont font;
	protected float fontHeight;
	protected int fontHAlign;
	protected int fontVAlign;
	
	public void setColor(Color color, int alpha) {
		setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha));
	}

	public void setColor(int r, int g, int b) {
		setColor(new Color(r, g, b));
	}
	
	public void setColor(int r, int g, int b, int a) {
		setColor(new Color(r, g, b, a));
	}

	public Color getColor() {
		return null;
	}
	
	public void drawImage(Image image, float x, float y, float z, float width,
			float height, float rotX, float rotY) {
		if(image!=null)drawImage(image, x, y, z, width, height, rotX, rotY, 0, 0, image.getWidth(), image.getHeight());
	}
	
}
