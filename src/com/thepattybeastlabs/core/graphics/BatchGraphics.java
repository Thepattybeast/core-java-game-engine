package com.thepattybeastlabs.core.graphics;

import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.awt.Color;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;


import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.geom.Matrix4;
import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.geom.Vector3;
import com.thepattybeastlabs.core.resource.Image;

public class BatchGraphics extends BasicGraphics {

	private GLBatchRender r;
	private float clipX, clipY, clipW, clipH;
	
	public BatchGraphics(){
	}
	
	public BatchGraphics(GLBatchRender r){
		this.r = r;
	}
	
	public void fill(Vector2[] shape, int[] indices, float x, float y, float width,
			float height, Color[] colors) {
		r.setRenderMode(r.GL_TRIANGLES);
		r.bindTexture(null);
		int start = r.getVeticeCount();
		Color old = getColor();
		for(int i = 0; i < shape.length; i++){
			if(colors!=null)setColor(colors[i]);
			r.vertex(shape[i].x*width+x, shape[i].y*height+y);
		}
		setColor(old);
		for(int i = 0; i < indices.length; i++){
			r.indice(start+indices[i]);
		}
	}

	public void draw(Vector2[] shape, int[] indices, float x, float y, float width,
			float height, Color[] colors) {
		r.setRenderMode(r.GL_LINES);
		r.bindTexture(null);
		Color old = getColor();
		for(int i = 0; i < shape.length; i++){
			if(colors!=null)setColor(colors[i]);
			r.vertex(shape[i].x*width+x, shape[i].y*height+y);
			r.indiceEnd(0);
		}
		setColor(old);
	}

	public void drawString(String text, float x, float y, int off, int len) {
		drawString(text, x, y, off, len, null);
	}

	public void drawString(String text, float x, float y, int off, int len, Color textBG) {
		if(font==null)return;
		if(fontHeight < 0)fontHeight = font.getHeight();
		if(fontHAlign==1)x-=(stringWidth(text)/2);
		if(fontHAlign==2)x-=(stringWidth(text));
		if(fontVAlign==1)y-=(fontHeight/2);
		if(fontVAlign==2)y-=(fontHeight);
		//System.out.println(fontHeight);
		int offset=0;
		float scale = fontHeight/font.getHeight();
		if(textBG!=null){
			Color old = getColor();
			setColor(textBG);
			fillRect(x, y, stringWidth(text, off, len), fontHeight);
			setColor(old);
		}
		for(int i = off; i < off+len; i++){
			for(int j = 0; j < font.getCharlist().length; j++){
				if(font.getCharlist()[j].getLetter()==text.charAt(i)){
					drawImage(font.getImage(), x+offset, y, (font.getCharlist()[j].getWidth()*scale), fontHeight, font.getCharlist()[j].getPos(), font.getCharlist()[j].getHOff(), font.getCharlist()[j].getWidth(), font.getHeight());
					offset+=(font.getCharlist()[j].getWidth()*scale)+3;
				}
			}
		}
	}
	
	public void drawImage(Image image, float x, float y, float width,
			float height, float imgX, float imgY, float imgWidth,
			float imgHeight) {
		imgX+=image.getX();imgY+=image.getY();
		r.setRenderMode(GL11.GL_TRIANGLES);
		r.bindTexture(image);
		
		r.vertex(x, y, image.getLeftUV(imgX), image.getTopUV(imgY));
		r.vertex(x+width, y, image.getRightUV(imgX, imgWidth), image.getTopUV(imgY));
		r.vertex(x+width, y+height,image.getRightUV(imgX, imgWidth), image.getBottomUV(imgY, imgHeight));
		r.vertex(x, y+height, image.getLeftUV(imgX), image.getBottomUV(imgY, imgHeight));
		
		r.indiceEnd(3);r.indiceEnd(2);r.indiceEnd(1);
		r.indiceEnd(3);r.indiceEnd(1);r.indiceEnd(0);
	}
	public void drawImage(Image image, float x, float y, float width,
			float height, float imgX, float imgY, float imgWidth,
			float imgHeight, float rot) {
		r.setRenderMode(GL11.GL_TRIANGLES);
		r.bindTexture(image);
		//rot+=90;
		//rot %=360;
		float sin = (float)Math.sin(Math.toRadians(rot));
		float cos = (float)Math.cos(Math.toRadians(rot));
		float sin2 = (float)Math.sin(Math.toRadians(rot));
		float cos2 = (float)Math.cos(Math.toRadians(rot));
		//System.out.println(sin + ", " + cos);
		height/=2;width/=2;
		x+=width;y+=height;
		/*r.vertex(x-((width)*cos), y-((height)*sin), imgX/image.getRealWidth(), imgY/image.getRealHeight());
		r.vertex(x+((width)*sin), y-((height)*cos), (imgX+imgWidth)/image.getRealWidth(), imgY/image.getRealHeight());
		r.vertex(x+((width)*cos), y+((height)*sin),(imgX+imgWidth)/image.getRealWidth(), (imgY+imgHeight)/image.getRealHeight());
		r.vertex(x-((width)*sin), y+((height)*cos), (imgX)/image.getRealWidth(), (imgY+imgHeight)/image.getRealHeight());*/
		rv(x,y,-width, -height, cos , sin , image.getLeftUV(imgX), image.getTopUV(imgY));
		rv(x,y,width, -height, cos2, sin2, image.getRightUV(imgX, imgWidth), image.getTopUV(imgY));
		rv(x,y,width,  height, cos, sin, image.getRightUV(imgX, imgWidth), image.getBottomUV(imgY, imgHeight));
		rv(x,y,-width,  height, cos2, sin2, image.getLeftUV(imgX), image.getBottomUV(imgY, imgHeight));
		r.indiceEnd(3);
		r.indiceEnd(2);
		r.indiceEnd(1);
		r.indiceEnd(3);
		r.indiceEnd(1);
		r.indiceEnd(0);
	}
	
	private void rv(float cx, float cy,float x, float y, float cos, float sin, float uvX, float uvY){
		r.vertex(rx(cx, cy, x, y, cos, sin), ry(cx, cy, x, y, cos, sin), uvX, uvY);
	}
	
	private float rx(float cx, float cy,float x, float y, float cos, float sin){
		return cx+x*cos-y*sin;
	}
	private float ry(float cx, float cy,float x, float y, float cos, float sin){
		return cy+y*cos+x*sin;
	}

	public void setColor(Color color) {
		r.setColor(color);
	}

	public String getName() {
		return "Batch Renderer";
	}

	public void create() throws Exception {
		if(r==null)r = new GLBatchRender(10000, Matrix4.ortho(0, Core.width, Core.height, 0, -1, 1));
		Core.gl.glEnable(Core.gl.GL_TEXTURE_2D);
		Core.gl.glEnable(GL11.GL_BLEND);
		if(Core.useCullFace)Core.gl.glEnable(GL11.GL_CULL_FACE);
		if(Core.useCullFace)GL11.glCullFace(GL11.GL_FRONT);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	}

	public void destory() throws Exception {
		GL20.glUseProgram(0);
	}

	public int stringWidth(String text, int off, int len) {
		if(font==null)return 0;
		if(fontHeight < 0)fontHeight = font.getHeight();
		int offset=0;
		float scale = fontHeight/font.getHeight();
		for(int i = off; i < off+len; i++){
			for(int j = 0; j < font.getCharlist().length; j++){
				if(font.getCharlist()[j].getLetter()==text.charAt(i)){
					offset+=(font.getCharlist()[j].getWidth()*scale)+3;
				}
			}
		}
		return offset;
	}

	public void setClip(float x, float y, float width, float height) {
		this.clipX=x;this.clipY=y;this.clipW = width;this.clipH = height;
		
		r.setClip(x, y, width, height, true);
	}

	public void rotate(float rot, float width, float height) {
		throw new RuntimeException("Not Implemented");
	}

	public void rotateTo(float rot, float width, float height) {
		throw new RuntimeException("Not Implemented");
	}
	
	public void setRenderingScale(int width, int height){
		r.setPerspective(Matrix4.ortho(0, width, height, 0, -1, 1));
	}

	public float getRotation() {
		return 0;
	}

	public void setClearColor(Color color) {
		glClearColor(((float)color.getRed()/255f),((float)color.getGreen()/255f),((float)color.getBlue()/255f),((float)color.getAlpha()/255f));
	}

	public void fillRect(float x, float y, float width, float height) {
		//x+=clipX;
		//y+=clipY;
		//if(x < clipX){width+=(x-clipX);x=clipX;}
		//if(y < clipY){height+=(y-clipY);y=clipY;}
		//if(x+width > clipX+clipW){width-=((x+width)-(clipX + clipW));}
		r.setRenderMode(GL11.GL_TRIANGLES);
		r.bindTexture(null);
		r.vertex(x, y);
		r.vertex(x+width, y);
		r.vertex(x+width, y+height);
		r.vertex(x, y+height);
		r.indiceEnd(3);
		r.indiceEnd(2);
		r.indiceEnd(1);
		r.indiceEnd(3);
		r.indiceEnd(1);
		r.indiceEnd(0);
	}

	public void drawRect(float x, float y, float width, float height) {
		r.setRenderMode(GL11.GL_LINES);
		r.bindTexture(null);
		r.vertex(x, y);
		r.vertex(x+width, y);
		r.vertex(x+width, y+height);
		r.vertex(x, y+height);
		r.indiceEnd(3);
		r.indiceEnd(2);
		r.indiceEnd(2);
		r.indiceEnd(1);
		r.indiceEnd(1);
		r.indiceEnd(0);
		r.indiceEnd(0);
		r.indiceEnd(3);
	}

	public void drawLine(float x, float y, float x2, float y2) {
		r.setRenderMode(GL11.GL_LINES);
		r.bindTexture(null);
		r.vertex(x, y);
		r.vertex(x2, y2);
		r.indiceEnd(1);
		r.indiceEnd(0);
	}
	
	public void finish(){
		setColor(Color.white);
		r.flush();
	}

	public void drawCircle(float x, float y, float width, float height) {
		r.setRenderMode(r.GL_LINES);
		r.bindTexture(null);
		width /=2;height/=2;
		x+=width;y+=height;
		r.vertex(x, y+height);
		for(int i = 0; i < 361; i++){
			r.indiceEnd(0);
			r.vertex(x+((float)Math.sin(Math.toRadians(i))*width), y+((float)Math.cos(Math.toRadians(i))*height));
			r.indiceEnd(0);
		}
	}

	public void fillCircle(float x, float y, float width, float height) {
		r.setRenderMode(r.GL_TRIANGLES);
		r.bindTexture(null);
		width /=2;height/=2;
		x+=width;y+=height;
		r.vertex(x+width, y+height);
		int center = r.getVeticeCount();
		r.vertex(x, y-height);
		for(int i = 0; i < 361; i++){
			r.indiceEnd(0);
			r.indice(center);
			r.vertex(x+((float)Math.sin(Math.toRadians(i))*width), y+((float)Math.cos(Math.toRadians(i))*height));
			r.indiceEnd(0);
		}
	}

	@Override
	public boolean hasBatchRender() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public GLBatchRender getBatchRender() {
		// TODO Auto-generated method stub
		return r;
	}

}
