package com.thepattybeastlabs.core.graphics;

import java.awt.Color;

import static org.lwjgl.opengl.GL20.*;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.geom.Matrix4;
import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.geom.Vector3;
import com.thepattybeastlabs.core.resource.Image;
import com.thepattybeastlabs.core.resource.ResourceLoadingUtils;

public class Graphics3D11 extends BasicGraphics3D {

	private Color color;
	private GLBatchRender r;
	
	public void create() throws Exception {
		System.out.println("3D Graphics Created");
		r = new GLBatchRender(0, Matrix4.projection(75, (float)Core.width/(float)Core.height, 0.1f, 1000));
	}
	
	public void flush() {
		r.flush();
	}

	public void enter() {
		
	}

	public void fillCube(float x, float y, float z, float width, float height,
			float depth) {
		
	}
	
	public void fillRect(float x, float y, float z, float width, float height, float rotX, float rotY){
		r.bindTexture(null);
		rotX %= 360;
		rotX = (float)Math.toRadians(rotX);
		rotY %= 360;
		rotY = (float)Math.toRadians(rotY);
		float xSin = (float)Math.sin(rotX);
		float xCos = (float)Math.cos(rotX);
		float ySin = (float)Math.sin(rotY);
		float yCos = (float)Math.cos(rotY);
		float depth = width*(xSin);
		float xOff = width*xSin*yCos;
		float zOff = width*xCos*yCos;
		width = width*(xCos);
		height = height*ySin;
		r.setRenderMode(r.GL_TRIANGLES);
		r.vertex(x, y, z);
		r.vertex(x+xOff, y+height, z+zOff);
		r.vertex(x+xOff+width, y+height, z+depth+zOff);
		r.vertex(x, y, z);
		r.vertex(x+xOff+width, y+height, z+depth+zOff);
		r.vertex(x+width, y, z+depth);
		
	}
	
	public void drawImage(Image image, float x, float y, float z, float width,
			float height, float rotX,float rotY, float imgX, float imgY, float imgWidth,
			float imgHeight) {
		r.setRenderMode(r.GL_TRIANGLES);
		r.bindTexture(image);
		rotX %= 360;
		rotX = (float)Math.toRadians(rotX);
		rotY %= 360;
		rotY = (float)Math.toRadians(rotY);
		float xSin = (float)Math.sin(rotX);
		float xCos = (float)Math.cos(rotX);
		float ySin = (float)Math.sin(rotY);
		float yCos = (float)Math.cos(rotY);
		float depth = width*(xSin);
		float xOff = width*xSin*yCos;
		float zOff = width*xCos*yCos;
		width = width*(xCos);
		height = height*ySin;
		r.vertex(x, y, z, (imgX)/image.getRealWidth(), (imgY+imgHeight)/image.getRealHeight());
		r.vertex(x+xOff, y+height, z+zOff, (imgX)/image.getRealWidth(), (imgY)/image.getRealHeight());
		r.vertex(x+width+xOff, y+height,z+zOff+depth, (imgX+imgWidth)/image.getRealWidth(), (imgY)/image.getRealHeight());
		r.vertex(x, y, z, imgX/image.getRealWidth(), (imgY+imgHeight)/image.getRealHeight());
		r.vertex(x+width+xOff, y+height,z+zOff+depth, (imgX+imgWidth)/image.getRealWidth(), (imgY)/image.getRealHeight());	
		r.vertex(x+width, y, z+depth, (imgX+imgWidth)/image.getRealWidth(), (imgY+imgHeight)/image.getRealHeight());
	}
	
	public void translate(float x, float y, float z){
		//glTranslatef(x, y, z);
	}
	
	public void setTranslation(float x, float y, float z){
		r.setViewportTranlation(x, y, z);
	}
	
	public void rotate(float x, float y, float z){
		r.setViewportRotation(x, y, z);
	}
	
	public void draw(Vector3[] shape, float x, float y, float z, float width,
			float height, float depth, Color[] colors) {
		throw new RuntimeException("Not Implemented");
	}
	
	public void fill(Vector3[] shape, float x, float y, float z, float width,
			float height, float depth, Color[] colors) {
		throw new RuntimeException("Not Implemented");
	}
	
	public void setColor(Color color){
		if(color==null)color = Color.WHITE;
		this.color = color;
		r.setColor(color);
		//Core.glCalls+=1;
		//glColor4f(((float)color.getRed()/255f),((float)color.getGreen()/255f),((float)color.getBlue()/255f),((float)color.getAlpha()/255f));
	}
	
	public void setColor(Color color, int alpha){
		setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha));
	}
	
	public void setColor(int r, int g, int b){
		setColor(new Color(r, g, b));
	}
	
	public void drawTriangles(Vector3[] triangles, int[] indices, int x, int y, int z){
		if(triangles==null)return;
		r.bindTexture(null);
		r.setRenderMode(r.GL_TRIANGLES);
		for(int i =0 ; i < triangles.length; i++){
			r.vertex(x+triangles[i].x, y+triangles[i].y, z+triangles[i].z);
		}
		for(int i = 0; i < indices.length; i++){
			r.indiceEnd(triangles.length-indices[i]);
		}
	}
	
	public Color getColor(){
		return color;
	}
	
	public void rotateTo(float rot, float width, float height, float depth, boolean x, boolean y, boolean z) {
	}

}
