package com.thepattybeastlabs.core.graphics;

import java.awt.Color;
import java.awt.Rectangle;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.CoreFont;
import com.thepattybeastlabs.core.Graphics;
import com.thepattybeastlabs.core.resource.Image;

public abstract class BasicGraphics implements Graphics {
	
	protected CoreFont font;
	protected float fontHeight;
	protected int fontHAlign;
	protected int fontVAlign;

	public void drawImage(Image image, float x, float y, float width,
			float height) {
		if(image!=null)drawImage(image, x, y, width, height, 0, 0, image.getWidth(), image.getHeight());
	}
	
	public void drawImage(Image image, float x, float y, float width,
			float height, float rot) {
		if(image!=null)drawImage(image, x, y, width, height, 0, 0, image.getWidth(), image.getHeight(), rot);
	}

	public void setColor(Color color, int alpha) {
		setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha));
	}

	public void setColor(int r, int g, int b) {
		setColor(new Color(r, g, b));
	}
	
	public void setColor(int r, int g, int b, int a) {
		setColor(new Color(r, g, b, a));
	}

	public Color getColor() {
		return null;
	}

	public void setTextAlign(int horizontal, int vertical) {
		fontVAlign = vertical;
		fontHAlign = horizontal;
	}
	
	public void drawString(String text, float x, float y){
		drawString(text, x, y, 0, text.length());
	}
	
	public void drawString(String text, float x, float y, Color backgroundColor){
		drawString(text, x, y, 0, text.length(), backgroundColor);
	}

	public void setFont(CoreFont font, float size) {
		fontHeight = size;
		if(font==null)return;
		this.font = font;	
	}

	public void setFont(String font, float size) {
		this.font = CoreFont.list.get(font);
		fontHeight = size;
	}
	
	public CoreFont getFont(){
		return font;
	}
	
	public void resetClip(){
		Rectangle rect = Core.getViewBounds();
		setClip(0, 0, 0, 0);
	}
	
	public int stringWidth(String text){
		return stringWidth(text, 0, text.length());
	}

}
