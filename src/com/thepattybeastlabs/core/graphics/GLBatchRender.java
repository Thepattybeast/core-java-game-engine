package com.thepattybeastlabs.core.graphics;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.OpenGLException;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.geom.Matrix4;
import com.thepattybeastlabs.core.geom.Vector3;
import com.thepattybeastlabs.core.opengl.GLContext;
import com.thepattybeastlabs.core.resource.Image;
import com.thepattybeastlabs.core.resource.ResourceLoadingUtils;
import com.thepattybeastlabs.core.shader.Shader;
import com.thepattybeastlabs.core.shader.Shader.ShaderType;
import com.thepattybeastlabs.core.shader.ShaderProgram;
import com.thepattybeastlabs.core.utils.ReadOnlyList;

import static org.lwjgl.opengl.GL20.*;

public class GLBatchRender {

	public static final String COLOR_ATTRIB_NAME="color";
	public static final String VERTEX_POSITION_ATTRIB_NAME="vertexPosition";
	public static final String TEXTURE_ATTRIB_NAME="tex";
	
	public static final String PROJECTION_UNIFORM_NAME="proj";
	public static final String TRANFORM_UNIFORM_NAME="trans";
	
	public static final String VS_DATA="#version 120\nattribute vec3 vertexPosition;\n"
			+ "attribute vec4 color;\nattribute vec2 tex;\n varying vec4 fColor; varying vec2 fTex;"
			+ "uniform mat4 proj;uniform mat4 trans;"
			+ "\nvoid main(){gl_Position=proj*trans*vec4(vertexPosition,1.0);fColor=color;fTex=tex;\n}";
	public static final String FRAG_DATA = "#version 120\nvarying vec4 fColor;varying vec2 fTex;uniform sampler2D mts;"
			+ " void main(){vec4 texColor=texture2D(mts, fTex);\nvec4 setColor = ((fColor+128)/255);\n"
			+ "gl_FragColor = vec4(min(texColor.x, setColor.x),min(texColor.y, setColor.y),min(texColor.z, setColor.z), min(texColor.a, setColor.a));}";
	//gl_FragColor = (fColor+128)/255;
	//layout(location=1) 
	private static Image blankTex = null;
	public static boolean WIREFRAME = false;
	private static List<GLBatchRender> list = new ArrayList<>();
	public static final ReadOnlyList<GLBatchRender> RENDERERS = new ReadOnlyList<>(list);
	
	public static void recreateGL(){
		for(GLBatchRender r : RENDERERS){
			r.createGL();//TODO preserve custom shaders
		}
	}
	
	public static void destoryGL(){
		for(GLBatchRender r : RENDERERS){
			r.unload();
		}
	}
	
	private ByteBuffer vertices;
	private IntBuffer indices;
	private int verticesId, indicesId;
	private int indicesPut, verticesPut;
	private int shaders;
	private static ShaderProgram shader;
	private Matrix4 projection, transform, forceTransform;
	//private int projId, transId;
	public static int inc = 24;
	private Color color = new Color(200,1,140, 100);
	private Image activeTexture = blankTex;
	
	private ArrayList<RenderSection> sections = new ArrayList<>();
	private float clipX, clipY, clipWidth, clipHeight;
	//private int colorLoc, vertexLoc, UVLoc;
	private Vector3 viewPortTranslation = new Vector3(0, 0, 0);
	private Vector3 viewPortRotation = new Vector3(0, 0, 0);
	private int renderMode = 0, beginIndex, /**Count of vertices in the specified shape (render mode)*/vertexShapeCount;
	private boolean resized = false;
	private boolean uploadMatrix = false;
	private boolean clearOnPaint = true;
	private boolean changed = false;
	private RenderSection section;

	public final int GL_TRIANGLES, GL_TRIANGLES_VERTEX_COUNT = 3;
	public final int GL_QUADS, GL_QUADS_VERTEX_COUNT = 4;
	public final int GL_LINES, GL_LINES_VERTEX_COUNT = 2;
	
	/**@param startingSize The starting size of the indice buffer(in floats, the final size in bytes will be: startingSize * 24) maximum size 11,184,810 vertices
	   @peram proj The Projection matrix see Matrix4 class
	   @peram gl The GLContext use Core.gl
	   @peram programId the id of the shader program see ResourceLoadingUtils pass in -1 for the default shader**/
	public GLBatchRender(int startingSize, Matrix4 proj){
		GL_TRIANGLES = GL11.GL_TRIANGLES;
		GL_QUADS = GL11.GL_QUADS;
		GL_LINES = GL11.GL_LINES;
		renderMode = WIREFRAME ? GL11.GL_LINE_LOOP : GL_TRIANGLES;
		vertexShapeCount = GL_TRIANGLES_VERTEX_COUNT;
		
		Image.size+=(startingSize)+(startingSize*inc);
		indices = BufferUtils.createIntBuffer(startingSize);
		if(indices.capacity()!=startingSize)System.out.println("[WARN] Expected buffer capacity to be "+ startingSize + " got " + indices.capacity());
		indices.position(0);

		vertices = BufferUtils.createByteBuffer(startingSize*inc);

		projection = proj;
		transform = Matrix4.blankProjection();
		
		createGL();
		//check variables
		//check(projId, "proj","mat4");
		//check(transId, "trans","mat4");
		uploadMatrix = true;
		list.add(this);
	}
	
	private void createGL(){
		BufferedImage blankTemp = new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR);
		blankTemp.setRGB(0, 0, Color.WHITE.getRGB());
		if(blankTex==null)try {blankTex = new Image(blankTemp);blankTex.load();} catch (Exception e) {e.printStackTrace();}
		indicesId = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_DYNAMIC_DRAW);
		verticesId = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, verticesId);
		glBufferData(GL_ARRAY_BUFFER, vertices, GL_DYNAMIC_DRAW);
		
		if(shader==null){
			try {
				shader = new ShaderProgram(new Shader(ShaderType.VERTEX, VS_DATA),new Shader(ShaderType.FRAGMENT, FRAG_DATA));
				shader.load();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		glUseProgram(shaders);
	}
	
	public void increaseSize(){
		if(clearOnPaint==false)throw new RuntimeException("Buffer Overflow in " + this.toString() + " at " +indicesPut + "/" + indices.capacity());
		IntBuffer tempI = indices;
		indices = BufferUtils.createIntBuffer(tempI.capacity()+10000);
		indices.position(0);
		vertices = BufferUtils.createByteBuffer((tempI.capacity()+10000)*16);
		vertices.position(0);
		
		System.out.println("[WARN] Resized arrays, new size is " + indices.capacity() + " vertices and indices.");

		resized = true;
	}
	
	public void setViewportTranlation(float x, float y, float z){
		viewPortTranslation.x = x;viewPortTranslation.y = y;viewPortTranslation.z = z;
	}
	
	public void setViewportRotation(float x, float y, float z){
		viewPortRotation.x = x;viewPortRotation.y = y;viewPortRotation.z = z;
	}
	
	/**Force this batchrenderer to use a specific transform set to null to disable*/
	public void setForcedTransform(Matrix4 matrix){
		this.forceTransform = matrix;
	}
	
	public void setColor(Color color){
		if(color==null)color = Color.WHITE;
		this.color = color;
	}
	
	/**addOffset= add clip x and y to vertex x and y*/
	public void setClip(float x, float y, float width, float height, boolean addOffset){
		newSection();
		section.setClip(width!=0&&height!=0);
		section.setClipBounds((int)x, (int)y, (int)width, (int)height);
		section.setAddClipoffset(addOffset);
	}
	
	public void check(int id, String name, String type){
		if(id==-1){
			throw new InvalidShaderException("Missing field '"+name+"' of type '"+type+"'.");
		}
	}
	
	public void vertex(float x, float y, float z, float uvx, float uvy){
		//finished = false;
		if(vertices.remaining() < inc){increaseSize();}
		if(section.isAddClipoffset()){
			x+=section.getClipX();
			y+=section.getClipY();
		}
		vertices.putFloat(x);
		vertices.putFloat(y);
		vertices.putFloat(z);
		vertices.put(new byte[]{(byte)(color.getRed()+128), (byte)(color.getGreen()+128), (byte)(color.getBlue()+128), (byte)(color.getAlpha()+128)});
		vertices.putFloat(uvx);
		vertices.putFloat(uvy);
		verticesPut++;
		changed = true;
	}
	
	public void indice(int indice){
		changed = true;
		//if(!clearOnPaint)System.out.println("iput: " + indicesPut);
		if(indices.remaining()<=0){increaseSize();}
		
		if(sections.size()==0)setRenderMode(GL_TRIANGLES);
		try{
		indices.put(indice);indicesPut+=1;
		}catch(BufferOverflowException e){
			System.err.println("Indice Buffer Overflow, Attempting to increase buffer size.");
			increaseSize();
		}
		
		
	}
	
	public void unload(){
		glDeleteBuffers(verticesId);
		glDeleteBuffers(indicesId);
		
	}

	public void destroy(){
		unload();
		list.remove(this);
	}
	
	public void indiceEnd(int offset){
		indice(verticesPut-offset-1);
	}
	
	public void bindTexture(Image i){
		if(i==null)i=blankTex;
		if(section!=null&&i.getId()==section.getTex().getId())return;
		newSection().setTex(i);
	}
	
	public void bindShader(ShaderProgram shader){
		if(shader==null)shader=GLBatchRender.shader;
		if(section!=null&&shader.getId()==section.getShader().getId())return;
		newSection().setShader(shader);
	}
	
	public void setRenderMode(int mode){
		if(section!=null&&mode==section.getRenderMode())return;
		newSection().setRenderMode(mode);
		beginIndex = indicesPut;
		
	}
	
	private RenderSection newSection(){
		if(sections.size() > 0&&section!=null)section.setEnd(indicesPut);
		RenderSection r = new RenderSection(indicesPut, section, blankTex, shader);
		sections.add(r);
		section=r;
		return r;
	}
	
	public void vertex(float x, float y, float uvx, float uvy){
		vertex(x, y, 0, uvx, uvy);
	}
	
	public void vertex(float x, float y){
		vertex(x, y, 0, 0, 0);
	}
	
	public void vertex(float x, float y, float z){
		vertex(x, y, z, 0, 0);
	}
	
	public void clear(){
		vertices.clear();
		indices.clear();
		indicesPut = 0;
		verticesPut = 0;
		sections.clear();
	}
	
	public void finish(){
		vertices.flip();
		indices.flip();
		if(sections.size() > 0)sections.get(sections.size()-1).setEnd(indicesPut);
		section = null;
	}
	
	private void enableShader(ShaderProgram s){
		glUseProgram(s.getId());
		glEnableVertexAttribArray(s.getAttrib(VERTEX_POSITION_ATTRIB_NAME));
		glEnableVertexAttribArray(s.getAttrib(COLOR_ATTRIB_NAME));
		glEnableVertexAttribArray(s.getAttrib(TEXTURE_ATTRIB_NAME));
		glVertexAttribPointer(s.getAttrib(VERTEX_POSITION_ATTRIB_NAME), 3, GL_FLOAT, false, inc, 0);
		glVertexAttribPointer(s.getAttrib(COLOR_ATTRIB_NAME), 4, GL_BYTE, false, inc, 12);
		glVertexAttribPointer(s.getAttrib(TEXTURE_ATTRIB_NAME), 2, GL_FLOAT, true, inc, 16);
		glUniformMatrix4fv(s.getUniform(PROJECTION_UNIFORM_NAME), false, projection.asBuffer());
		glUniformMatrix4fv(s.getUniform(TRANFORM_UNIFORM_NAME), false, transform.asBuffer());
	}
	
	private void disableShader(ShaderProgram s){
		glDisableVertexAttribArray(s.getAttrib(VERTEX_POSITION_ATTRIB_NAME));
		glDisableVertexAttribArray(s.getAttrib(COLOR_ATTRIB_NAME));
		glDisableVertexAttribArray(s.getAttrib(TEXTURE_ATTRIB_NAME));
	}
	
	public void flush(){
		if(indicesPut==0)return;
		if(WIREFRAME)renderMode = GL11.GL_LINE_LOOP;
		if(forceTransform==null){
			transform.clear();
			transform = transform.mul(Matrix4.translate(viewPortTranslation.x, viewPortTranslation.y, viewPortTranslation.z));
			transform = transform.mul(Matrix4.rotation(viewPortRotation.x, viewPortRotation.y, viewPortRotation.z));
		}else{
			transform = forceTransform;
		}
		
		if(clearOnPaint)finish();
		
		
		glBindBuffer(GL_ARRAY_BUFFER, verticesId);
		if(changed){
			if(resized){
				vertices.position(0);
				vertices.limit(vertices.capacity());
				glBufferData(GL_ARRAY_BUFFER, vertices, GL_DYNAMIC_DRAW);
				glGetBufferParameteri(GL_BUFFER_SIZE, verticesId);
			}
			else glBufferSubData(GL_ARRAY_BUFFER, 0, vertices);
		}
		
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesId);
		if(changed){
			if(resized){
				indicesId = glGenBuffers();
				indices.position(0);
				indices.limit(indices.capacity());
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesId);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_DYNAMIC_DRAW);
			}
			else glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indices);
		}
		
		
		if(!resized){
			ShaderProgram activeShader=null;
			for(int i = 0; i < sections.size(); i++){
				RenderSection s = sections.get(i);
				if(s.getTex()!=null&&s.getTex().getProps()!=null&&(!WIREFRAME||s.getTex()==blankTex))glBindTexture(GL_TEXTURE_2D, s.getTex().getId());
				if(s.getShader()!=null&&s.getShader()!=activeShader&&s.getShader().isLoaded()){
				glBindBuffer(GL_ARRAY_BUFFER, verticesId);
				if(activeShader!=null){
					glUseProgram(s.getShader().getId());
					disableShader(activeShader);
				}
				
				glUseProgram(s.getShader().getId());
				enableShader(s.getShader());
				activeShader=s.getShader();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesId);
			}
			if(s.isClip()){
				glEnable(GL_SCISSOR_TEST);
				float xs = (float)Core.winWidth/(float)Core.width;
				float ys = (float)Core.winHeight/(float)Core.height;
				glScissor((int)((float)s.getClipX()*xs), (int)(Core.winHeight-((float)s.getClipY()*ys)-((float)s.getClipHeight()*ys))
						, (int)((float)s.getClipWidth()*xs), (int)((float)s.getClipHeight()*ys));
			}else{
				glDisable(GL_SCISSOR_TEST);
			}
			if(s.getEnd()-s.getBegin()>0)glDrawElements(WIREFRAME?GL_LINES:s.getRenderMode(), s.getEnd()-s.getBegin(), GL_UNSIGNED_INT, s.getBegin()*4);
			Core.glCalls+=1;
				
			}
		}
		
		if(clearOnPaint)clear();
		resized = false;
		changed = false;
	}
	
	public int getVeticeCount(){
		return verticesPut;
	}
	
	private void checkError(){
		int err = glGetError();
		if(glGetError()!=GL_NO_ERROR){
			System.out.println(new OpenGLException(err).getMessage() + " ["+err+"]");
		}
	}
	
	public class InvalidShaderException extends RuntimeException{

		public InvalidShaderException() {
			super();
		}

		public InvalidShaderException(String message, Throwable cause,
				boolean enableSuppression, boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}

		public InvalidShaderException(String message, Throwable cause) {
			super(message, cause);
		}

		public InvalidShaderException(String message) {
			super(message);
		}

		public InvalidShaderException(Throwable cause) {
			super(cause);
		}
		
	}
	
	
	public boolean isClearOnPaint() {
		return clearOnPaint;
	}

	public void setClearOnPaint(boolean clearOnPaint) {
		this.clearOnPaint = clearOnPaint;
	}

	public void setPerspective(Matrix4 ortho) {
		projection = ortho;
		uploadMatrix = true;
	}

	public List<RenderSection> getSections() {
		return sections;
	}
	
}
