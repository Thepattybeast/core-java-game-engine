package com.thepattybeastlabs.core.graphics;

import java.awt.Graphics;

import org.lwjgl.opengl.GL11;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.Graphics3D;
import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.geom.Vector3;
import com.thepattybeastlabs.core.input.Input;

public class Camera3D {

	private Graphics3D g;
	public Vector3 pos;
	public Vector3 rot;
	private Vector2 lastMouse;
	public Camera3D(Graphics3D g){
		this.g = g;
		pos = new Vector3(0, 0, 5);
		rot = new Vector3(0, 0, 0);
	}
	
	public void tick(Input in){
		if(in.isDown("forward")){
			pos.z-=Math.cos(Math.toRadians(rot.y))*0.1;
			pos.x+=Math.sin(Math.toRadians(rot.y))*0.1;
			pos.y+=Math.sin(Math.toRadians(rot.x))*0.1;
		}
		if(in.isDown("back")){
			pos.z+=Math.cos(Math.toRadians(rot.y))*0.1;
			pos.x-=Math.sin(Math.toRadians(rot.y))*0.1;
			pos.y-=Math.sin(Math.toRadians(rot.x))*0.1;
		}
		if(in.isDown("left")){
			pos.z-=Math.cos(Math.toRadians(rot.y+90))*0.1;
			pos.x+=Math.sin(Math.toRadians(rot.y+90))*0.1;
		}
		if(in.isDown("right")){
			pos.z-=Math.cos(Math.toRadians(rot.y-90))*0.1;
			pos.x+=Math.sin(Math.toRadians(rot.y-90))*0.1;
		}
		Vector2 mouse = Core.sys.getMouseInfo();
		if(lastMouse==null)lastMouse = mouse;
		if(g!=null)g.setTranslation(pos.x, pos.y, pos.z);
		rot.x+=(mouse.y-lastMouse.y)*0.7f;
		rot.y-=(mouse.x-lastMouse.x)*0.7f;
		if(g!=null)g.rotate(rot.x, rot.y, rot.z);
		lastMouse = mouse;
		//Mouse.setCursorPosition(Display.getWidth()/2, Display.getHeight()/2);
	}
	
	
	
	public Vector3 getPos() {
		return pos;
	}

	public void setPos(Vector3 pos) {
		this.pos = pos;
	}

	public Vector3 getRot() {
		return rot;
	}

	public void setRot(Vector3 rot) {
		this.rot = rot;
	}

	public void applyTransform(){
		//if(pos!=null)g.translate(pos.x, pos.y, pos.z);
		
		//if(rot!=null)g.rotate(rot.x, 0, 0, 0, true, false, false);
		//if(rot!=null)g.rotate(rot.y, 0, 0, 0, false, true, false);
		
	}
	
	public void reverseTransform(){
		//if(rot!=null)g.rotate(-rot.x, 0, 0, 0, true, false, false);
		//if(rot!=null)g.rotate(-rot.y, 0, 0, 0, false, true, false);
		//if(pos!=null)g.translate(-pos.x, -pos.y, -pos.z);
		//GL11.glLoadIdentity();
	}
	
}
