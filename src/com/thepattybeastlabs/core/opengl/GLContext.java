package com.thepattybeastlabs.core.opengl;

import static org.lwjgl.opengl.GL11.glTexParameterf;

import java.nio.ByteBuffer;

public abstract class GLContext {
	
	public final int GL_COLOR_BUFFER_BIT;
	public final int GL_DEPTH_BUFFER_BIT;
	public final int GL_DEPTH_TEST;
	public final int GL_TEXTURE_2D;
	public final int GL_RGBA;
	public final int GL_UNSIGNED_BYTE;
	public final int GL_UNSIGNED_INT;
	public final int GL_UNSIGNED_SHORT;
	public final int GL_TEXTURE_MAG_FILTER;
	public final int GL_TEXTURE_MIN_FILTER;
	public final int GL_NEAREST;
	public final int GL_LINEAR;
	public final int GL_LINK_STATUS;
	public final int GL_TRUE;
	public final int GL_FALSE;
	public final int GL_TRIANGLES;
	public final int GL_POLYGON;
	public final int GL_QUADS;
	public final int GL_LINE_LOOP;
	public final int GL_LINES;
	public final int GL_ELEMENT_ARRAY_BUFFER;
	public final int GL_STATIC_DRAW;
	public final int GL_DYNAMIC_DRAW;
	public final int GL_ARRAY_BUFFER;
	public final int GL_VERTEX_SHADER;
	public final int GL_FRAGMENT_SHADER;
	public final int GL_FLOAT;
	public final int GL_INT;
	public final int GL_SHORT;
	public final int GL_BYTE;
	
	/**Constructor for GLContext takes all final variables in the GLContext class*/
	public GLContext(int gL_COLOR_BUFFER_BIT, int gL_DEPTH_BUFFER_BIT,
			int gL_DEPTH_TEST, int gL_TEXTURE_2D, int gL_RGBA,
			int gL_UNSIGNED_BYTE, int gL_UNSIGNED_INT, int gL_UNSIGNED_SHORT,
			int gL_TEXTURE_MAG_FILTER, int gL_TEXTURE_MIN_FILTER,
			int gL_NEAREST, int gL_LINEAR, int gL_LINK_STATUS, int gL_TRUE,
			int gL_FALSE, int gL_TRIANGLES, int gL_POLYGON, int gL_QUADS,
			int gL_LINE_LOOP, int gL_LINES, int gL_ELEMENT_ARRAY_BUFFER,
			int gL_STATIC_DRAW, int gL_DYNAMIC_DRAW, int gL_ARRAY_BUFFER,
			int gL_VERTEX_SHADER, int gL_FRAGMENT_SHADER, int gL_FLOAT,
			int gL_INT, int gL_SHORT, int gL_BYTE) {
		GL_COLOR_BUFFER_BIT = gL_COLOR_BUFFER_BIT;
		GL_DEPTH_BUFFER_BIT = gL_DEPTH_BUFFER_BIT;
		GL_DEPTH_TEST = gL_DEPTH_TEST;
		GL_TEXTURE_2D = gL_TEXTURE_2D;
		GL_RGBA = gL_RGBA;
		GL_UNSIGNED_BYTE = gL_UNSIGNED_BYTE;
		GL_UNSIGNED_INT = gL_UNSIGNED_INT;
		GL_UNSIGNED_SHORT = gL_UNSIGNED_SHORT;
		GL_TEXTURE_MAG_FILTER = gL_TEXTURE_MAG_FILTER;
		GL_TEXTURE_MIN_FILTER = gL_TEXTURE_MIN_FILTER;
		GL_NEAREST = gL_NEAREST;
		GL_LINEAR = gL_LINEAR;
		GL_LINK_STATUS = gL_LINK_STATUS;
		GL_TRUE = gL_TRUE;
		GL_FALSE = gL_FALSE;
		GL_TRIANGLES = gL_TRIANGLES;
		GL_POLYGON = gL_POLYGON;
		GL_QUADS = gL_QUADS;
		GL_LINE_LOOP = gL_LINE_LOOP;
		GL_LINES = gL_LINES;
		GL_ELEMENT_ARRAY_BUFFER = gL_ELEMENT_ARRAY_BUFFER;
		GL_STATIC_DRAW = gL_STATIC_DRAW;
		GL_DYNAMIC_DRAW = gL_DYNAMIC_DRAW;
		GL_ARRAY_BUFFER = gL_ARRAY_BUFFER;
		GL_VERTEX_SHADER = gL_VERTEX_SHADER;
		GL_FRAGMENT_SHADER = gL_FRAGMENT_SHADER;
		GL_FLOAT = gL_FLOAT;
		GL_INT = gL_INT;
		GL_SHORT = gL_SHORT;
		GL_BYTE = gL_BYTE;
	}

	public abstract void glClear(int code);
	


	public abstract void glScissor(int x, int y, int width, int height);
	public abstract void glViewport(int x, int y, int width, int height);
	public abstract String getVersion();
	public abstract void glDisable(int flag);
	public abstract void glEnable(int flag);
	
	//textures
	public abstract int glGenTextures();
	public abstract void glBindTexture(int target, int id);
	public abstract void glTexImage2D(int target, int level, int internalFormat, int width, int height, int border, int format, int type, ByteBuffer data);
	public abstract void glTexParameterf(int target, int pname, float param);
	public abstract void glTexParameteri(int target, int pname, int param);
	public abstract float glGetFloat(int field);
	public abstract void glDeleteTextures(int id);
	
	//programs
	public abstract int glCreateProgram();
	public abstract void glLinkProgram(int programId);
	public abstract void glValidateProgram(int programId);
	public abstract int glGetProgrami(int programId, int field);
	public abstract String glGetProgramInfoLog(int programId, int length);
	
	//shaders
	public abstract int glCreateShader(int type);
	public abstract void glShaderSource(int shaderId, String shader);
	public abstract void glCompileShader(int shaderId);
	public abstract int glGetShaderi(int shaderId, int field);
	public abstract String glGetShaderInfoLog(int shaderId, int length);

	
	
	//fields...
	
	
}
