package com.thepattybeastlabs.core.opengl;



import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL21;

public class LWJGLGLContext extends GLContext {
	
	public LWJGLGLContext(){
		super(GL11.GL_COLOR_BUFFER_BIT, GL11.GL_DEPTH_BUFFER_BIT, GL11.GL_DEPTH_TEST, GL11.GL_TEXTURE_2D, GL11.GL_RGBA, 
				GL11.GL_UNSIGNED_BYTE, GL11.GL_UNSIGNED_INT, GL11.GL_UNSIGNED_SHORT, GL11.GL_TEXTURE_MAG_FILTER, 
				GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST, GL11.GL_LINEAR, GL20.GL_LINK_STATUS, GL11.GL_TRUE, 
				GL11.GL_FALSE, GL11.GL_TRIANGLES, GL11.GL_POLYGON, GL11.GL_QUADS, GL11.GL_LINE_LOOP, GL11.GL_LINES, 
				GL15.GL_ELEMENT_ARRAY_BUFFER, GL15.GL_STATIC_DRAW, GL15.GL_DYNAMIC_DRAW, GL15.GL_ARRAY_BUFFER, GL20.GL_VERTEX_SHADER, 
				GL20.GL_FRAGMENT_SHADER, GL11.GL_FLOAT, GL11.GL_INT, GL11.GL_SHORT, GL11.GL_BYTE);
	}
	
	public void glClear(int code) {
		GL11.glClear(code);
	}

	public void glScissor(int x, int y, int width, int height) {
		GL11.glScissor(x, y, width, height);
	}
	
	public void glViewport(int x, int y, int width, int height) {
		GL11.glViewport(x, y, width, height);
	}
	
	public String getVersion(){
		return GL11.glGetString(GL11.GL_VERSION);
	}
	
	public void glDisable(int flag){
		GL11.glDisable(flag);
	}
	
	public void glEnable(int flag){
		GL11.glEnable(flag);
	}
	@Override
	public int glGenTextures() {
		return GL11.glGenTextures();
	}
	@Override
	public void glBindTexture(int target, int id) {
		GL11.glBindTexture(target, id);
	}
	@Override
	public void glTexImage2D(int target, int level, int internalFormat,
			int width, int height, int border, int format, int type,
			ByteBuffer data) {
		GL11.glTexImage2D(target, level, internalFormat, width, height, border, format, type, data);
	}
	@Override
	public void glTexParameterf(int target, int pname, float param) {
		GL11.glTexParameterf(target, pname, param);
	}
	@Override
	public void glTexParameteri(int target, int pname, int param) {
		GL11.glTexParameteri(target, pname, param);
	}
	@Override
	public float glGetFloat(int field) {
		return GL11.glGetFloat(field);
	}
	@Override
	public int glCreateProgram() {
		return GL20.glCreateProgram();
	}
	@Override
	public void glLinkProgram(int programId) {
		GL20.glLinkProgram(programId);
	}
	@Override
	public void glValidateProgram(int programId) {
		GL20.glValidateProgram(programId);
	}
	@Override
	public int glGetProgrami(int programId, int field) {
		return GL20.glGetProgrami(programId, field);
	}
	@Override
	public String glGetProgramInfoLog(int programId, int length) {
		return GL20.glGetProgramInfoLog(programId, length);
	}
	@Override
	public int glCreateShader(int type) {
		return GL20.glCreateShader(type);
	}
	@Override
	public void glShaderSource(int shaderId, String shader) {
		GL20.glShaderSource(shaderId, shader);
	}
	@Override
	public void glCompileShader(int shaderId) {
		GL20.glCompileShader(shaderId);
	}
	@Override
	public int glGetShaderi(int shaderId, int field) {
		return GL20.glGetShaderi(shaderId, field);
	}
	@Override
	public String glGetShaderInfoLog(int shaderId, int length) {
		return GL20.glGetShaderInfoLog(shaderId, length);
	}
	@Override
	public void glDeleteTextures(int id) {
		GL11.glDeleteTextures(id);
	}


}
