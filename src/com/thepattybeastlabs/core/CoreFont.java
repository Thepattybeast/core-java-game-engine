package com.thepattybeastlabs.core;

import java.awt.Color;
import java.awt.Font;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.thepattybeastlabs.core.resource.Image;
import com.thepattybeastlabs.core.resource.LoadableResource;
import com.thepattybeastlabs.core.resource.ResourceList;

public class CoreFont extends LoadableResource{

	public static final ResourceList<CoreFont> list = new ResourceList<CoreFont>(CoreFont.class);
	
	private Image img;
	private FontChar[] charList;
	private float height;
	public static String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-=_+`~[]{}\\|'\";:,.<>/? ";
	private Font font;
	
	public CoreFont(Font font) throws Exception{
		super(new File(font.getFontName() + ":"   + font.getSize()+ ":"+ font.getStyle()));
		this.font = font;
		genImage();
	}
	
	public CoreFont(File font) throws Exception{
		this(font.getPath());
	}
	
	public CoreFont(String font) throws Exception{
		super(new File(font));
		String[] splt = font.split(":");
		String name = "Tahoma";
		int size = Core.defaultFontSize;
		int style = 0;
		if(splt.length > 0){
			name = splt[0];
		}
		if(splt.length > 1){
			size = Integer.parseInt(splt[1]);
		}
		if(splt.length > 2){
			style = Integer.parseInt(splt[2]);
		}
		
		this.font = new Font(name, style, size);
		genImage();
	}
	
	private void genImage() throws Exception{
		Rectangle2D bounds = font.getStringBounds(chars, new FontRenderContext(null, true, false));
		int fWidth = 0;
		for(int i = 0; i < chars.length() ;i ++){
			fWidth += font.getStringBounds(chars.charAt(i)+"", new FontRenderContext(null, true, false)).getWidth()+10;
		}
		int width = (int)Math.sqrt((fWidth*bounds.getHeight()));
		BufferedImage image = new BufferedImage(width, width+(int)bounds.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		java.awt.Graphics2D g = (java.awt.Graphics2D)image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setFont(font);
		charList = new FontChar[chars.length()];
		height = (float)bounds.getHeight();
		int offset = 0;
		int cWidth = 0;
		int yOff = 0;
		for(int i = 0; i < chars.length() ;i ++){
			cWidth = g.getFontMetrics().stringWidth(chars.charAt(i)+"")+10;
			if(offset+cWidth > width){i--;yOff+=(int)bounds.getHeight();offset=0;}
			else{
				charList[i] = new FontChar(chars.charAt(i), offset,g.getFontMetrics().stringWidth(chars.charAt(i)+""), yOff);
				offset += cWidth;
				g.drawString(charList[i].letter+"", charList[i].pos, (int)(-bounds.getY())+yOff);
			}
		}
		//JOptionPane.showMessageDialog(null, new JLabel(new ImageIcon(image)), "Font_Image "  + width, JOptionPane.PLAIN_MESSAGE);
		img = new Image(image);
	}
	
	public void load() throws Exception{
		img.load();
	}
	
	public Image getImage(){
		return img;
	}
	
	
	
	public FontChar[] getCharlist(){
		return charList;
	}
	
	public class FontChar{
		private char letter;
		private int pos, width, hoff;
		public FontChar(char letter, int pos, int width, int hoff) {
			this.letter = letter;
			this.pos = pos;
			this.width = width;
			this.hoff = hoff;
		}
		public char getLetter() {
			return letter;
		}
		public void setLetter(char letter) {
			this.letter = letter;
		}
		public int getPos() {
			return pos;
		}
		public void setPos(int pos) {
			this.pos = pos;
		}
		public int getWidth() {
			return width;
		}
		public void setWidth(int width) {
			this.width = width;
		}
		public int getHOff(){
			return hoff;
		}
		public void setHOff(int hOff){
			this.hoff=hOff;
		}
	}

	public void destroy() throws Exception {
		img.destroy();
	}

	public float getHeight() {
		return height;
	}
	
}
