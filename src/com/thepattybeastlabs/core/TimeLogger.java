package com.thepattybeastlabs.core;

import java.util.HashMap;
import java.util.Map;

public class TimeLogger {

	private Map<String, Item> items = new HashMap<>();
	private String current;
	private StringBuilder b = new StringBuilder();
	private int frames = 0;
	
	public void update(){
		
	}
	
	public void begin(String item){
		if(current!=null)end();
		current = item;
		Item i = items.get(item);
		if(i==null)items.put(item, i=new Item());
		i.st = System.nanoTime();
	}
	
	/**End the current item, called if new item is started*/
	public void end(){
		Item i = items.get(current);
		i.ft+=System.nanoTime()-i.st;
		current=null;
	}

	public void finish() {
		if(current!=null)end();
		frames++;
	}
	
	public void updateDisp(){
		b.delete(0, b.length());
		int i = 0;
		for(String key : items.keySet()){
			i++;
			Item it = items.get(key);
			b.append(String.format("%.2fms/%s", (double)(it.ft/frames)/1000000.0, key));
			it.ft = 0;
			if(i!=items.size())b.append(", ");
		}
		frames = 0;
	}
	
	public String disp(){
		return b.toString();
	}
	
	private class Item{
		public long st, ft;
	}
	
}
