package com.thepattybeastlabs.core.shader;

public class ShaderProgramException extends RuntimeException {

private ShaderProgram program;
	
	public ShaderProgramException(ShaderProgram program, String message) {
		super(message);
		this.program=program;
	}

	/**@returns the shader the exception occured on*/
	public ShaderProgram getShaderProgram() {
		return program;
	}
	
}
