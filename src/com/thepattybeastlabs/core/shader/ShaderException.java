package com.thepattybeastlabs.core.shader;

public class ShaderException extends RuntimeException {

	private Shader shader;
	
	public ShaderException(Shader shader, String message) {
		super(message);
		this.shader = shader;
	}

	/**@returns the shader the exception occured on*/
	public Shader getShader() {
		return shader;
	}

}
