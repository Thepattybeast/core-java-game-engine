package com.thepattybeastlabs.core.shader;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.OpenGLException;

import com.thepattybeastlabs.core.Core;
import com.thepattybeastlabs.core.resource.LoadableResource;
import com.thepattybeastlabs.core.resource.ResourceList;
import com.thepattybeastlabs.core.utils.ReadOnlyList;

public class ShaderProgram extends LoadableResource {

	private static List<ShaderProgram> iList = new ArrayList<ShaderProgram>();
	public static final ReadOnlyList<ShaderProgram> SHADERPROGRAMS=new ReadOnlyList<>(iList);
	public static final ResourceList<ShaderProgram> list = new ResourceList<>("res/shaders", ShaderProgram.class);
	
	public static void recreateGL(){
		for(ShaderProgram s:SHADERPROGRAMS){
			s.loadDontAdd();
		}
	}
	
	public static void destroyGL(){
		for(ShaderProgram s:SHADERPROGRAMS){
			s.unload();
		}
	}
	
	private Shader[] shaders;
	private int id=-1;
	private String lastLoadLog;
	
	public ShaderProgram(File path) throws Exception {
		super(path);
		throw new RuntimeException("Not Implemented.");
	}
	
	public ShaderProgram(Shader... shaders) throws Exception {
		this.shaders = shaders;
		
	}
	
	public int getAttrib(String name){
		int attr = glGetAttribLocation(id, name);
		if(attr==-1)throw new ShaderProgramException(this, "Attrib '"+name+"' could not be found.");
		return attr;
	}
	
	public int getUniform(String name){
		int attr = glGetUniformLocation(id, name);
		if(attr==-1)throw new ShaderProgramException(this, "Uniform '"+name+"' could not be found.");
		return attr;
	}
	
	public void load() throws Exception {
		loadDontAdd();
		iList.add(this);
	}
	
	private void loadDontAdd(){
		for(Shader s : shaders){
			s.load();
		}
		int prog = glCreateProgram();
		if(prog==0)throw new ShaderProgramException(this, "Failed to create program, glCreateProgram() returned 0") ;
		
		for(int i = 0; i < shaders.length; i++){
			glAttachShader(prog, shaders[i].getId());
		}
		glLinkProgram(prog);
		if(glGetProgrami(prog, GL_LINK_STATUS)==GL_FALSE){
			lastLoadLog = glGetProgramInfoLog(prog);
			
			throw new ShaderProgramException(this, "Failed to link shader: \n"+lastLoadLog);
			
		}
		glValidateProgram(prog);
		if(glGetProgrami(prog, GL_VALIDATE_STATUS)==GL_FALSE){
			lastLoadLog = glGetProgramInfoLog(prog);
			throw new ShaderProgramException(this, "Failed to validate shader: \n"+lastLoadLog);
		}
		for(Shader s : shaders){
			s.unload();
		}
		id=prog;
	}
	
	public boolean isLoaded(){
		return id!=-1;
	}
	
	public void destroy() throws Exception {
		unload();
		iList.remove(this);
	}
	
	private void unload(){
		glDeleteProgram(id);
	}

	public Shader[] getShaders() {
		return shaders;
	}

	public void setShaders(Shader[] shaders) {
		this.shaders = shaders;
	}

	public int getId() {
		return id;
	}

	public String getLastLoadLog() {
		return lastLoadLog;
	}

	public void setLastLoadLog(String lastLoadLog) {
		this.lastLoadLog = lastLoadLog;
	}

}
