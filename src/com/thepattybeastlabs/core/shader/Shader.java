package com.thepattybeastlabs.core.shader;

import com.thepattybeastlabs.core.Core;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.GL40;
import org.lwjgl.opengl.GL43;

public class Shader {

	public enum ShaderType{VERTEX,FRAGMENT,TESS_CONTROL,TESS_EVALUATION,GEOMETRY,COMPUTE}
	
	private ShaderType type;
	private String code;
	private int id=-1;
	private String lastLoadLog;
	
	public Shader(ShaderType type, String code) {
		this.type = type;
		this.code = code;
	}
	
	public Shader(ShaderType type, File file) throws IOException {
		this.type = type;
		this.code = readFile(file);
	}
	
	private String readFile(File file) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		StringBuilder res = new StringBuilder();
		String line = null;
		while((line=reader.readLine())!=null){
			res.append(line+'\n');
		}
		reader.close();
		return res.toString();
	}

	public boolean isLoaded(){
		return id!=-1;
	}
	
	/**Loads this specific shader into memory. This method is only meant to be called from a ShaderProgram.*/
	public void load(){
		if(id!=-1)return;
		int shader = glCreateShader(getTypeCode(type));
		if(shader==0)throw new ShaderException(this, "Failed to allocate shader in memory.");
		glShaderSource(shader, code);
		glCompileShader(shader);
		lastLoadLog = glGetShaderInfoLog(shader);
		if(glGetShaderi(shader, GL_COMPILE_STATUS)==GL_FALSE)throw new ShaderException(this, "Failed to compile shader: \n"+lastLoadLog);
		id=shader;
	}
	
	private int getTypeCode(ShaderType type){
		switch(type){
		case VERTEX: return GL_VERTEX_SHADER;
		case FRAGMENT: return GL_FRAGMENT_SHADER;
		case GEOMETRY: return GL32.GL_GEOMETRY_SHADER;
		case COMPUTE: return GL43.GL_COMPUTE_SHADER;
		case TESS_CONTROL: return GL40.GL_TESS_CONTROL_SHADER;
		case TESS_EVALUATION: return GL40.GL_TESS_EVALUATION_SHADER;
		default: return 0;
		}
	}
	
	public void unload(){
		if(id==-1)return;
		id=-1;
		glDeleteShader(id);
	}

	public ShaderType getType() {
		return type;
	}

	public void setType(ShaderType type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getId() {
		return id;
	}

	public String getLastLoadLog() {
		return lastLoadLog;
	}

	public void setLastLoadLog(String lastLoadLog) {
		this.lastLoadLog = lastLoadLog;
	}
	
	
	
}
