package com.thepattybeastlabs.core;

import java.util.ArrayList;
import java.util.Arrays;

import com.thepattybeastlabs.core.graphics.GLBatchRender;

public class Util {

	public static void paintLoadSandTimer(GLBatchRender r, float x, float y, float width, float height){
		r.bindTexture(null);
		r.setRenderMode(r.GL_TRIANGLES);
		r.vertex(x, y);r.indiceEnd(0);
		r.vertex(x+width, y);r.indiceEnd(0);
		r.vertex(x+(width/2), y+(height/2)+5);r.indiceEnd(0);
		r.vertex(x+(width/2), y+(height/2)-5);r.indiceEnd(0);
		r.vertex(x+width, y+height);r.indiceEnd(0);
		r.vertex(x, y+height);r.indiceEnd(0);
		r.setRenderMode(r.GL_LINES);
		r.vertex(x, y);r.indiceEnd(0);
		r.vertex(x, y+height);r.indiceEnd(0);
		r.vertex(x+width, y);r.indiceEnd(0);
		r.vertex(x+width, y+height);r.indiceEnd(0);
	}
	
	public static String[] parseCommandLine(String cmdLine){
		ArrayList<String> command = new ArrayList<>();
		StringBuilder buf = new StringBuilder();
		boolean squote=false, dquote=false, escape=false;
		for(int i = 0; i < cmdLine.length(); i++){
			char c = cmdLine.charAt(i);
			boolean setEscape=false;
			if(c=='"'&&!squote&&!escape)dquote=!dquote;
			else if(c=='\''&&!dquote&&!escape)squote=!squote;
			else if(c=='\\'&&!escape)setEscape=true;
			else if(c==' '&&!dquote&&!squote&&!escape){
				if(buf.length()>0){
					command.add(buf.toString());
					buf.delete(0, buf.length());
				}
			}else{
				buf.append(c);
			}
			escape=false;
			if(setEscape)escape=true;
		}
		if(buf.length()>0)command.add(buf.toString());
		System.out.println("Command: " + Arrays.toString(command.toArray()));
		return command.toArray(new String[0]);
	}
	
}
