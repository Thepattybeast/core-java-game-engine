package com.thepattybeastlabs.core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.lwjgl.LWJGLUtil;
import org.lwjgl.Sys;

import com.thepattybeastlabs.core.audio.AL10AudioEngine;
import com.thepattybeastlabs.core.audio.AudioEngine;
import com.thepattybeastlabs.core.config.Config;
import com.thepattybeastlabs.core.console.Console;
import com.thepattybeastlabs.core.geom.Vector2;
import com.thepattybeastlabs.core.graphics.BatchGraphics;
import com.thepattybeastlabs.core.graphics.Camera3D;
import com.thepattybeastlabs.core.graphics.GLBatchRender;
import com.thepattybeastlabs.core.graphics.Graphics3D11;
import com.thepattybeastlabs.core.gui.FormButton;
import com.thepattybeastlabs.core.gui.Screen;
import com.thepattybeastlabs.core.gui.ScreenManager;
import com.thepattybeastlabs.core.gui.Window;
import com.thepattybeastlabs.core.gui.WindowManager;
import com.thepattybeastlabs.core.gui.presetui.ErrorDialog;
import com.thepattybeastlabs.core.gui.presetui.GLBatchRendererListWindow;
import com.thepattybeastlabs.core.gui.presetui.ImageListWindow;
import com.thepattybeastlabs.core.input.Input;
import com.thepattybeastlabs.core.opengl.GLContext;
import com.thepattybeastlabs.core.opengl.LWJGLGLContext;
import com.thepattybeastlabs.core.resource.Image;
import com.thepattybeastlabs.core.resource.ResourceList;
import com.thepattybeastlabs.core.resource.ResourceLoadingUtils;
import com.thepattybeastlabs.core.shader.Shader;
import com.thepattybeastlabs.core.shader.ShaderProgram;
import com.thepattybeastlabs.core.system.LWJGLSystem;
import com.thepattybeastlabs.core.utils.PrintStreamWrapper;

public final class Core {

	public static final int FPS_UNCAPPED = 0;
	
	public static float bg_rot = 0;
	
	public static boolean doDynamicLoading;
	public static boolean doDynamicResChanges = false;
	public static boolean doResourceLoadLogs = true;
	public static boolean doDynamicLoadWarnings = true;
	public static boolean useCullFace = false;
	
	
	public static boolean lockCursor = false;
	/**The size of the font if unspecified*/
	public static int defaultFontSize = 120;
	public static boolean drawScreen = true;
	public static int deltaTickrate = 140;
	public static boolean forcePowTwoTex = true;
	public static boolean showFPS = true;
	public static String loadString = "Loading Engine...";
	public static int targetFPS = FPS_UNCAPPED;
	public static int glCalls = 0;
	public static final double VERSION = 0.14;
	private static boolean running;
	public static boolean forceAr = true;
	private static int fps = 0;
	private static long targetLoopTime = 1000000000/140;
	public static int width = 1280, height = 720, winWidth, winHeight;
	public static float targetWidth, targetHeight;
	private static ScreenManager screenManager = ScreenManager.I;
	public static Graphics g;
	public static Graphics3D11 g3d;
	public static GLContext gl;
	public static AudioEngine ae;
	public static Object level;
	public static Input input;
	private static CoreGame game;
	private static CoreFont f;
	private static boolean reloadShaders=false;
	
	public static Image bg;
	public static Image lastBG;
	
	private static long st = System.currentTimeMillis();
	private static long nt = System.nanoTime();
	private static long ft = System.nanoTime();
	private static TimeLogger tLog;
	private static int frames = 0;
	private static long frameTime=0;
	private static Window errorWindow;
	
	/**Internal actual drawbounds use width, and height vars instead*/
	public static Rectangle drawBounds;
	
	public static com.thepattybeastlabs.core.system.System sys;
	public static final WindowManager wm = WindowManager.I;
	public static ArrayList<ResourceList<?>> lists = new ArrayList<>();
	private static boolean loaded;
	private static boolean isPostInitRun;
	private static double verCode = 0;
	private static Thread currentThread;
	
	public static void loadGame(String[] args, int internalRenderWidth, int internalRenderHeight, final CoreGame game){
		
		Core.game = game;
		
		tLog = new TimeLogger();
		targetWidth = internalRenderWidth;
		targetHeight = internalRenderHeight;
		winWidth=0;winHeight=0;//must be zero to trigger resize on create(setup input)
		int createWidth = Config.i.get("Display", "width", internalRenderWidth);
		int createHeight = Config.i.get("Display", "height", internalRenderHeight);
		int winMode = Config.i.get("Display", "mode", 2);
		for(int i = 0; i < args.length; i++){
			if(args[i].equals("-w")){
				i++;createWidth = Integer.parseInt(args[i]);
			}
			if(args[i].equals("-h")){
				i++;createHeight = Integer.parseInt(args[i]);
			}
			if(args[i].equals("-wm")){
				i++;winMode = Integer.parseInt(args[i]);
			}
		}
		System.out.println("Core Engine: " + VERSION);
		width = internalRenderWidth;
		height = internalRenderHeight;
		sys = new LWJGLSystem();
		Console.i(new Console("Console", 640, 480));
		System.setOut(new PrintStreamWrapper(System.out, "[INFO]"));
		System.setErr(new PrintStreamWrapper(System.err, "[ERR]"));
		//System.out.println(Sys.getVersion());
		//g = new Graphics11();
		gl = new LWJGLGLContext();
		g3d = new Graphics3D11();
		ae = new AL10AudioEngine();
		try {
			sys.createDisplay(game.getName(), createWidth, createHeight, winMode);
			verCode = getVersion(gl.getVersion());
			g = new BatchGraphics();
			g.create();
			g3d.create();
			ae.create();
		} catch (Throwable e) {
			e.printStackTrace();
			Core.haltAndCatchFire(e);
			System.exit(1);
		}
		
		System.out.println("Video: OpenGL Version " + gl.getVersion() + " (Version=" + verCode + ")");
		System.out.println("Audio: " + ae.getName());
		
		running = true;	
		//winWidth = Display.getWidth();
		//winHeight = Display.getHeight();
		g.setClearColor(Color.BLACK);
		g.setColor(Color.WHITE);
		Util.paintLoadSandTimer(g.getBatchRender(), Core.width/2-50, Core.height/2-100, 100, 200);
		g.finish();
		sys.updateDisplay();
		System.out.println("Loading Font..");
		try {
			f = new CoreFont(new Font("Tahoma", 0, 40));
			f.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Loading Game");
		try {
			g.setFont(f, 22);
			g.drawString("Loading Engine...", width-300, height-100, new Color(0,0,0,100));
			game.openGLInit();
			g.finish();
			sys.updateDisplay();
			
			new File("cfg").mkdir();
			input = new Input(new Rectangle(0, 0, width, height) ,new Dimension(width, height), new File("cfg/keymap.cfg"));
			input.setSystem(sys);
			
			currentThread = Thread.currentThread();
			new Thread(new Runnable() {
				public void run() {
					try {
						input.load();
						loadString = "Loading Game...";
						game.init(input);
						input.createBind("Show Console", "Console_Open", Input.OS_KEYBOARD, "`");
						input.createBind("Acitvate Gui", "gui_activate", Input.OS_MOUSE, "m1");
						input.createBind("Scroll Gui", "gui_scroll", Input.OS_MOUSE, "mwheel");
						input.createBind("Toggle Wireframe", "wireframe_toggle", Input.OS_KEYBOARD, "F12");
						input.createBind("Toggle Debug Info", "fps_toggle", Input.OS_KEYBOARD, "F3");
						loadString = "Loading Resources...";
						while(getLoadedListsCount() < lists.size()){
							Thread.sleep(64);
							loadString = "Loading Resources ("+getLoadedListsCount()+"/"+lists.size()+")...";
						}
						loadString = "Finishing...";
						System.gc();
						loaded=true;
					} catch (Exception e) {
						CoreFont.list.loadResource(f, "Tahoma");
						Core.lockCursor = false;
						e.printStackTrace();
						errorWindow = ErrorDialog.window("Critical Error During Initialization", e, Core.width-200, 400);
						loadString = "";
					}
				}
			}, "Game_Load").start();
			
			System.out.println("Stating Thread");
			while(running){
				runLoop();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			String errText = "Error: TYPE_UNHANDLED_EXCEPTION<br>"+e1.toString() + "<br>";
			StackTraceElement[] stes = e1.getStackTrace();
			for(int i = 0; i < stes.length; i++){
				errText+="    at "+stes[i] + "<br>";
			}
			errText+="(Please <a href=SYS_COPY>copy</a> and paste this message to any error reports.)";
			showMessage(errText, "Core Encountered An Error");
		}
		
		try {
			System.out.println("Shutting down");
			game.close();
			for(int i = 0; i < lists.size(); i++){
				lists.get(i).destory();
				i--;
			}
			ae.destroy();
			g.destory();
			sys.destoryDisplay();
			input.save();
			Config.i.save();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(0);//force close the application in case there are other running threads blocking close
	}
	
	public static void recreateGL(){
		ShaderProgram.recreateGL();
		GLBatchRender.recreateGL();
		Image.recreateGL();
		try {
			g.create();
		} catch (Exception e) {
			e.printStackTrace();
		}
		onResize(null);
	}
	
	public static void destroyGL(){
		GLBatchRender.destoryGL();
		Image.destroyGL();
		ShaderProgram.destroyGL();
	}
	
	public static CoreFont getEngineFont(){
		return f;
	}
	
	private static void runLoop(){
		try{
			if(errorWindow!=null){
				errorWindow.tick(input, ae, 1);//should be delta terrible hack dont think it will break anything yet
				errorWindow.render(g, input);//the entire error system is a terrible hack, i feel bad :(.
				if(errorWindow.getLastActivatedId().equals("Exit_Btn")){
					Core.exit();
				}
			}
				targetLoopTime = 1000000000/deltaTickrate;
				tLog.begin("sys");
				Vector2 dispSize = sys.getDisplayResolution();
				if(dispSize.getX() != winWidth || dispSize.getY() != winHeight){
					onResize(dispSize);
				}
				//GL11.glFlush();
				gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT);
				sys.tick();
				if(lockCursor && screenManager.getScreen()==null&&wm.windows().size()<1){
					Vector2 cursor = sys.getMouseInfo();
					if(cursor.getX() < 0)cursor.setX(0);
					if(cursor.getY() < 0)cursor.setY(0);
					if(cursor.getX() > width)cursor.setX(width);
					if(cursor.getY() > height)cursor.setY(height);
					sys.setMousePosition(cursor);
				}
				input.tick();
				if(loaded && input.isPressed("wireframe_toggle")){
					GLBatchRender.WIREFRAME = !GLBatchRender.WIREFRAME;
				}
				if(loaded && input.isPressed("fps_toggle")){
					showFPS = !showFPS;
				}
				tLog.begin("tick");
				float delta = (float)((double)(System.nanoTime()-nt)/(double)targetLoopTime);
				nt = System.nanoTime();
				if(loaded&&errorWindow==null)game.tick(input, ae, delta, screenManager.getScreen());
				if(screenManager!=null && drawScreen&&errorWindow==null){
					screenManager.tick(input, ae, delta, 0, 0, Core.width, Core.height);
				}
				if(errorWindow==null)
				wm.tick(input, ae, delta);
				if(loaded && input.isPressed("Console_Open")){
					wm.add(Console.i().getWindow());
				}
				
				ft = System.nanoTime();
				tLog.begin("ren3d");
				gl.glEnable(gl.GL_DEPTH_TEST);
				if(loaded&&errorWindow==null)game.render(g3d, input);
				
				tLog.begin("gl3d");
				g3d.flush();
				gl.glDisable(gl.GL_DEPTH_TEST);
				tLog.begin("ren");
				if(loaded&&errorWindow==null)game.render(g, input);
				if(!loaded&&(screenManager==null||screenManager.getScreen()==null)){
					g.setFont(f, 22);
					g.drawString(loadString, width-300, height-100, new Color(0,0,0,100));
				}
				if(screenManager!=null && drawScreen&&errorWindow==null){
					screenManager.render(g, input, 0, 0, Core.width, Core.height);
				}
				g.setColor(new Color(255,255,100));
				if(errorWindow==null)
				wm.render(g, input);
				
				g.setColor(Color.WHITE);
				g.setFont(f, 22);
				tLog.end();
				if(showFPS){
					g.setTextAlign(0, 0);
					g.drawString(fps+" fps ("+(int)(1000.0/(frameTime/1000000.0))+" ft), "+ResourceLoadingUtils.readableBytes(Image.size) + ", " + glCalls + " glCalls/f", 5, 5 , new Color(0,0,0,100));
					g.drawString(tLog.disp(), 5, 30 , new Color(0,0,0,100));
					g.setTextAlign(2, 0);
					Runtime r = Runtime.getRuntime();
					g.drawString(ResourceLoadingUtils.readableBytes(r.totalMemory()-r.freeMemory()) + "/"+
					ResourceLoadingUtils.readableBytes(r.totalMemory())+", Max: " + 
					ResourceLoadingUtils.readableBytes(r.maxMemory()), width-5, 5 , new Color(0,0,0,100));
					g.setTextAlign(0, 0);
				}
				glCalls = 0;
				tLog.begin("gl");
				g.finish();
				
				tLog.begin("display");
				sys.updateDisplay();
				tLog.finish();
				
				ft = System.nanoTime()-ft;
				for(int i = 0; i < lists.size(); i++){
					if(!lists.get(i).isLoadQueEmpty()){
						lists.get(i).tickLoad();
						break;
					}
				}
				if(!isPostInitRun && loaded){
					isPostInitRun = true;
					game.postInit();
				}
				if(sys.isExitRequested())running = false;
				if(targetFPS > 0){
					long wait = (long)(1000000000.0/(double)targetFPS)-ft;
					if(wait > 0){
						Thread.sleep((int)((double)wait/1000000.0));
					}
				}
				frames++;
				if(System.currentTimeMillis() - st > 1000){
					fps = frames;
					frameTime = ft;
					frames = 0;
					tLog.updateDisp();
					st = System.currentTimeMillis();
				}
			
		}catch(Throwable e){
			e.printStackTrace();
			errorWindow = ErrorDialog.window(game.getName() + " Critical Error", e, Core.width-200, 350);
			Core.lockCursor = false;
		}
	}
	
	private static void onResize(Vector2 dispSize){
		if(dispSize==null)dispSize=sys.getDisplayResolution();
		winWidth = (int)dispSize.getX();
		winHeight = (int)dispSize.getY();
		int vpHeight = (int)((float)winWidth/targetWidth*targetHeight);
		int vpWidth = winWidth;
		if(vpHeight > winHeight){
			vpHeight = winHeight;
			vpWidth = (int)((float)winHeight/targetHeight*targetWidth);
		}
		if(!forceAr){
			vpWidth = winWidth;
			vpHeight = winHeight;
		}
		input.setDrawBounds(drawBounds = new Rectangle((winWidth/2-vpWidth/2), (winHeight/2-vpHeight/2), vpWidth, vpHeight));
		if(doDynamicResChanges){
			width = vpWidth;
			height = vpHeight;
			input.setInternalRenderingResolution(new Dimension(width, height));
			g.setRenderingScale(width, height);
		}
		gl.glViewport((winWidth/2-vpWidth/2), (winHeight/2-vpHeight/2), vpWidth, vpHeight);
		g.resetClip();
	}
	
	private static double getVersion(String versionCode){
		String[] s1 = versionCode.split(" ");
		String[] s2 = s1[0].split("\\.");
		return Double.parseDouble(s2[0]+"."+s2[1]);
	}
	
	/**@Returns the number of loaded resource lists, this number may differ from lists.size()*/
	public static int getLoadedListsCount(){
		int num = 0;
		for(int i = 0; i < lists.size(); i++){
			if(lists.get(i).isLoadQueEmpty())num++;
		}
		return num;
	}
	
	public static void setScreen(Screen screen){
		screenManager.setScreen(screen);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getLevel(){
		return (T)level;
	}
	
	public static void showAndWait(Window window){
		if(hasOpenGLContext()){
			System.err.println("Core.showAndWait() cannot be called in main thread.");
			return;
		}
		window.setCloseOnActivate(true);
		wm.add(window);
		while(wm.contains(window)){
			try {
				Thread.sleep(64);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Rectangle getViewBounds(){
		int vpHeight = winWidth/16*9;
		int vpWidth = winWidth;
		if(vpHeight > winHeight){
			vpHeight = winHeight;
			vpWidth = winHeight/9*16;
		}
		return new Rectangle((winWidth/2-vpWidth/2), (winHeight/2-vpHeight/2), vpWidth, vpHeight);
	}
	
	public static boolean hasOpenGLContext(){
		return Thread.currentThread()==currentThread;
	}
	
	public static void exit(){
		running = false;
	}
	
	public static boolean isRunning(){
		return running;
	}
	
	/**Show a html formatted message**/
	public static void showMessage(final String message, String title){
		JEditorPane t = new JEditorPane("text/html", "<html>"+message+"</html>");
		t.setOpaque(false);
		t.setEditable(false);
		t.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if(e.getEventType()==HyperlinkEvent.EventType.ACTIVATED){
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(message), new ClipboardOwner() {
						public void lostOwnership(java.awt.datatransfer.Clipboard clipboard,
								Transferable contents) {	
						}
					});
				}
			}
		});
		JOptionPane.showMessageDialog(null, t, title, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void haltAndCatchFire(Throwable e){
		e.printStackTrace();
		String errText = e.toString()+"<br>";
		StackTraceElement[] stes = e.getStackTrace();
		for(int i = 0; i < stes.length; i++){
			errText+="&nbsp;&nbsp;&nbsp;&nbsp;at "+stes[i] + "<br>";
		}
		errText+="(Please <a href=SYS_COPY>copy</a> and paste this message to any error reports.)";
		showMessage(errText, "Core Error");
		exit();
	}
	
}
