#version 120
attribute vec3 vertexPosition;
attribute vec4 color;
attribute vec2 tex;
varying vec4 fColor;
varying vec2 fTex;
uniform mat4 proj;
uniform mat4 trans;

void main(){
	gl_Position=proj*trans*vec4(vertexPosition,1.0);
	fColor=color;
	fTex=tex;
}