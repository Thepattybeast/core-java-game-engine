#version 120
varying vec4 fColor;
varying vec2 fTex;
uniform sampler2D mts;

float resolution = 1024.0;
float radius = 3.0;
vec2 dir = vec2(1.0, 1.0);
void main(){
	vec4 sum=vec4(0, 0,0,0);
	vec4 setColor = ((fColor+128)/255);
	float hstep = dir.x;
	float vstep = dir.y;
	float blur = radius / resolution; 
	
    //apply blurring, using a 9-tap filter with predefined gaussian weights
    
	sum += texture2D(mts, vec2(fTex.x - 4.0*blur*hstep, fTex.y - 4.0*blur*vstep)) * 0.0162162162;
	sum += texture2D(mts, vec2(fTex.x - 3.0*blur*hstep, fTex.y - 3.0*blur*vstep)) * 0.0540540541;
	sum += texture2D(mts, vec2(fTex.x - 2.0*blur*hstep, fTex.y - 2.0*blur*vstep)) * 0.1216216216;
	sum += texture2D(mts, vec2(fTex.x - 1.0*blur*hstep, fTex.y - 1.0*blur*vstep)) * 0.1945945946;
	
	sum += texture2D(mts, vec2(fTex.x, fTex.y)) * 0.2270270270;
	
	sum += texture2D(mts, vec2(fTex.x + 1.0*blur*hstep, fTex.y + 1.0*blur*vstep)) * 0.1945945946;
	sum += texture2D(mts, vec2(fTex.x + 2.0*blur*hstep, fTex.y + 2.0*blur*vstep)) * 0.1216216216;
	sum += texture2D(mts, vec2(fTex.x + 3.0*blur*hstep, fTex.y + 3.0*blur*vstep)) * 0.0540540541;
	sum += texture2D(mts, vec2(fTex.x + 4.0*blur*hstep, fTex.y + 4.0*blur*vstep)) * 0.0162162162;
	gl_FragColor = vec4(min(sum.x, setColor.x),min(sum.y, setColor.y),min(sum.z, setColor.z), min(sum.a, setColor.a));
}